<?php
# MASTER/NAVIGATION
$lang['main_menu']      = "Menu";
$lang['home']           = "Accueil";
$lang['product']        = "Produit";
$lang['women']          = "Femmes";
$lang['men']            = "Hommes";
$lang['crafts']         = "Artisanat";
$lang['about']          = "Qui sommes nous";
$lang['handmade']		= "Fait main";
$lang['brand']			= "Marque";
$lang['shop']			= "Boutique";
$lang['contact']        = "Contact";
$lang['subscription']   = "Abonnement";
$lang['socmed']         = "Des médias sociaux";
$lang['user']           = "Utilisateur";
$lang['sign_out']       = "Déconnexion";

# BUTTONS
$lang['btn_add']		= "Ajouter";
$lang['btn_update']     = "Mettre à jour";
$lang['btn_save']       = "Sauvegarder";
$lang['btn_close']      = "Fermer";
$lang['btn_cancel']     = "Annuler";
$lang['btn_detail']     = "Détail";
$lang['btn_reply']      = "Répondre";
$lang['btn_export']     = "Exportation";

# GENERAL
$lang['image']          = "Image";
$lang['name']           = "Prénom";
$lang['category']       = "Catégorie";
$lang['you_sure']       = "Êtes-vous sûr?";
$lang['title']          = "Titre";
$lang['content_image']  = "Image du contenu";
$lang['max_file_size']  = "Taille maximale du fichier";
$lang['min_resolution'] = "Résolution d'image minimale";
$lang['min_video_resolution'] = "Résolution vidéo minimale";
$lang['file_format']    = "Format de fichier";
$lang['option']         = "Options";
$lang['text']           = "Texte";
$lang['author']         = "Auteur";
$lang['banner']         = "Bannière";
$lang['banner_video']	= "Bannière vidéo";
$lang['img_sq']         = "Image carrée";
$lang['img_pt']         = "Portrait d'image";
$lang['link']           = "Lien";
$lang['select_file']	= "Sélectionner le fichier à importer";

# PRODUCT
$lang['products']       = "Des produits";
$lang['item']           = "Article";
$lang['product_detail'] = "Détail du produit";
$lang['overview']       = "Vue d'ensemble";
$lang['description']    = "La description";
$lang['specification']  = "Spécification";
$lang['variant']        = "Une variante";
$lang['color']          = "Couleur";
$lang['basic']          = "De base";
$lang['print']          = "Impression";
$lang['beach_wear']     = "Tenue de plage";
$lang['casual_chic']    = "Casual Chic";
$lang['choose_display'] = "Choisissez les produits à afficher";

# HOME
$lang['notification']   = "Notification";
$lang['carousel']       = "Carrousel";
$lang['quotes']         = "Citations";
$lang['content']        = "Contenu";
$lang['paragraph']		= "Paragraphe";
$lang['image_link']		= "Lien image";

# ABOUT
$lang['advantage']      = "Avantage";
$lang['our_values']     = "Nos valeurs";
$lang['beach_shop']     = "Le magasin de plage";

# CONTACT
$lang['messages']       = "Messages";
$lang['branch']         = "Branche";
$lang['address']        = "Addresse";
$lang['sender']         = "Expéditeur";
$lang['email']          = "E-Mail";
$lang['subject']        = "Objet";
$lang['date']           = "Date";
$lang['from']           = "Expéditeur";
$lang['read_msg']       = "Lire le message";
$lang['map_address'] 	= "L'adresse de la carte (Google Maps)";

# SUBSCRIBE
$lang['subscribers']    = "Les abonnés";

# USER
$lang['user_manage']    = "Gestion des utilisateurs";
$lang['users']          = "Utilisateurs";
$lang['user']           = "Utilisateur";
$lang['full_name']      = "Nom complet";
$lang['level']          = "Niveau";
$lang['password']       = "Mot de passe";
$lang['c_password']     = "Confirmez le mot de passe";
$lang['show_password']  = "Montrer le mot de passe";