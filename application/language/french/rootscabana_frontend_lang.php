<?php
# NAVIGATION
$lang['home']			= "accueil";
$lang['product']		= "produit";
$lang['women'] 			= "femme";
$lang['women_basic']	= "femmes basique";
$lang['women_print']	= "femmes print";
$lang['men']			= "homme";
$lang['men_basic']		= "hommes basique";
$lang['men_print']		= "hommes print";
$lang['all_coll']		= "Toutes les collections";
$lang['basic']			= "basique";
$lang['print']			= "print";
$lang['beach_wear']		= "vêtements de plage";
$lang['casual_chic']	= "casual chic";
$lang['crafts']			= "artisanat";
$lang['about_us']		= "qui sommes-nous";
$lang['contact_us']		= "contactez-nous";
$lang['search_box']		= "barre de recherche";
$lang['lets_search']	= "Let's get searching";
$lang['collections']	= "Collections";
$lang['beach_shop']		= "Magasin de plage";

# FOOTER
$lang['subs_goodness']	= "Abonnez-vous à la newsletter";
$lang['we_promise']		= "Nous nous engageons à vous envoyer uniquement des choses intéressantes!";
$lang['in_touch']		= "Entrer en contact";

# BUTTONS
$lang['search']			= "chercher";
$lang['btn_learnmore']	= "apprendre encore plus";
$lang['btn_subs']		= "souscrire";
$lang['btn_showmore']	= "montre plus";
$lang['btn_share']		= "partager";
$lang['btn_submit']		= "soumettre";
$lang['btn_see']		= "voir +";

# ITEM SINGLE
$lang['pick_color']		= "Choisissez votre couleur préférée:";
$lang['spec']			= "Caractéristiques";
$lang['also_like']		= "Vous pourriez aussi aimer:";
$lang['available_color'] = "Couleurs Disponibles";

# ABOUT
$lang['our_values']		= "Nos valeurs";
$lang['the_beach_shop']	= "Le magasin de plage";
$lang['handmade']		= "Fait main";
$lang['brand']			= "Marque";
$lang['shop']			= "Boutique";

# CONTACT
$lang['name']			= "Prénom";
$lang['email']			= "Email";
$lang['subject']		= "Objet";
$lang['message']		= "Message";

# SEARCH
$lang['no_result']      = "Pas de résultat!";
$lang['result_for']     = "Résultat de la recherche pour:";