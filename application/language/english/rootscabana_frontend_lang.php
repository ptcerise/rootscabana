<?php
# NAVIGATION
$lang['home']			= "home";
$lang['product']		= "product";
$lang['women'] 			= "women";
$lang['women_basic']	= "women basic";
$lang['women_print']	= "women print";
$lang['men']			= "men";
$lang['men_basic']		= "men basic";
$lang['men_print']		= "men print";
$lang['all_coll']		= "all collections";
$lang['basic']			= "basic";
$lang['print']			= "print";
$lang['beach_wear']		= "beach wear";
$lang['casual_chic']	= "casual chic";
$lang['crafts']			= "crafts";
$lang['about_us']		= "about us";
$lang['contact_us']		= "contact us";
$lang['search_box']		= "search box";
$lang['lets_search']	= "Let's get searching";
$lang['collections']	= "collections";
$lang['beach_shop']		= "beach shop";

# FOOTER
$lang['subs_goodness']	= "Subscribe to the newsletter";
$lang['we_promise']		= "We promise to only send you interesting stuff that you will care about!";
$lang['in_touch']		= "Get in touch";

# BUTTONS
$lang['search']			= "search";
$lang['btn_learnmore']	= "learn more";
$lang['btn_subs']		= "subscribe";
$lang['btn_showmore']	= "show more";
$lang['btn_share']		= "share";
$lang['btn_submit']		= "submit";
$lang['btn_see']		= "see +";

# ITEM SINGLE
$lang['pick_color']		= "Pick your favorite color:";
$lang['spec']			= "Specifications";
$lang['also_like']		= "You may also like:";
$lang['available_color'] = "Available Color";

# ABOUT
$lang['our_values']		= "Our values";
$lang['the_beach_shop']	= "The beach shop";
$lang['handmade']		= "Handmade";
$lang['brand']			= "Brand";
$lang['shop']			= "Shop";

# CONTACT
$lang['name']			= "Name";
$lang['email']			= "Email";
$lang['subject']		= "Subject";
$lang['message']		= "Message";

# SEARCH
$lang['no_result']      = "NO RESULTS!";
$lang['result_for']     = "Search results for:";