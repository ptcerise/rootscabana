<?php
# MASTER/NAVIGATION
$lang['main_menu']      = "Main Menu";
$lang['home']           = "Home";
$lang['product']        = "Product";
$lang['women']          = "Women";
$lang['men']            = "Men";
$lang['crafts']         = "Crafts";
$lang['about']          = "About";
$lang['handmade']		= "Handmade";
$lang['brand']			= "Brand";
$lang['shop']			= "Shop";
$lang['contact']        = "Contact";
$lang['subscription']   = "Subscription";
$lang['socmed']         = "Social Media";
$lang['user']           = "User";
$lang['sign_out']       = "Sign out";

# BUTTONS
$lang['btn_add']		= "Add";
$lang['btn_update']     = "Update";
$lang['btn_save']       = "Save";
$lang['btn_close']      = "Close";
$lang['btn_cancel']     = "Cancel";
$lang['btn_detail']     = "Detail";
$lang['btn_reply']      = "Reply";
$lang['btn_export']     = "Export";

# GENERAL
$lang['image']          = "Image";
$lang['name']           = "Name";
$lang['category']       = "Category";
$lang['you_sure']       = "Are you sure?";
$lang['title']          = "Title";
$lang['content_image']  = "Content Image";
$lang['max_file_size']  = "Max file size";
$lang['min_resolution'] = "Min image resolution";
$lang['min_video_resolution'] = "Min video resolution";
$lang['file_format']    = "File format";
$lang['option']         = "Options";
$lang['text']           = "Text";
$lang['author']         = "Author";
$lang['banner']         = "Banner";
$lang['banner_video']	= "Video Banner";
$lang['img_sq']         = "Image Square";
$lang['img_pt']         = "Image Portrait";
$lang['link']           = "Link";
$lang['select_file']	= "Select file to upload";

# PRODUCT
$lang['products']       = "Products";
$lang['item']           = "Item";
$lang['product_detail'] = "Product Detail";
$lang['overview']       = "Overview";
$lang['description']    = "Description";
$lang['specification']  = "Specification";
$lang['variant']        = "Variant";
$lang['color']          = "Color";
$lang['basic']          = "Basic";
$lang['print']          = "Print";
$lang['beach_wear']     = "Beach Wear";
$lang['casual_chic']    = "Casual Chic";
$lang['choose_display'] = "Choose products to display";

# HOME
$lang['notification']   = "Notification";
$lang['carousel']       = "Carousel";
$lang['quotes']         = "Quotes";
$lang['content']        = "Content";
$lang['paragraph']		= "Paragraph";
$lang['image_link']		= "Image link";

# ABOUT
$lang['advantage']      = "Advantage";
$lang['our_values']     = "Our Values";
$lang['beach_shop']     = "The Beach Shop";

# CONTACT
$lang['messages']       = "Messages";
$lang['branch']         = "Branch";
$lang['address']        = "Address";
$lang['sender']         = "Sender";
$lang['email']          = "E-Mail";
$lang['subject']        = "Subject";
$lang['date']           = "Date";
$lang['from']           = "From";
$lang['read_msg']       = "Read Message";
$lang['map_address'] 	= "Address for Map (Google Maps)";

# SUBSCRIBE
$lang['subscribers']    = "Subscribers";

# USER
$lang['user_manage']    = "User Management";
$lang['users']          = "Users";
$lang['user']           = "User";
$lang['full_name']      = "Full Name";
$lang['level']          = "Level";
$lang['password']       = "Password";
$lang['c_password']     = "Confirm Password";
$lang['show_password']  = "Show Password";