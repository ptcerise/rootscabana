    <!-- Page header -->

    <!-- Main content starts here -->
    <main id="mainContent">
        <section class="container-fluid nopadding">
            <div class="container-fixed">
                <div class="col-xs-12">
                    <div class="row collection-types" onclick="window.location.href='<?= base_url() ?>product/category/men-basic'">
                        <div class="col-xs-12 col-sm-6 type-item" style="background-image: url('<?= base_url('assets/upload/category/'.$men_basic->category_image_sq); ?>');">
                            <h2>
                                <?php
                                    $name = str_replace("-", "_", $men_basic->category_name);
                                    echo ucfirst($this->lang->line($name));
                                ?>
                            </h2>
                        </div>
                        <div class="col-xs-12 col-sm-6 product-prev right">
                            <?php foreach($products_basic as $key=>$val){
                                if($key == 2){break;}
                                ?>
                            <div class="col-xs-12 col-sm-6 text-center font3 <?php if($key == 1){echo "hidden-xs";} ?>">
                                <div class="image" style="background-image: url(<?= base_url('assets/upload/product').'/'.$val->product_image; ?>);"></div>
                                <h4><?= $val->product_name; ?></h4>
                                <?php
                                if($lang == "" || $lang == "french"){
                                    $content = $val->product_content_fr;
                                }else{
                                    $content = $val->product_content_en;
                                }
                                ?>
                                <div class="card-desc" title="<?= str_replace('<p>','',str_replace('</p>','',$content)); ?>">
                                    <?= substr($content,0,60).".."; ?>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="row collection-types zero-margintop" onclick="window.location.href='<?= base_url() ?>product/category/men-print'">
                        <div class="col-xs-12 col-sm-6 type-item pull-right" style="background-image: url('<?= base_url('assets/upload/category/'.$men_print->category_image_sq); ?>');">
                            <h2>
                                <?php
                                    $name = str_replace("-", "_", $men_print->category_name);
                                    echo ucfirst($this->lang->line($name));
                                ?>
                            </h2>
                        </div>
                        <div class="col-xs-12 col-sm-6 product-prev left">
                            <?php foreach($products_print as $key=>$val){
                                if($key == 2){break;}
                                ?>
                            <div class="col-xs-12 col-sm-6 text-center font3 <?php if($key == 1){echo "hidden-xs";} ?>">
                                <div class="image" style="background-image: url(<?= base_url('assets/upload/product').'/'.$val->product_image; ?>);"></div>
                                <h4><?= $val->product_name; ?></h4>
                                <?php
                                if($lang == "" || $lang == "french"){
                                    $content = $val->product_content_fr;
                                }else{
                                    $content = $val->product_content_en;
                                }
                                ?>
                                <div class="card-desc" title="<?= str_replace('<p>','',str_replace('</p>','',$content)); ?>">
                                    <?= substr($content,0,60).".."; ?>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="row collection-types zero-margintop" onclick="window.location.href='<?= base_url() ?>product/category/men-beach-wear'">
                        <div class="col-xs-12 col-sm-6 type-item" style="background-image: url('<?= base_url('assets/upload/category/'.$men_beachwear->category_image_sq); ?>');">
                            <h2>
                                <?php
                                    $name = str_replace("-", " ", $men_beachwear->category_name);
                                    $x = explode(' ',$name);
                                    echo ucfirst($this->lang->line($x[1]."_".$x[2]));
                                ?>
                            </h2>
                        </div>
                        <div class="col-xs-12 col-sm-6 product-prev right">
                            <?php foreach($products_beachwear as $key=>$val){
                                if($key == 2){break;}
                                ?>
                            <div class="col-xs-12 col-sm-6 text-center font3 <?php if($key == 1){echo "hidden-xs";} ?>">
                                <div class="image" style="background-image: url(<?= base_url('assets/upload/product').'/'.$val->product_image; ?>);"></div>
                                <h4><?= $val->product_name; ?></h4>
                                <?php
                                if($lang == "" || $lang == "french"){
                                    $content = $val->product_content_fr;
                                }else{
                                    $content = $val->product_content_en;
                                }
                                ?>
                                <div class="card-desc" title="<?= str_replace('<p>','',str_replace('</p>','',$content)); ?>">
                                    <?= substr($content,0,60).".."; ?>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="row collection-types zero-margintop" onclick="window.location.href='<?= base_url() ?>product/category/men-casual-chic'">
                        <div class="col-xs-12 col-sm-6 type-item pull-right" style="background-image: url('<?= base_url('assets/upload/category/'.$men_casualchic->category_image_sq); ?>');">
                            <h2>
                                <?php
                                    $name = str_replace("-", " ", $men_casualchic->category_name);
                                    $x = explode(' ',$name);
                                    echo ucfirst($this->lang->line($x[1]."_".$x[2]));
                                ?>
                            </h2>
                        </div>
                        <div class="col-xs-12 col-sm-6 product-prev left">
                            <?php foreach($products_casualchic as $key=>$val){
                                if($key == 2){break;}
                                ?>
                            <div class="col-xs-12 col-sm-6 text-center font3 <?php if($key == 1){echo "hidden-xs";} ?>">
                                <div class="image" style="background-image: url(<?= base_url('assets/upload/product').'/'.$val->product_image; ?>);"></div>
                                <h4><?= $val->product_name; ?></h4>
                                <?php
                                if($lang == "" || $lang == "french"){
                                    $content = $val->product_content_fr;
                                }else{
                                    $content = $val->product_content_en;
                                }
                                ?>
                                <div class="card-desc" title="<?= str_replace('<p>','',str_replace('</p>','',$content)); ?>">
                                    <?= substr($content,0,60).".."; ?>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </main>
