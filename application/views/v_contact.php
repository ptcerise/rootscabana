<!-- Page header -->
    <header class="text-center">
        <div class="container-fluid nopadding paddingbottom0 mgtop-75">
            <div class="container-fixed">
                <div class="">
                    <div class="header-background" style="background-image: url('<?php echo base_url('assets/upload/contact/'.$banner->media_url) ?>');">
                        <h1 class="banner-caption"><?php echo $this->lang->line("contact_us"); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Main content starts here -->
    <main id="mainContent">
        <section class="paddingtop0">
            <div class="container-fluid nopadding paddingtop0">
            <div class="container-fixed aboutborder">
                <div class="">

                    <?php foreach( $branch as $key => $value ):
                        $x = $key+1;
                        if($x % 2 != 0){
                    ?>
                    <div class="separator-about shop container-fluid">
                        <div class="col-xs-12 col-sm-6 about-right content-box about-content shop" style="">
                            <div>
                                <h2 class="" data-animate="true" data-effect="fadeInLeftBig">
                                    <?php

                                            echo $value->media_title_fr;

                                    ?>
                                </h2>
                                <div class="text" data-animate="true" data-effect="fadeInUpBig">
                                    <?php

                                            echo $value->media_content_fr;

                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 about-left content-box about-image shop">
                            <img alt="image" src="<?= base_url('assets/upload/about/'.$value->media_url); ?>" class="img-responsive" data-animate="true" data-effect="fadeInRightBig">
                        </div>
                    </div>

                    <?php }else{ ?>
                    <div class="separator-about shop container-fluid">
                        <div class="col-xs-12 col-sm-6 about-left content-box about-content shop pull-right">
                            <div>
                                <h2 class="" data-animate="true" data-effect="fadeInRightBig">
                                    <?php

                                            echo $value->media_title_fr;

                                    ?>
                                </h2>
                                <div class="text" data-animate="true" data-effect="fadeInUpBig">
                                    <?php

                                            echo $value->media_content_fr;

                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 about-right content-box about-image shop">
                            <img alt="image" src="<?= base_url('assets/upload/about/'.$value->media_url); ?>" class="img-responsive" data-animate="true" data-effect="fadeInLeftBig">
                        </div>
                    </div>
                    <?php } ?>
                <?php endforeach; ?>
                    <div class="separator-about shop container-fluid">
                        <div class="col-xs-12 col-sm-6 content-box about-content shop last">
                            <div class="row" data-animate="true" data-effect="fadeInLeftBig">
                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 text-center">
                                    <!-- <p class="lead text-center"> -->

                                        <?php
                                            if($lang == "" || $lang == "french"){
                                                echo $content->general_content_fr;
                                            }else{
                                                echo $content->general_content_en;
                                            }
                                        ?>
                                    <!-- </p> -->
                                    <br>
                                </div>
                            </div>
                            <div id="message" class="col-xs 12" data-animate="true" data-effect="fadeInUpBig">
                                <div class="col-sm-12">
                                    <?php echo $this->session->userdata('notif_message') ?>
                                    <form method="post" action="<?php echo base_url() ?>contact/send">
                                        <div class="form-group">
                                            <label for="contactName"><?php echo $this->lang->line("name"); ?></label>
                                            <input type="text" class="form-control" id="contactName" name="contact_name" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="contactEmail"><?php echo $this->lang->line("email"); ?></label>
                                            <input type="email" class="form-control" id="contactEmail" name="contact_email" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="contactSubject"><?php echo $this->lang->line("subject"); ?></label>
                                            <input type="text" class="form-control" id="contactSubject" name="contact_subject" required>
                                        </div>
                                        <div class="form-group">
                                            <label class="label-message" for="contactMessage"><?php echo $this->lang->line("message"); ?></label>
                                            <textarea id="contactMessage" class="form-control materialize-textarea expanding" name="contact_message" row="1"></textarea>
                                        </div>
                                        <div class="text-center">
                                            <button id="contactSubmit" type="submit" class="btn btn-raised"><?php echo $this->lang->line("btn_submit"); ?></button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                        <div class="col-xs-12 col-sm-6 content-box about-content shop contact-map">

                            <div id="map" data-animate="true" data-effect="fadeInRightBig">
                                <?php echo $map['js']; ?>
                                <?php echo $map['html']; ?>
                            </div>
                            <br>
                        </div>
                    </div>
                </div>
            </div>

            </div>
        </section>

    </main>
