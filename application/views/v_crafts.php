<!-- Page header -->
    <header class="text-center">
        <div class="container-fluid nopadding paddingbottom0 mgtop-75">
            <div class="container-fixed">
                <div class="">
                    <div class="header-background" style="background-image: url('<?php echo base_url('assets/upload/crafts/'.$banner->media_url) ?>');">
                        <h1 class="banner-caption"><?php echo $this->lang->line("crafts"); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main id="mainContent">
        <section class="container-fluid">
            <div class="container-fixed">
                <div class="col-xs-12">
                    <div class="row">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url() ?>home"><?php echo ucfirst($this->lang->line("home")); ?></a></li>
                            <li class="active"><?php echo ucfirst($this->lang->line("crafts")); ?></li>
                        </ol>
                    </div>
                    <div class="crafts-content">
                        <?php foreach($crafts as $key=>$val){ ?>
                            <div class="col-sm-6 col-md-3 crafts-item crafts">
                            <?php
                            if($lang == "" || $lang == "french"){
                                $content = $val->crafts_spec_fr;
                            }else{
                                $content = $val->crafts_spec_en;
                            }
                            ?>
                            <article role="button" class="card text-center" data-toggle="tooltip" data-placement="bottom" title="<?= str_replace('<p>','',str_replace('</p>','',$content)); ?>">
                                <!-- <a role="button"> -->
                                    <div class="article-content">
                                        <figure class="">
                                            <img alt="<?php echo $val->crafts_name; ?>" src="<?php echo base_url('assets/upload/crafts/'.$val->crafts_image) ?>" class="img-responsive">
                                        </figure>
                                        <div class="card-title-block title-craft">
                                            <h2><?php echo $val->crafts_name; ?></h2>

                                            <div class="card-desc" title="<?= str_replace('<p>','',str_replace('</p>','',$content)); ?>">
                                                <p><?= substr(str_replace('<p>','',str_replace('</p>','',$content)),0,50).".."; ?></p>
                                            </div>
                                        </div>
                                    </div>
                                    <aside class="article-specs text-left">
                                        <div class="card-block">
                                            <h3 class="article-title"><?php echo $this->lang->line("spec"); ?></h3>
                                            <?php
                                                if($lang == "" || $lang == "french"){
                                                    echo $val->crafts_spec_fr;
                                                }else{
                                                    echo $val->crafts_spec_en;
                                                }
                                            ?>
                                        </div>
                                        <!-- <a role="button" class="btn btn-flat pull-right">Close</a> -->
                                    </aside>
                                <!-- </a> -->
                            </article>
                            </div>
                        <?php } ?>

                    </div>
                    <div class="row text-center">
                        <a href="#" id="showmore_craft" class="btn btn-raised"><?php echo $this->lang->line('btn_showmore'); ?></a>
                    </div>
                </div>
            </div>
        </section>
    </main>
