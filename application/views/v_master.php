<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Roots Cabana</title>
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url();?>assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url();?>assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url();?>assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url();?>assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url();?>assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url();?>assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url();?>assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url();?>assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url();?>assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url();?>assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url();?>assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url();?>assets/favicon/favicon-96x96.png">
    <link rel="shortcut icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo base_url();?>assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo base_url();?>assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Bootstrap -->
    <link href="https://fonts.googleapis.com/css?family=EB+Garamond|La+Belle+Aurore|Lato:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Nothing+You+Could+Do" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/dist/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/bjqs.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/jquery.bxslider.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <!-- Custom stylesheet -->
    <link href="<?php echo base_url();?>assets/css/main.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php $nav_lang = $this->session->userdata('fe_lang'); ?>

</head>
<body class="<?php echo $body_class; ?>">


<!-- <div id="topBanner" class="text-center">
    <?php
        $notif = $this->db->query("SELECT * FROM general WHERE general_page='all' AND general_section='notif'")->row();
    ?>
    <p><?php
        if($nav_lang == "" || $nav_lang == "french"){
            echo $notif->general_content_fr;
        }else{
            echo $notif->general_content_en;
        }
    ?></p>
    <a role="button"><i class="material-icons">&#xE14C;</i></a>
</div> -->
<nav class="navbar navbar-default">
    <div class="container visible-xs visible-sm">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo base_url(); ?>home">
                <figure>
                    <img src="<?php echo base_url(); ?>assets/img/logo_brown.jpg" alt="Roots Cabana logo">
                </figure>
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-left">
                <li class="dropdown">
                    <a href="<?php echo base_url(); ?>women" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <?php echo $this->lang->line("women"); ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>women"><?php echo $this->lang->line("all_coll"); ?></a></li>
                        <li><a href="<?php echo base_url(); ?>product/category/women-basic"><?php echo $this->lang->line("basic"); ?></a></li>
                        <li><a href="<?php echo base_url(); ?>product/category/women-print"><?php echo $this->lang->line("print"); ?></a></li>
                        <li><a href="<?php echo base_url(); ?>product/category/women-beach-wear"><?php echo $this->lang->line("beach_wear"); ?></a></li>
                        <li><a href="<?php echo base_url(); ?>product/category/women-casual-chic"><?php echo $this->lang->line("casual_chic"); ?></a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="<?php echo base_url(); ?>men" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->lang->line("men"); ?> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>men"><?php echo $this->lang->line("all_coll"); ?></a></li>
                        <li><a href="<?php echo base_url(); ?>product/category/men-basic"><?php echo $this->lang->line("basic"); ?></a></li>
                        <li><a href="<?php echo base_url(); ?>product/category/men-print"><?php echo $this->lang->line("print"); ?></a></li>
                        <li><a href="<?php echo base_url(); ?>product/category/men-beach-wear"><?php echo $this->lang->line("beach_wear"); ?></a></li>
                        <li><a href="<?php echo base_url(); ?>product/category/men-casual-chic"><?php echo $this->lang->line("casual_chic"); ?></a></li>
                    </ul>
                </li>
                <li><a href="<?php echo base_url(); ?>crafts"><?php echo $this->lang->line("crafts"); ?></a></li>
                <li><a href="<?php echo base_url(); ?>about"><?php echo $this->lang->line("about_us"); ?></a></li>
                <li><a href="<?php echo base_url(); ?>contact"><?php echo $this->lang->line("contact_us"); ?></a></li>
                <li>
                    <?php
                        if($nav_lang == "" || $nav_lang == "french"){
                    ?>
                        <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/english">EN</a>
                    <?php }else{ ?>
                        <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/french">FR</a>
                    <?php } ?>
                </li>
                <li class="dropdown nav-search"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="material-icons">&#xE8B6;</i></a>
                    <ul class="dropdown-menu">
                        <li>
                            <form class="form-inline" method="get" action="<?php echo base_url(); ?>product/search">
                                <div class="form-group">
                                    <label for="searchbox" class="sr-only"><?php echo $this->lang->line("search_box"); ?></label>
                                    <input type="text" class="form-control" id="seachbox" name="keyword" placeholder="<?php echo $this->lang->line('lets_search'); ?>" required>
                                </div>
                                <button type="submit" class="btn btn-flat"><?php echo $this->lang->line("search"); ?></button>
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <div class="container navbar-v3 hidden-xs hidden-sm visible-md visible-lg">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">

                <div class="col-xs-12">
                    <div class="nav-left text-left">
                        <div class="row">
                            <div class="col-sm-12 link-box navbar-nav">
                                <div class="row dropdown link-item-left hover-line">
                                    <a id="dropdownFemaleButton" href="#" data-target="#dropdownFemale" data-collapse-group='nav-group'>
                                        <?php echo $this->lang->line("women"); ?> <span class="caret"></span>
                                    </a>
                                    <div class="static-hoverline" style="">
                                    </div>
                                    <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                        <?php echo $this->lang->line("women"); ?> <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>women"><?php echo $this->lang->line("all_coll"); ?></a></li>
                                        <li><a href="<?php echo base_url(); ?>product/category/women-basic"><?php echo $this->lang->line("basic"); ?></a></li>
                                        <li><a href="<?php echo base_url(); ?>product/category/women-print"><?php echo $this->lang->line("print"); ?></a></li>
                                        <li><a href="<?php echo base_url(); ?>product/category/women-beach-wear"><?php echo $this->lang->line("beach_wear"); ?></a></li>
                                        <li><a href="<?php echo base_url(); ?>product/category/women-casual-chic"><?php echo $this->lang->line("casual_chic"); ?></a></li>
                                    </ul> -->
                                </div>

                                <div class="row dropdown link-item-left hover-line">
                                    <a id="dropdownMaleButton" href="#" data-target="#dropdownMale" data-collapse-group='nav-group'>
                                        <?php echo $this->lang->line("men"); ?> <span class="caret"></span>
                                    </a>
                                    <div class="static-hoverline" style="">
                                    </div>
                                    <!-- <a href="#" data-toggle="dropdown">
                                        <?php echo $this->lang->line("men"); ?> <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url(); ?>men"><?php echo $this->lang->line("all_coll"); ?></a></li>
                                        <li><a href="<?php echo base_url(); ?>product/category/men-basic"><?php echo $this->lang->line("basic"); ?></a></li>
                                        <li><a href="<?php echo base_url(); ?>product/category/men-print"><?php echo $this->lang->line("print"); ?></a></li>
                                        <li><a href="<?php echo base_url(); ?>product/category/men-beach-wear"><?php echo $this->lang->line("beach_wear"); ?></a></li>
                                        <li><a href="<?php echo base_url(); ?>product/category/men-casual-chic"><?php echo $this->lang->line("casual_chic"); ?></a></li>
                                    </ul> -->
                                </div>

                                <div class="row link-item-left hover-line">
                                    <a href="<?php echo base_url(); ?>crafts">
                                        <?php echo $this->lang->line("crafts"); ?></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="nav-center">
                        <a href="<?php echo base_url(); ?>home">
                            <figure class="text-center">
                                <img class="img-responsive" src="<?php echo base_url(); ?>assets/img/logo_brown.jpg" alt="Roots Cabana logo">
                            </figure>
                        </a>
                    </div>
                    <div class="nav-right text-right">
                        <div class="row">
                            <div class="col-sm-12 link-box">


                                <div class="row link-item-right hover-line">
                                    <a href="<?php echo base_url(); ?>contact">
                                        <?php echo $this->lang->line("contact_us"); ?></span>
                                    </a>
                                </div>

                                <div class="row link-item-right hover-line">
                                    <a href="<?php echo base_url(); ?>about">
                                        <?php echo $this->lang->line("about_us"); ?></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="lang-searchbox">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <?php
                                if($nav_lang == "" || $nav_lang == "french"){
                            ?>
                                <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/english">EN</a>
                            <?php }else{ ?>
                                <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/french">FR</a>
                            <?php } ?>
                        </li>
                        <li class="dropdown nav-search"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="material-icons">&#xE8B6;</i></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <form class="form-inline" method="get" action="<?php echo base_url(); ?>product/search">
                                        <div class="form-group">
                                            <label for="searchbox" class="sr-only"><?php echo $this->lang->line("search_box"); ?></label>
                                            <input type="text" class="form-control" id="seachbox" name="keyword" placeholder="<?php echo $this->lang->line('lets_search'); ?>" required>
                                        </div>
                                        <button type="submit" class="btn btn-flat"><?php echo $this->lang->line("search"); ?></button>
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>


            <!-- DROPDOWN WITH IMAGE -->
            <div id="dropdownFemale" class="col-xs-12">
                <div class="dropdown-category">
                        <?php
                            $collections = $this->Access->readtable("category","",array("category_name"=>"women-basic"))->row()->category_image_sq;
                            $basic = $this->Access->readtable("category","",array("category_name"=>"women-basic"))->row()->category_image_sq;
                            $print = $this->Access->readtable("category","",array("category_name"=>"women-print"))->row()->category_image_sq;
                            $beachwear = $this->Access->readtable("category","",array("category_name"=>"women-beach-wear"))->row()->category_image_sq;
                            $casualchic = $this->Access->readtable("category","",array("category_name"=>"women-casual-chic"))->row()->category_image_sq;
                        ?>
                        <br>
                        <div class="">
                            <div class="text-center box-category">
                                <a href="<?= base_url('women') ?>">

                                    <div class="allcollections" style="">
                                        <img class="img-responsive" title="<?= $this->lang->line('all_coll') ?>" alt="<?= $this->lang->line('all_coll') ?>" src="<?= base_url('assets/upload/category/'.$collections); ?>">
                                        <div class="thumb top" style="background-image: url(<?= base_url('assets/upload/category/'.$basic); ?>)">
                                        </div>
                                        <div class="thumb top right" style="background-image: url(<?= base_url('assets/upload/category/'.$print); ?>)">
                                        </div>
                                        <div class="thumb bottom" style="background-image: url(<?= base_url('assets/upload/category/'.$beachwear); ?>)">
                                        </div>
                                        <div class="thumb bottom right" style="background-image: url(<?= base_url('assets/upload/category/'.$casualchic); ?>)">
                                        </div>
                                    </div>
                                    <br>
                                    <?= $this->lang->line('all_coll') ?>
                                </a>
                            </div>
                            <div class="text-center box-category">
                                <a href="<?= base_url('product/category/women-basic') ?>">
                                    <img class="img-responsive" title="<?= $this->lang->line('basic') ?>" alt="<?= $this->lang->line('basic') ?>" src="<?= base_url('assets/upload/category/'.$basic); ?>"><br>
                                    <?= $this->lang->line('basic') ?>
                                </a>
                            </div>
                            <div class="text-center box-category">
                                <a href="<?= base_url('product/category/women-print') ?>">
                                    <img class="img-responsive" title="<?= $this->lang->line('print') ?>" alt="<?= $this->lang->line('print') ?>" src="<?= base_url('assets/upload/category/'.$print); ?>"><br>
                                    <?= $this->lang->line('print') ?>
                                </a>
                            </div>
                            <div class="text-center box-category">
                                <a href="<?= base_url('product/category/women-beach-wear') ?>">
                                    <img class="img-responsive" title="<?= $this->lang->line('beach_wear') ?>" alt="<?= $this->lang->line('beach_wear') ?>" src="<?= base_url('assets/upload/category/'.$beachwear); ?>"><br>
                                    <?= $this->lang->line('beach_wear') ?>
                                </a>
                            </div>
                            <div class="text-center box-category">
                                <a href="<?= base_url('product/category/women-casual-chic') ?>">
                                    <img class="img-responsive" title="<?= $this->lang->line('casual_chic') ?>" alt="<?= $this->lang->line('casual_chic') ?>" src="<?= base_url('assets/upload/category/'.$casualchic); ?>"><br>
                                    <?= $this->lang->line('casual_chic') ?>
                                </a>
                            </div>
                        </div>

                    </div>
            </div>
            <div id="dropdownMale" class="col-xs-12">
                <div class="dropdown-category">
                        <?php
                            $collections = $this->Access->readtable("category","",array("category_name"=>"men-basic"))->row()->category_image_sq;
                            $basic = $this->Access->readtable("category","",array("category_name"=>"men-basic"))->row()->category_image_sq;
                            $print = $this->Access->readtable("category","",array("category_name"=>"men-print"))->row()->category_image_sq;
                            $beachwear = $this->Access->readtable("category","",array("category_name"=>"men-beach-wear"))->row()->category_image_sq;
                            $casualchic = $this->Access->readtable("category","",array("category_name"=>"men-casual-chic"))->row()->category_image_sq;
                        ?>
                        <br>
                        <div class="">
                            <div class="text-center box-category">
                                <a href="<?= base_url('men') ?>">
                                    <!-- <img class="img-responsive" title="<?= $this->lang->line('all_coll') ?>" alt="<?= $this->lang->line('all_coll') ?>" src="<?= base_url('assets/upload/category/'.$collections); ?>"> -->
                                    <div class="allcollections" style="">
                                        <img class="img-responsive" title="<?= $this->lang->line('all_coll') ?>" alt="<?= $this->lang->line('all_coll') ?>" src="<?= base_url('assets/upload/category/'.$collections); ?>">
                                        <div class="thumb top" style="background-image: url(<?= base_url('assets/upload/category/'.$basic); ?>)">
                                        </div>
                                        <div class="thumb top right" style="background-image: url(<?= base_url('assets/upload/category/'.$print); ?>)">
                                        </div>
                                        <div class="thumb bottom" style="background-image: url(<?= base_url('assets/upload/category/'.$beachwear); ?>)">
                                        </div>
                                        <div class="thumb bottom right" style="background-image: url(<?= base_url('assets/upload/category/'.$casualchic); ?>)">
                                        </div>
                                    </div>
                                    <br>
                                    <?= $this->lang->line('all_coll') ?>
                                </a>
                            </div>
                            <div class="text-center box-category">
                                <a href="<?= base_url('product/category/men-basic') ?>">
                                    <img class="img-responsive" title="<?= $this->lang->line('basic') ?>" alt="<?= $this->lang->line('basic') ?>" src="<?= base_url('assets/upload/category/'.$basic); ?>"><br>
                                    <?= $this->lang->line('basic') ?>
                                </a>
                            </div>
                            <div class="text-center box-category">
                                <a href="<?= base_url('product/category/men-print') ?>">
                                    <img class="img-responsive" title="<?= $this->lang->line('print') ?>" alt="<?= $this->lang->line('print') ?>" src="<?= base_url('assets/upload/category/'.$print); ?>"><br>
                                    <?= $this->lang->line('print') ?>
                                </a>
                            </div>
                            <div class="text-center box-category">
                                <a href="<?= base_url('product/category/men-beach-wear') ?>">
                                    <img class="img-responsive" title="<?= $this->lang->line('beach_wear') ?>" alt="<?= $this->lang->line('beach_wear') ?>" src="<?= base_url('assets/upload/category/'.$beachwear); ?>"><br>
                                    <?= $this->lang->line('beach_wear') ?>
                                </a>
                            </div>
                            <div class="text-center box-category">
                                <a href="<?= base_url('product/category/men-casual-chic') ?>">
                                    <img class="img-responsive" title="<?= $this->lang->line('casual_chic') ?>" alt="<?= $this->lang->line('casual_chic') ?>" src="<?= base_url('assets/upload/category/'.$casualchic); ?>"><br>
                                    <?= $this->lang->line('casual_chic') ?>
                                </a>
                            </div>
                        </div>

                    </div>
            </div>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <div class="container-fluid navbar-fixed-scroll navbar-fixed-top hidden-xs hidden-sm visible-md visible-lg">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-3">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="nav-logo-left row">
                        <a href="<?php echo base_url(); ?>home">
                            <figure class="text-center">
                                <img class="img-responsive" src="<?php echo base_url(); ?>assets/img/logo_brown.jpg" alt="Roots Cabana logo">
                            </figure>
                        </a>
                    </div>
                    <div class="col-xs-9 nav-left2 text-center">
                        <div class="row">
                            <div class="col-sm-2 link-box navbar-nav">
                                <div class="row dropdown hover-line">
                                    <a id="dropdownFemaleButton2" role="button" data-toggle="collapse" data-target="#dropdownFemale2" data-collapse-group='nav-group'>
                                        <?php echo $this->lang->line("women"); ?> <span class="caret"></span>
                                    </a>
                                    <div class="static-hoverline" style="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 link-box navbar-nav">
                                <div class="row dropdown hover-line">
                                    <a id="dropdownMaleButton2" role="button" data-toggle="collapse" data-target="#dropdownMale2" data-collapse-group='nav-group'>
                                        <?php echo $this->lang->line("men"); ?> <span class="caret"></span>
                                    </a>
                                    <div class="static-hoverline" style="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 link-box navbar-nav">
                                <div class="row hover-line">
                                    <a href="<?php echo base_url(); ?>crafts">
                                        <?php echo $this->lang->line("crafts"); ?></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3 link-box navbar-nav">
                                <div class="row hover-line">
                                    <a href="<?php echo base_url(); ?>about">
                                        <?php echo $this->lang->line("about_us"); ?></span>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3 link-box navbar-nav">
                                <div class="row hover-line">
                                    <a href="<?php echo base_url(); ?>contact">
                                        <?php echo $this->lang->line("contact_us"); ?></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <?php
                                if($nav_lang == "" || $nav_lang == "french"){
                            ?>
                                <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/english">EN</a>
                            <?php }else{ ?>
                                <a href="<?php echo base_url(); ?>LanguageSwitcher/switchLang/french">FR</a>
                            <?php } ?>
                        </li>
                        <li class="dropdown nav-search"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="material-icons">&#xE8B6;</i></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <form class="form-inline" method="get" action="<?php echo base_url(); ?>product/search">
                                        <div class="form-group">
                                            <label for="searchbox" class="sr-only"><?php echo $this->lang->line("search_box"); ?></label>
                                            <input type="text" class="form-control" id="seachbox" name="keyword" placeholder="<?php echo $this->lang->line('lets_search'); ?>" required>
                                        </div>
                                        <button type="submit" class="btn btn-flat"><?php echo $this->lang->line("search"); ?></button>
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>


            <div class="col-sm-8 col-sm-offset-2">
                <div id="dropdownFemale2" class="collapse row dropdown-category">
                    <?php
                        $collections = $this->Access->readtable("category","",array("category_name"=>"women-basic"))->row()->category_image_sq;
                        $basic = $this->Access->readtable("category","",array("category_name"=>"women-basic"))->row()->category_image_sq;
                        $print = $this->Access->readtable("category","",array("category_name"=>"women-print"))->row()->category_image_sq;
                        $beachwear = $this->Access->readtable("category","",array("category_name"=>"women-beach-wear"))->row()->category_image_sq;
                        $casualchic = $this->Access->readtable("category","",array("category_name"=>"women-casual-chic"))->row()->category_image_sq;
                    ?>
                    <br>
                    <div class="row">
                        <div class="text-center box-category">
                            <a href="<?= base_url('women') ?>">
                                <!-- <img class="img-responsive" title="<?= $this->lang->line('all_coll') ?>" alt="<?= $this->lang->line('all_coll') ?>" src="<?= base_url('assets/upload/category/'.$collections); ?>"><br>
                                <?= $this->lang->line('all_coll') ?> -->
                                <div class="allcollections" style="">
                                    <img class="img-responsive" title="<?= $this->lang->line('all_coll') ?>" alt="<?= $this->lang->line('all_coll') ?>" src="<?= base_url('assets/upload/category/'.$collections); ?>">
                                    <div class="thumb top" style="background-image: url(<?= base_url('assets/upload/category/'.$basic); ?>)">
                                    </div>
                                    <div class="thumb top right" style="background-image: url(<?= base_url('assets/upload/category/'.$print); ?>)">
                                    </div>
                                    <div class="thumb bottom" style="background-image: url(<?= base_url('assets/upload/category/'.$beachwear); ?>)">
                                    </div>
                                    <div class="thumb bottom right" style="background-image: url(<?= base_url('assets/upload/category/'.$casualchic); ?>)">
                                    </div>
                                </div>
                                <br>
                                <?= $this->lang->line('all_coll') ?>
                            </a>
                        </div>
                        <div class="text-center box-category">
                            <a href="<?= base_url('product/category/women-basic') ?>">
                                <img class="img-responsive" title="<?= $this->lang->line('basic') ?>" alt="<?= $this->lang->line('basic') ?>" src="<?= base_url('assets/upload/category/'.$basic); ?>"><br>
                                <?= $this->lang->line('basic') ?>
                            </a>
                        </div>
                        <div class="text-center box-category">
                            <a href="<?= base_url('product/category/women-print') ?>">
                                <img class="img-responsive" title="<?= $this->lang->line('print') ?>" alt="<?= $this->lang->line('print') ?>" src="<?= base_url('assets/upload/category/'.$print); ?>"><br>
                                <?= $this->lang->line('print') ?>
                            </a>
                        </div>
                        <div class="text-center box-category">
                            <a href="<?= base_url('product/category/women-beach-wear') ?>">
                                <img class="img-responsive" title="<?= $this->lang->line('beach_wear') ?>" alt="<?= $this->lang->line('beach_wear') ?>" src="<?= base_url('assets/upload/category/'.$beachwear); ?>"><br>
                                <?= $this->lang->line('beach_wear') ?>
                            </a>
                        </div>
                        <div class="text-center box-category">
                            <a href="<?= base_url('product/category/women-casual-chic') ?>">
                                <img class="img-responsive" title="<?= $this->lang->line('casual_chic') ?>" alt="<?= $this->lang->line('casual_chic') ?>" src="<?= base_url('assets/upload/category/'.$casualchic); ?>"><br>
                                <?= $this->lang->line('casual_chic') ?>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-sm-8 col-sm-offset-2">
                <div id="dropdownMale2" class="collapse row dropdown-category">
                    <?php
                        $collections = $this->Access->readtable("category","",array("category_name"=>"men-basic"))->row()->category_image_sq;
                        $basic = $this->Access->readtable("category","",array("category_name"=>"men-basic"))->row()->category_image_sq;
                        $print = $this->Access->readtable("category","",array("category_name"=>"men-print"))->row()->category_image_sq;
                        $beachwear = $this->Access->readtable("category","",array("category_name"=>"men-beach-wear"))->row()->category_image_sq;
                        $casualchic = $this->Access->readtable("category","",array("category_name"=>"men-casual-chic"))->row()->category_image_sq;
                    ?>
                    <br>
                    <div class="row">
                        <div class="text-center box-category">
                            <a href="<?= base_url('men') ?>">
                                <!-- <img class="img-responsive" title="<?= $this->lang->line('all_coll') ?>" alt="<?= $this->lang->line('all_coll') ?>" src="<?= base_url('assets/upload/category/'.$collections); ?>"><br>
                                <?= $this->lang->line('all_coll') ?> -->
                                <div class="allcollections" style="">
                                    <img class="img-responsive" title="<?= $this->lang->line('all_coll') ?>" alt="<?= $this->lang->line('all_coll') ?>" src="<?= base_url('assets/upload/category/'.$collections); ?>">
                                    <div class="thumb top" style="background-image: url(<?= base_url('assets/upload/category/'.$basic); ?>)">
                                    </div>
                                    <div class="thumb top right" style="background-image: url(<?= base_url('assets/upload/category/'.$print); ?>)">
                                    </div>
                                    <div class="thumb bottom" style="background-image: url(<?= base_url('assets/upload/category/'.$beachwear); ?>)">
                                    </div>
                                    <div class="thumb bottom right" style="background-image: url(<?= base_url('assets/upload/category/'.$casualchic); ?>)">
                                    </div>
                                </div>
                                <br>
                                <?= $this->lang->line('all_coll') ?>
                            </a>
                        </div>
                        <div class="text-center box-category">
                            <a href="<?= base_url('product/category/men-basic') ?>">
                                <img class="img-responsive" title="<?= $this->lang->line('basic') ?>" alt="<?= $this->lang->line('basic') ?>" src="<?= base_url('assets/upload/category/'.$basic); ?>"><br>
                                <?= $this->lang->line('basic') ?>
                            </a>
                        </div>
                        <div class="text-center box-category">
                            <a href="<?= base_url('product/category/men-print') ?>">
                                <img class="img-responsive" title="<?= $this->lang->line('print') ?>" alt="<?= $this->lang->line('print') ?>" src="<?= base_url('assets/upload/category/'.$print); ?>"><br>
                                <?= $this->lang->line('print') ?>
                            </a>
                        </div>
                        <div class="text-center box-category">
                            <a href="<?= base_url('product/category/men-beach-wear') ?>">
                                <img class="img-responsive" title="<?= $this->lang->line('beach_wear') ?>" alt="<?= $this->lang->line('beach_wear') ?>" src="<?= base_url('assets/upload/category/'.$beachwear); ?>"><br>
                                <?= $this->lang->line('beach_wear') ?>
                            </a>
                        </div>
                        <div class="text-center box-category">
                            <a href="<?= base_url('product/category/men-casual-chic') ?>">
                                <img class="img-responsive" title="<?= $this->lang->line('casual_chic') ?>" alt="<?= $this->lang->line('casual_chic') ?>" src="<?= base_url('assets/upload/category/'.$casualchic); ?>"><br>
                                <?= $this->lang->line('casual_chic') ?>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
</nav>
<?php echo $content; ?>

<!-- Page footer starts here -->
    <footer>
        <div class="container">
            <!-- <div role="navigation" class="row">
                <ul class="nav nav-footer text-center">
                    <li><a href="<?php echo base_url(); ?>women"><?php echo $this->lang->line("women"); ?></a></li>
                    <li><a href="<?php echo base_url(); ?>men"><?php echo $this->lang->line("men"); ?></a></li>
                    <li><a href="<?php echo base_url(); ?>crafts"><?php echo $this->lang->line("crafts"); ?></a></li>
                    <li><a href="<?php echo base_url(); ?>about"><?php echo $this->lang->line("about_us"); ?></a></li>
                    <li><a href="<?php echo base_url(); ?>contact"><?php echo $this->lang->line("contact_us"); ?></a></li>
                </ul>
            </div> -->
            <br><br>
            <div class="row">

                <figure class="col-md-2 footer-logo">
                  <a href="<?php echo base_url(); ?>">
                    <img class="img img-responsive" src="<?php echo base_url(); ?>assets/img/logo_brown.jpg" alt="Roots Cabana logo">
                  </a>
                </figure>

                <article id="subscribe" class="col-md-7">
                <?php echo $this->session->userdata('subscribtion'); ?>
                    <p class="shout"><?php echo $this->lang->line("subs_goodness"); ?></p>
                    <form method="post" action="<?php echo base_url() ?>backend/subscribe/register" class="form-inline">
                        <div class="form-group">
                            <label for="newsLetter" class="sr-only">Email</label>
                            <input type="email" class="form-control" id="newsLetter" placeholder="johndoe@example.com" name="subscribe" required>
                        </div>
                        <button type="submit" class="btn btn-raised"><?php echo $this->lang->line("btn_subs"); ?></button>
                    </form>
                    <p class="wisper hidden-xs"><?php echo $this->lang->line("we_promise"); ?></p>
                </article>
                <article id="social" class="col-md-3">
                    <?php
                        $facebook = $this->db->query("SELECT * FROM media WHERE media_section='link_facebook'")->row()->media_url;
                        $twitter = $this->db->query("SELECT * FROM media WHERE media_section='link_twitter'")->row()->media_url;
                        $instagram = $this->db->query("SELECT * FROM media WHERE media_section='link_instagram'")->row()->media_url;
                        $pinterest = $this->db->query("SELECT * FROM media WHERE media_section='link_pinterest'")->row()->media_url;
                        $email = $this->db->query("SELECT * FROM media WHERE media_section='link_email'")->row()->media_url;
                    ?>
                    <p class="shout"><?php echo $this->lang->line("in_touch"); ?></p>

                    <a href="<?php echo $twitter; ?>" target="_blank" class="social-media-icons">
                        <div class="social-media twitter"></div>
                    </a>
                    <a href="<?php echo $facebook; ?>" target="_blank" class="social-media-icons">
                        <div class="social-media facebook"></div>
                    </a>
                    <a href="<?php echo $pinterest; ?>" target="_blank" class="social-media-icons">
                        <div class="social-media pinterest"></div>
                    </a>
                    <a href="<?php echo $instagram; ?>" target="_blank" class="social-media-icons">
                        <div class="social-media instagram"></div>
                    </a>
                </article>
            </div>
            <div class="row copyrights text-center">
                <p>Copyrights ©Roots Cabana <?= date('Y') ?>. | A concept by </p>
                <a href="http://cerise-concept.com/" target="_blank"><img src="<?php echo base_url(); ?>assets/img/icons/icon_cerise.svg" alt="Cerise concept logo"></a>
            </div>
        </div>

    </footer>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>assets/dist/js/bootstrap.min.js"></script>
    <!-- Load custom scripts -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.bxslider.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/scripts.js"></script>
    <script src="<?php echo base_url(); ?>assets/dist/js/gmaps.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/scripts.min.js"></script>

    <?php if($body_class == 'home'): ?>
        <script type="text/javascript">
            $(document).ready(function(){
                $(".instagram").find("figure.instagram-image").hide();
                var tagname = "<?= '#'.$ig_tag; ?>";

                $(".instagram").find("figure[data-tag*='"+tagname+"']").show();
                $(".instagram").find("figure.instagram-image:visible:gt(5)").hide();
                $(".instagram").find("figure.instagram-image:hidden").remove();
            });
            //
            // $(document).ready(function(){
            //   // $("#scrollDown").css("width",($(window).width()-58)+"px");
            //
            //   if($(window).width() <= 768){
            //     $("div.padding-video").css("height",($(window).height()-65)+"px");
            //   }else if($(window).width() <= 1280){
            //     $("div.padding-video").css("height",($(window).height()-85)+"px");
            //   }else if($(window).width() >= 1600){
            //     $("div.padding-video").css("height",($(window).height()-250)+"px")
            //   }else{
            //     $("div.padding-video").css("height",($(window).height()-90)+"px");
            //   }
            //
            // });

        </script>
    <?php endif; ?>

</body>

</html>
