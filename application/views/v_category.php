
    <main id="mainContent">
        <section>
            <div class="container">
                <div class="col-xs-12">

                    <div class="product-list">
                        <?php foreach ($products as $key => $value) :?>
                        <div class="col-xs-12 col-sm-6 col-md-4 product-item">
                            <article class="card text-center">
                                <a href="<?php echo base_url("product/detail/".$value->product_id."-".url_title(strtolower($value->product_name))) ?>">
                                    <div class="card-img paddingbottom0" style="background-image: url(<?php echo base_url("assets/upload/product/".$value->product_image) ?>)">
                                    </div>
                                    <div class="card-title-block title-item paddingtop0 font3">
                                        <h2 style="overflow: hidden;">

                                            <?php
                                                echo $value->product_name;
                                            ?>
                                            
                                        </h2>
                                        <?php
                                        if($lang == "" || $lang == "french"){
                                            $content = $value->product_content_fr;
                                        }else{
                                            $content = $value->product_content_en;
                                        }
                                        ?>
                                        <div class="card-desc" title="<?= str_replace('<p>','',str_replace('</p>','',$content)); ?>">
                                            <?= substr($content,0,60).".."; ?>
                                        </div>
                                    </div>
                                </a>
                            </article>
                        </div>
                        <?php endforeach;?>
                    </div>
                    <div class="row text-center">
                        <a href="#" id="showmore_product" class="btn btn-raised">
                            <?php echo $this->lang->line("btn_showmore"); ?>
                        </a>
                    </div>
                </div>
            </div>
        </section>
    </main>
