<!-- Main content starts here -->
    <main id="mainContent">
        <article class="main-article-single">
            <div class="container">
                <div class="col-xs-12">
                    <div class="row">
                        <ol class="breadcrumb">
                            <li><a href="<?php echo base_url() ?>home"><?= ucwords($this->lang->line('home')) ?></a></li>
                            <li>
                                <a href="<?php echo base_url() ?><?php $cname = explode("-",$product->product_category);echo $cname[0]; ?>">
                                    <?php $cname = explode("-",$product->product_category);echo ucwords($this->lang->line($cname[0])); ?>
                                </a>
                            </li>
                            <li><a href="<?php echo base_url() ?>product/category/<?php echo $product->product_category; ?>">

                                <?php
                                        $z = $cname[0]."_".$cname[1];

                                        if($cname[1] == "beach" || $cname[1] == "casual"){
                                          echo ucwords($this->lang->line($cname[1]."_".$cname[2]));
                                        }else{
                                          echo ucwords($this->lang->line($z));
                                        }

                                ?>

                            </a></li>
                            <li class="active"><?php echo ucwords($product->product_name); ?></li>
                        </ol>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div id="itemSingleCarousel" class="carousel slide" data-ride="carousel">
                            <!-- Wrapper for slides -->
                                <div class="carousel-inner text-center" role="listbox">
                                    <div class="item active" data-item-no="0">
                                        <img src="<?php echo base_url('assets/upload/product/'.$product->product_image) ?>" class="img-responsive">
                                    </div>
                                    <?php foreach ($product_detail as $key => $value) { ?>
                                    <div class="item" data-item-no="<?= $key+1; ?>">
                                        <img src="<?php echo base_url('assets/upload/product/detail/'.$value->detail_image) ?>" class="img-responsive">
                                    </div>
                                    <?php } ?>
                                    <a class="left carousel-control" href="#itemSingleCarousel" role="button" data-slide="prev">
                                        <img class="img-responsive" src="<?= base_url('assets/img/left.png'); ?>">
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#itemSingleCarousel" role="button" data-slide="next">
                                        <img class="img-responsive" src="<?= base_url('assets/img/right.png'); ?>">
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                            <!-- Variant -->
                            <aside id="itemCarouselIndicator" class="variants article-block text-center">
                                <div class="row">
                                    <div class="col-xs-4 col-sm-4 variant-item">
                                        <a href="#" role="button" data-target="#itemSingleCarousel" data-slide-to="0">
                                            <img src="<?php echo base_url('assets/upload/product/'.$product->product_image) ?>" class="img-responsive">
                                        </a>
                                    </div>
                                    <?php foreach ($product_detail as $key => $value) { ?>
                                    <div class="col-xs-4 col-sm-4 variant-item">
                                        <a href="#" role="button" data-target="#itemSingleCarousel" data-slide-to="<?php echo $key+1; ?>">
                                            <img src="<?php echo base_url('assets/upload/product/detail/'.$value->detail_image) ?>" class="img-responsive">
                                        </a>
                                    </div>
                                    <?php } ?>
                                </div>
                            </aside>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-sm-offset-1 col-md-6 col-md-offset-1 main-item-single">
                            <div class="article-block item-single"><br>
                                <h1 class="font3"><?php echo $product->product_name; ?></h1>
                            </div>
                            <div class="article-block item-single">
                                <br>
                                <h3 class="font3 bold"><?= $this->lang->line('spec'); ?></h3>
                                <br>
                                <?php
                                    if($lang == "" || $lang == "french"){
                                        echo $product->product_content_fr;
                                    }else{
                                        echo $product->product_content_en;
                                    }
                                ?>
                            </div>
                            <div class="article-block item-single">
                                <br>
                                <h3 class="font3 bold"><?= $this->lang->line('available_color'); ?></h3>
                                <br>
                                <div class="item-colors">
                                    <?php
                                        foreach($product_color as $key => $value):
                                    ?>
                                        <div class="color" style="background-color: <?= $value->color_code; ?>"></div>
                                    <?php endforeach; ?>
                                </div>

                            </div>
                            <div class="article-block item-single">
                                <a id="shareButton" role="button" class="btn btn-raised"><?php echo $this->lang->line("btn_share"); ?></a>
                                <div id="shareLinks">
                                    <?php
                                        $share_link = urlencode("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
                                        $name = $product->product_name;
                                        $image = base_url('assets/upload/product/'.$product->product_image);
                                        $share_image = urlencode($image);
                                        $share_title = urlencode($name);
                                    ?>

                                    <a href="http://twitter.com/intent/tweet/?url=<?php echo $share_link; ?>" target="_blank" class="social-media-icons">
                                        <div class="social-media twitter"></div>
                                    </a>

                                    <a href="http://www.facebook.com/sharer/sharer.php?p[url]=<?php echo $share_link; ?>&p[title]=<?php echo $share_title; ?>" target="_blank" class="social-media-icons">
                                        <div class="social-media facebook"></div>
                                    </a>

                                    <a href="https://pinterest.com/pin/create/button/?url=<?php echo $share_link; ?>&media=<?php echo $share_image; ?>&description=<?php echo $share_title; ?>" target="_blank" class="social-media-icons">
                                        <div class="social-media pinterest"></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        <section>
            <div class="container">
                <div class="col-xs-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <p class="also-like font3"><?php echo $this->lang->line("also_like"); ?></p>
                        </div>
                    </div>
                    <div class="row">
                        <?php foreach ($other_product as $key => $value) { ?>
                        <div class="col-xs-6 col-sm-6 col-md-4">
                            <article class="card text-center other-product">

                                    <div class="card-img" style="background-image: url(<?php echo base_url('assets/upload/product/'.$value->product_image); ?>)">
                                        <!-- <img src="<?php echo base_url('assets/upload/product/'.$value->product_image); ?>" class="img-responsive img-hover"> -->
                                    </div>
                                    <!-- <div class="card-title-block title-item">
                                        <h2 style="overflow: hidden;">
                                            <strong>
                                            <?php
                                                echo $value->product_name;
                                            ?>
                                            </strong>
                                        </h2>
                                    </div> -->

                                <div class="wrapper-voir text-center">
                                    <a class="btn btn-raised" href="<?php echo base_url("product/detail/".$value->product_id."-".url_title(strtolower($value->product_name))); ?>"><?= $this->lang->line('btn_see') ?></a>
                                </div>
                            </article>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
    </main>
