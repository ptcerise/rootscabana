
    <header class="text-center">
        <div class="container-fluid nopadding paddingbottom0">
            <div class="submenu container-fixed">
                <div class=" col-xs-12 col-sm-4 col-sm-offset-4 text-center">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4">
                            <a class="<?php if($submenu == "handmade"){echo 'active';} ?>" href="<?= base_url('about/handmade') ?>"><?php echo $this->lang->line("handmade"); ?></a>
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <a class="<?php if($submenu == "brand"){echo 'active';} ?>" href="<?= base_url('about/brand') ?>"><?php echo $this->lang->line("brand"); ?></a>
                        </div>
                        <div class="col-xs-4 col-sm-4">
                            <a class="<?php if($submenu == "shop"){echo 'active';} ?>" href="<?= base_url('about/shop') ?>"><?php echo $this->lang->line("shop"); ?></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fixed">
                <div class="">
                    <div class="header-background" style="background-image: url('<?php echo base_url() ?>assets/upload/about/<?php echo $banner->media_url; ?>');">
                        <h1 class="banner-caption"><?php echo $this->lang->line($submenu); ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!-- Main content starts here -->
    <main id="mainContent">
        <section class="paddingtop0">
            <div class="container-fluid nopadding paddingtop0">
            <div class="container-fixed aboutborder">
                <div class="">

                    <?php foreach( $contents as $key => $value ):
                        $x = $key+1;
                        if($x % 2 != 0){
                    ?>
                    <div class="separator-about <?= $submenu ?> container-fluid">
                        <div class="col-xs-12 col-sm-6 content-box about-right about-content <?= $submenu ?>" style="">
                            <div>
                                <h2 class="" data-animate="true" data-effect="fadeInLeftBig">
                                    <?php
                                        if($lang == "" || $lang == "french"){
                                            echo $value->media_title_fr;
                                        }else{
                                            echo $value->media_title_en;
                                        }
                                    ?>
                                </h2>
                                <div class="text" data-animate="true" data-effect="fadeInUpBig">
                                    <?php
                                        if($lang == "" || $lang == "french"){
                                            echo $value->media_content_fr;
                                        }else{
                                            echo $value->media_content_en;
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 content-box about-left about-image <?= $submenu ?>">
                            <img alt="image" src="<?= base_url('assets/upload/about/'.$value->media_url); ?>" class="img-responsive" data-animate="true" data-effect="fadeInRightBig">
                        </div>
                    </div>

                    <?php }else{ ?>
                    <div class="separator-about <?= $submenu ?> container-fluid">
                        <div class="col-xs-12 col-sm-6 content-box about-left about-content <?= $submenu ?> pull-right">
                            <div>
                                <h2 class="" data-animate="true" data-effect="fadeInRightBig">
                                    <?php
                                        if($lang == "" || $lang == "french"){
                                            echo $value->media_title_fr;
                                        }else{
                                            echo $value->media_title_en;
                                        }
                                    ?>
                                </h2>
                                <div class="text" data-animate="true" data-effect="fadeInUpBig">
                                    <?php
                                        if($lang == "" || $lang == "french"){
                                            echo $value->media_content_fr;
                                        }else{
                                            echo $value->media_content_en;
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 content-box about-right about-image <?= $submenu ?>">
                            <img alt="image" src="<?= base_url('assets/upload/about/'.$value->media_url); ?>" class="img-responsive" data-animate="true" data-effect="fadeInLeftBig">
                        </div>
                    </div>
                    <?php } ?>
                <?php endforeach; ?>
                </div>
            </div>
            </div>
        </section>
    </main>
