
    <!-- Top page main navigation -->
    <!-- Hero header starts here -->
    <header class="text-center">
        <!-- Hero header carousel -->
        <section id="hero-carousel" class="video-banner text-center">
        <a id="playButton" role="button" onclick="play_video();">
            <img class="img img-responsive" src="<?= base_url('assets/img/play.png'); ?>">
        </a>
            <video id="video-banner" loop="">
               <?php
                    $y = explode('.',$video->media_url);
                    $ext = $y[1];
                ?>
                <source src="<?= base_url('assets/upload/video/'.$video->media_url); ?>" type="video/<?= strtolower($ext); ?>">
            </video>
        </section>

    </header>
    <div class="padding-video"></div>
    <!-- Main content starts here -->
    <main id="mainContent">
        <div id="scrollDown" class="">
          <div>
            <button role="button" class="btn"><i class="material-icons">&#xE313;</i></a>
          </div>
        </div>
        <!-- TEMPLATE: Introduction module -->
        <section class="" id="mainContentSection">
        <br>
            <div class="container">
                <div class="row">
                    <div class=" col-xs-12 col-sm-10 col-sm-offset-1">
                        <article class="flat-card card-block">
                            <h3 class="card-title text-center bold">
                                <?php
                                    if($lang == "" || $lang == "french"){
                                        echo $content1->general_title_fr;
                                    }else{
                                        echo $content1->general_title_en;
                                    }
                                ?>
                            </h3>
                            <div class="text-center">
                                <?php
                                    if($lang == "" || $lang == "french"){
                                        echo $content1->general_content_fr;
                                    }else{
                                        echo $content1->general_content_en;
                                    }
                                ?>
                            </div>
                        </article>
                    </div>
                </div>
            </div>

        </section>

        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="">
                        <div class="col-xs-12 col-sm-6 imagelink">
                            <div class="imagelink-square" style="background-image: url(<?= base_url('assets/upload/home/'.$imagelink1->media_url); ?>)">
                                <div class="imagelink-text text-center">
                                    <p><?= ucfirst($this->lang->line('collections')) ?></p>
                                    <h2><?= ucfirst($this->lang->line('men')) ?></h2>
                                </div>
                            </div>
                            <div class="wrapper-voir text-center">
                                <a class="btn btn-raised" href="<?=base_url('men')?>"><?= $this->lang->line('btn_see') ?></a>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 imagelink">
                            <div class="imagelink-square" style="background-image: url(<?= base_url('assets/upload/home/'.$imagelink2->media_url); ?>)">
                                <div class="imagelink-text text-center">
                                    <p><?= ucfirst($this->lang->line('collections')) ?></p>
                                    <h2><?= ucfirst($this->lang->line('women')) ?></h2>
                                </div>
                            </div>
                            <div class="wrapper-voir text-center">
                                <a class="btn btn-raised" href="<?=base_url('women')?>"><?= $this->lang->line('btn_see') ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="imagelink3">
                        <div class="col-xs-12 imagelink">
                            <div class="imagelink3-bg" style="background-image: url(<?= base_url('assets/upload/home/'.$imagelink3->media_url); ?>)">
                                <div class="imagelink-text text-center">
                                    <h2><?= ucfirst($this->lang->line('beach_shop')) ?></h2>
                                </div>
                            </div>
                            <div class="wrapper-voir text-center">
                                <a class="btn btn-raised" href="<?=base_url('contact')?>"><?= $this->lang->line('btn_see') ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<br>
        <div>
            <div class="container">
                <div class="row">
                    <div class=" col-xs-12 col-sm-10 col-sm-offset-1">
                        <article class="flat-card card-block">
                            <h3 class="card-title text-center bold">
                                <?php
                                    if($lang == "" || $lang == "french"){
                                        echo $content2->general_title_fr;
                                    }else{
                                        echo $content2->general_title_en;
                                    }
                                ?>
                            </h3>
                            <div class="text-center">
                                <?php
                                    if($lang == "" || $lang == "french"){
                                        echo $content2->general_content_fr;
                                    }else{
                                        echo $content2->general_content_en;
                                    }
                                ?>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
        <section class="instagram">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="shout insta-tag"><?php echo !empty($ig_tag)?'#'.$ig_tag:'#RootsCabana'; ?></p>
                        <div class="row">
                            <?php
                                foreach($images as $key=>$item){
                                    $image_thumb = $item['images']['standard_resolution']['url'];
                                    $image_link = $item['link'];
                                    $image_caption = $item['caption']['text'];
                                    $image_tags = $item['tags'];
                            ?>
                                <figure class="col-xs-12 col-sm-4 instagram-image"
                                       data-tag="<?php foreach($image_tags as $x){echo "#".$x;} ?>">
                                    <a title="<?= $image_link ?>" href="<?php echo $image_link; ?>" target="_blank">
                                        <div class="image" style="background-image: url(<?php echo $image_thumb; ?>);">
                                            <!-- <img alt="image" src="" class="img-responsive"> -->
                                        </div>
                                    </a>
                                </figure>
                            <?php
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
            <div class="container">
                <div class="row">
                    <div class=" col-xs-12 col-sm-10 col-sm-offset-1">
                        <article class="flat-card card-block">
                            <h3 class="card-title text-center bold">
                                <?php
                                    if($lang == "" || $lang == "french"){
                                        echo $content3->general_title_fr;
                                    }else{
                                        echo $content3->general_title_en;
                                    }
                                ?>
                            </h3>
                            <div class="text-center">
                                <?php
                                    if($lang == "" || $lang == "french"){
                                        echo $content3->general_content_fr;
                                    }else{
                                        echo $content3->general_content_en;
                                    }
                                ?>
                            </div>
                        </article>
                    </div>
                </div>
            </div>

        <section class="container">
            <div class="col-xs-12 col-sm-8 imagelink no-hover">
                <div role="button" title="<?= ucfirst($this->lang->line('about_us')) ?>" class="imagelink-bottom" style="background-image: url(<?= base_url('assets/upload/about/'.$imagelink_about->media_url); ?>)">
                    <div class="imagelink-text text-center">
                        <h2><?= ucfirst($this->lang->line('about_us')) ?></h2>
                        <br>
                        <a class="btn btn-raised imglnk-bottom" href="<?=base_url('about')?>"><?= $this->lang->line('btn_see') ?></a>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 imagelink no-hover">
                <a href="<?= base_url('crafts'); ?>">
                    <div role="button" title="<?= ucfirst($this->lang->line('crafts')) ?>" class="imagelink-bottom imglsquare" style="background-image: url(<?= base_url('assets/upload/crafts/'.$imagelink_crafts->media_url); ?>)">
                        <div class="imagelink-text text-center">
                            <h2><?= ucfirst($this->lang->line('crafts')) ?></h2>
                        </div>
                    </div>
                </a>
            </div>
        </section>
        <?php if(!empty($press)){ ?>
        <section class="press-release row">
          <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            <ul class="bxslider">
                <?php foreach($press as $key => $val){ ?>
                    <li>
                      <a href="<?= $val->media_content_en ?>" target="_blank" title="<?= $val->media_content_en ?>">
                        <img src="<?= base_url('assets/upload/home/press/'.$val->media_url) ?>" />
                      </a>
                    </li>
                <?php } ?>
            </ul>
          </div>
        </section>
        <?php } ?>
    </main>
