 <div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php
                if($submenu == 'about-handmade'){
                    echo $this->lang->line("handmade");
                }elseif($submenu == 'about-brand'){
                    echo $this->lang->line("brand");
                }else{
                    echo $this->lang->line("shop");
                }

                $submenux = $submenu;
            ?>
        </h1>
    </section>

    <!-- Main content -->

    <!-- BANNER -->
    <section id="banner" class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("banner"); ?>
                    </div>
                    <div class="panel-body">
                    <?php echo $this->session->userdata('notif_banner'); ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="banner_img" method="post" action="<?php echo base_url(); ?>backend/about/save_banner" enctype="multipart/form-data">
                                <figure class="carousel-images">
                                    <div class="col-xs-12 col-sm-10 col-sm-offset-1 image-with-preview">
                                        <div class="form-group col-xs-12 col-sm-6 col-sm-offset-3">
                                            <label class="control-label col-xs-12"><?php echo $this->lang->line("image"); ?></label>
                                            <input type="hidden" name="banner_id" value="<?php echo $banner->media_id; ?>">
                                            <input type="hidden" name="banner_submenu" value="<?= $submenu ?>">
                                            <input class="form-control btn btn-default" type="file" name="banner">
                                        </div>
                                        <img class="prev-img img-bordered img-responsive" alt="banner" src="<?php echo !empty($banner->media_url)?base_url('assets/upload/about/'.$banner->media_url):base_url('assets/img/blank.png'); ?>">
                                    </div>
                                </figure>
                                
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-sm-offset-4 upload-rules">
                                <ul>
                                    <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                    <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                    <li><?php echo $this->lang->line("min_resolution"); ?> <strong>1024x275</strong> px</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-center">
                        <button class="btn btn-primary" ><?php echo $this->lang->line("btn_update"); ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Our values -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <?php echo $this->lang->line("content"); ?>
                            </div>
                            <div class="col-xs-12 col-sm-6 text-right">
                                <button id="addContent" class="btn btn-success btn-xs" data-id="0" data-toggle="modal" data-target="#modalContent"><strong>+ <?= $this->lang->line("btn_add"); ?></strong></button>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?= $this->session->userdata('notif_content'); ?>
                        <div id="content" class="col-xs-12">
                            <table class="table table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th><?= $this->lang->line('image'); ?></th>
                                        <th><?= $this->lang->line('title'); ?> [FR]</th>
                                        <th><?= $this->lang->line('title'); ?> [EN]</th>
                                        <th><?= $this->lang->line('content'); ?> [FR]</th>
                                        <th><?= $this->lang->line('content'); ?> [EN]</th>
                                        <th class="text-center"><?= $this->lang->line('option'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        foreach($contents as $key => $val):
                                    ?>
                                            <tr>
                                                <td width="15%" class="text-center"><img class="img-responsive" src="<?= base_url('assets/upload/about/'.$val->media_url); ?>"></td>
                                                <td width="12.5%"><?= $val->media_title_fr; ?></td>
                                                <td width="12.5%"><?= $val->media_title_en; ?></td>
                                                <td><?= $val->media_content_fr; ?></td>
                                                <td><?= $val->media_content_en; ?></td>
                                                <td width="10%" class="text-center">
                                                    <a href="#" data-id="<?= $val->media_id; ?>" data-toggle="modal" data-target="#modalContent" class="btn btn-warning btn-md" title="edit"><i class="fa fa-pencil"></i></a>

                                                    <a href="<?php echo base_url('backend/about/delete_content/'.$val->media_id.'/'.$submenu);?>" class="btn btn-danger btn-md" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="modalContent" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form id="form_content" method="post" action="<?php echo base_url(); ?>backend/about/save_content" enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-xs-12">

                            <input type="hidden" name="content_id" value="">
                            <input type="hidden" name="content_submenu" value="<?= $submenu ?>">
                            <div class="col-xs-12 image-with-preview">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $this->lang->line("image"); ?></label>
                                    <input class="form-control btn btn-default" type="file" name="content_img">
                                </div>
                                <img class="prev-img img-bordered img-responsive" alt="content image" src="<?= base_url('assets/img/blank.png') ?>">
                                <br>
                                <div class="row">
                                    <div class="col-xs-12 upload-rules">
                                        <ul>
                                            <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                            <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                            <li><?php echo $this->lang->line("min_resolution"); ?> <strong>450x300</strong> px</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label"><?php echo $this->lang->line("title"); ?> [French]</label>
                                    <input type="text" class="form-control" name="title_fr">
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?php echo $this->lang->line("title"); ?> [English]</label>
                                    <input type="text" class="form-control" name="title_en">
                                </div>
                            </div>
                            <div class="col-xs-12">

                                <div class="form-group">
                                    <label class="control-label"><?php echo $this->lang->line("content"); ?> [French]</label>
                                    <textarea class="form-control" name="content_fr" id="content_fr"></textarea>
                                </div>
                                <div class="form-group">
                                    <label class="control-label"><?php echo $this->lang->line("content"); ?> [English]</label>
                                    <textarea class="form-control" name="content_en" id="content_en"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('btn_close'); ?></button>
                    <button type="submit" class="btn btn-primary"><?= $this->lang->line('btn_save'); ?></button>
                </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- /.content -->
</div>

 <script>
     $('#addContent').click(function () {
         $('#modalContent').find('input').val('');
         $('#modalContent').find('input[name="content_submenu"]').val('<?= $submenu ?>');
         $('#modalContent').find('img').attr('src','<?= base_url("assets/img/blank.png"); ?>');
         tinyMCE.get('content_fr').setContent('');
         tinyMCE.get('content_en').setContent('');
     });


     $('a[title="edit"]').click(function () {
         var id = $(this).data('id');

         jQuery.ajax({
             type: "POST",
             url: "<?= base_url('backend/about/getdata_content'); ?>",
             dataType: 'json',

             data: {id: id},
             success: function(x) {
                 $('#modalContent').find('input[name="content_id"]').val(x.media_id);
                 $('#modalContent').find('img.prev-img').attr('src','<?= base_url('assets/upload/about') ?>/'+x.media_url);
                 $('#modalContent').find('input[name="title_fr"]').val(x.media_title_fr);
                 $('#modalContent').find('input[name="title_en"]').val(x.media_title_en);
                 tinyMCE.get('content_fr').setContent(x.media_content_fr);
                 tinyMCE.get('content_en').setContent(x.media_content_en);
                 $('#content_fr').val(x.media_content_fr);
                 $('#content_en').val(x.media_content_en);
             },
         });

     });
 </script>
