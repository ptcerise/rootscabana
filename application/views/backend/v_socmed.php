 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="col-xs-12">
            <h1>
                <?php echo $this->lang->line("socmed"); ?>
            </h1>
        </div>
    </section>

    <!-- Main content -->

    <section id="socmed" class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("socmed"); ?>
                    </div>
                    <div class="panel-body">
                    <?php echo $this->session->userdata("notif"); ?>
                    <form method="post" action="<?php echo base_url(); ?>backend/socmed/save">
                        <div class="form-group">
                            <label class="control-label">Facebook</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="http://facebook.com/username" name="link_facebook" value="<?php echo $facebook->media_url; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Twitter</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="http://twitter.com/username" name="link_twitter" value="<?php echo $twitter->media_url; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Instagram</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-instagram" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="http://instagram.com/username" name="link_instagram" value="<?php echo $instagram->media_url; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Pinterest</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-pinterest" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="http://pinterest.com/username" name="link_pinterest" value="<?php echo $pinterest->media_url; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">email</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                </span>
                                <input type="email" class="form-control" placeholder="example@example.com" name="link_email" value="<?php echo $email->media_url; ?>">
                            </div>
                        </div>
                    
                </div>
                <div class="panel-footer text-center">
                    <button class="btn btn-primary" type="submit" ><?php echo $this->lang->line("btn_update"); ?></button>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </section>

    <section id="instagram" class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Instagram
                    </div>
                    <div class="panel-body">
                    <?php echo $this->session->userdata("notif_ig"); ?>
                    <form method="post" action="<?php echo base_url(); ?>backend/socmed/save_igsetting">
                        <div class="form-group">
                            <label class="control-label">Hashtag</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-hashtag" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="example" name="tag" value="<?php echo !empty($igsetting->media_title_fr)?$igsetting->media_title_fr:'';?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Access Token</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-key" aria-hidden="true"></i>
                                </span>
                                <input type="text" class="form-control" placeholder="123456789.123456.1234567890" name="access_token" value="<?php echo !empty($igsetting->media_url)?$igsetting->media_url:'';?>">
                            </div>
                        </div>
                </div>
                <div class="panel-footer text-center">
                    <button class="btn btn-primary" type="submit" ><?php echo $this->lang->line("btn_update"); ?></button>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
