 <div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line("home"); ?>
        </h1>
    </section>

    <!-- BANNER -->
    <section id="banner" class="content">
        <div class="row">
            <div class="col-xs-12">
            <form id="banner_video" method="post" action="<?php echo base_url(); ?>backend/home/save_video_banner" enctype="multipart/form-data">
                <div class="panel panel-info">

                    <div class="panel-heading">
                        <?php echo $this->lang->line("banner_video"); ?>
                    </div>
                    <div class="panel-body">
                    <?php echo $this->session->userdata('notif_banner'); ?>
                        <div class="row">
                            <div class="col-xs-12">
                                    <video class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3" controls>
                                        <?php
                                            $y = explode('.',$video->media_url);
                                            $ext = $y[1];
                                        ?>
                                        <source src="<?= base_url('assets/upload/video/'.$video->media_url); ?>" type="video/<?= strtolower($ext); ?>">
                                    </video>


                                    <div class="form-group col-xs-12 col-sm-6 col-sm-offset-3">
                                        <hr>
                                        <label class="control-label col-xs-12"><?= $this->lang->line('select_file'); ?></label>
                                        <input type="hidden" name="banner_id" value="<?= isset($video->media_id)?$video->media_id:''; ?>">
                                        <input class="form-control btn btn-default" type="file" name="banner">
                                    </div>

                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3 upload-rules">
                                <ul>
                                    <li><?php echo $this->lang->line("file_format"); ?> <strong>*.mp4 / *.flv / *.avi</strong></li>
                                    <li><?php echo $this->lang->line("max_file_size"); ?> <strong>8 MB</strong></li>
                                    <li><?php echo $this->lang->line("min_video_resolution"); ?> <strong>1280x720</strong> px</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-center">
                        <button class="btn btn-primary" ><?php echo $this->lang->line("btn_update"); ?></button>
                    </div>

                </div>
                </form>
            </div>
        </div>
    </section>

    <!-- CONTENT -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("paragraph"); ?>
                    </div>
                    <div class="panel-body">

                        <!-- CONTENT 1 -->
                        <div id="content1" class="col-xs-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <?php echo $this->lang->line("paragraph"); ?> 1
                                </div>
                                <div class="panel-body">
                                <?php echo $this->session->userdata('notif_content1'); ?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <form id="form_content" method="post" action="<?php echo base_url(); ?>backend/home/save_content1" enctype="multipart/form-data">
                                            <input type="hidden" name="content1_id" value="<?= $content1->general_id; ?>">
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label"><?php echo $this->lang->line("title"); ?> [French]</label>
                                                        <input type="text" class="form-control" name="content1_title_fr" value="<?= $content1->general_title_fr; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label"><?php echo $this->lang->line("content"); ?> [French]</label>
                                                        <textarea class="form-control" name="content1_content_fr"><?= $content1->general_content_fr; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label"><?php echo $this->lang->line("title"); ?> [English]</label>
                                                        <input type="text" class="form-control" name="content1_title_en" value="<?= $content1->general_title_en; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label"><?php echo $this->lang->line("content"); ?> [English]</label>
                                                        <textarea class="form-control" name="content1_content_en"><?= $content1->general_content_en; ?></textarea>
                                                    </div>
                                                </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer text-center">
                                    <button class="btn btn-primary"><?php echo $this->lang->line("btn_update"); ?></button>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- CONTENT 2 -->
                        <div id="content2" class="col-xs-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <?php echo $this->lang->line("paragraph"); ?> 2
                                </div>
                                <div class="panel-body">
                                <?php echo $this->session->userdata('notif_content2'); ?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <form id="form_content2" method="post" action="<?php echo base_url(); ?>backend/home/save_content2" enctype="multipart/form-data">
                                            <input type="hidden" name="content2_id" value="<?= $content2->general_id; ?>">
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label"><?php echo $this->lang->line("title"); ?> [French]</label>
                                                        <input type="text" class="form-control" name="content2_title_fr" value="<?= $content2->general_title_fr; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label"><?php echo $this->lang->line("content"); ?> [French]</label>
                                                        <textarea class="form-control" name="content2_content_fr"><?= $content2->general_content_fr; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label"><?php echo $this->lang->line("title"); ?> [English]</label>
                                                        <input type="text" class="form-control" name="content2_title_en" value="<?= $content2->general_title_en; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label"><?php echo $this->lang->line("content"); ?> [English]</label>
                                                        <textarea class="form-control" name="content2_content_en"><?= $content2->general_content_en; ?></textarea>
                                                    </div>
                                                </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer text-center">
                                    <button class="btn btn-primary"><?php echo $this->lang->line("btn_update"); ?></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- CONTENT 3 -->
                        <div id="content3" class="col-xs-12">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <?php echo $this->lang->line("paragraph"); ?> 3
                                </div>
                                <div class="panel-body">
                                <?php echo $this->session->userdata('notif_content3'); ?>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <form id="form_content3" method="post" action="<?php echo base_url(); ?>backend/home/save_content3" enctype="multipart/form-data">
                                            <input type="hidden" name="content3_id" value="<?= $content3->general_id; ?>">
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label"><?php echo $this->lang->line("title"); ?> [French]</label>
                                                        <input type="text" class="form-control" name="content3_title_fr" value="<?= $content3->general_title_fr; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label"><?php echo $this->lang->line("content"); ?> [French]</label>
                                                        <textarea class="form-control" name="content3_content_fr"><?= $content3->general_content_fr; ?></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6">
                                                    <div class="form-group">
                                                        <label class="control-label"><?php echo $this->lang->line("title"); ?> [English]</label>
                                                        <input type="text" class="form-control" name="content3_title_en" value="<?= $content3->general_title_en; ?>">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label"><?php echo $this->lang->line("content"); ?> [English]</label>
                                                        <textarea class="form-control" name="content3_content_en"><?= $content3->general_content_en; ?></textarea>
                                                    </div>
                                                </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="panel-footer text-center">
                                    <button class="btn btn-primary"><?php echo $this->lang->line("btn_update"); ?></button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- IMAGE LINK -->
    <section id="imagelink" class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("image_link"); ?>
                    </div>
                    <form id="form_imagelink" action="<?= base_url('backend/home/save_imagelink'); ?>" method="post" enctype="multipart/form-data">
                    <div class="panel-body">
                        <?php echo $this->session->userdata('notif_imagelink'); ?>
                        <div class="col-xs-12 col-sm-3">
                            <figure>
                                <div class="image-with-preview">
                                    <div class="form-group">
                                        <label class="control-label col-xs-12">Collection Homme</label>
                                        <input type="hidden" name="imagelink1_id" value="<?= isset($imagelink1->media_id)?$imagelink1->media_id:''; ?>">
                                        <input class="form-control btn btn-default" type="file" name="imagelink1">
                                    </div>
                                    <img class="prev-img img-bordered img-responsive" alt="banner" src="<?php echo !empty($imagelink1->media_url)?base_url('assets/upload/home/'.$imagelink1->media_url):base_url('assets/img/blank.png'); ?>">
                                </div>
                            </figure>
                            <br>
                            <div class="row">
                                <div class="col-xs-12 upload-rules">
                                    <ul>
                                        <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                        <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                        <li><?php echo $this->lang->line("min_resolution"); ?> <strong>1200x1200</strong> px</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3">
                            <figure>
                                <div class="image-with-preview">
                                    <div class="form-group">
                                        <label class="control-label col-xs-12">Collection Femme</label>
                                        <input type="hidden" name="imagelink2_id" value="<?= isset($imagelink2->media_id)?$imagelink2->media_id:''; ?>">
                                        <input class="form-control btn btn-default" type="file" name="imagelink2">
                                    </div>
                                    <img class="prev-img img-bordered img-responsive" alt="banner" src="<?php echo !empty($imagelink2->media_url)?base_url('assets/upload/home/'.$imagelink2->media_url):base_url('assets/img/blank.png'); ?>">
                                </div>
                            </figure>
                            <br>
                            <div class="row">
                                <div class="col-xs-12 upload-rules">
                                    <ul>
                                        <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                        <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                        <li><?php echo $this->lang->line("min_resolution"); ?> <strong>1200x1200</strong> px</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <figure>
                                <div class="image-with-preview">
                                    <div class="form-group">
                                        <label class="control-label col-xs-12">Beach Shop</label>
                                        <input type="hidden" name="imagelink3_id" value="<?= isset($imagelink3->media_id)?$imagelink3->media_id:''; ?>">
                                        <input class="form-control btn btn-default" type="file" name="imagelink3">
                                    </div>
                                    <img class="prev-img img-bordered img-responsive" alt="banner" src="<?php echo !empty($imagelink3->media_url)?base_url('assets/upload/home/'.$imagelink3->media_url):base_url('assets/img/blank.png'); ?>">
                                </div>
                            </figure>
                            <br>
                            <div class="row">
                                <div class="col-xs-12 upload-rules">
                                    <ul>
                                        <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                        <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                        <li><?php echo $this->lang->line("min_resolution"); ?> <strong>1200x600</strong> px</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="panel-footer text-center">
                        <button class="btn btn-primary" ><?php echo $this->lang->line("btn_update"); ?></button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <section id="press" class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="panel panel-info">
            <div class="panel-heading">
              Press
              <div class="caption pull-right">
                 <a href="#" class="page btn btn-success btn-xs" title="Add Press" data-toggle="modal" data-target="#modal_press"><i class="fa fa-plus"></i> press</a>
             </div>
            </div>
            <div class="panel-body">
              <div class="table-responsive">
                <table id="tablePress" class="table table-hover">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Press logo</th>
                      <th>Press link</th>
                      <th>Options</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($press as $key => $val): ?>
                      <tr>
                        <td>
                          <?= $key+1 ?>
                        </td>
                        <td>
                          <img class="img img-responsive" src="<?= base_url('assets/upload/home/press/'.$val->media_url) ?>">
                        </td>
                        <td>
                          <a target="_blank" href="<?= $val->media_content_en ?>"><?= $val->media_content_en ?></a>
                        </td>
                        <td>
                          <a class="btn btn-warning" href="#" title="edit" data-toggle="modal" data-target="#update_press<?php echo $val->media_id; ?>">
                            <i class="fa fa-pencil"></i>
                          </a>

                          <div id="update_press<?= $val->media_id ?>" class="modal fade" role="dialog">
                            <form method="post" action="<?php echo base_url(); ?>backend/home/save_press" enctype="multipart/form-data">
                            <input type="hidden" name="press_id" value="<?= $val->media_id ?>">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Press</h4>
                                </div>
                                <div class="modal-body">

                                      <div class="row">
                                          <div class="col-xs-12 col-sm-10 col-sm-offset-1 image-with-preview">
                                              <div class="form-group">
                                                  <label class="control-label"><?php echo $this->lang->line("image"); ?></label>
                                                  <input class="form-control btn btn-default" type="file" name="press_image">
                                              </div>
                                              <img class="prev-img img-bordered img-responsive" alt="press image" src="<?php echo isset($val->media_url)?base_url('assets/upload/home/press/'.$val->media_url):base_url('assets/img/blank.png'); ?>">
                                          </div>
                                      </div>
                                      <br>
                                          <div class="row">
                                              <div class="col-xs-12 col-sm-10 col-sm-offset-1 upload-rules">
                                                  <ul>
                                                      <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                                      <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                                      <li><?php echo $this->lang->line("min_resolution"); ?> <strong>60x40</strong> px</li>
                                                  </ul>
                                              </div>
                                          </div>
                                      <hr>
                                      <div class="row">
                                          <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                              <div class="form-group">
                                                  <label class="control-label">Press url</label>
                                                  <input class="form-control" type="url" name="press_url" value="<?= $val->media_content_en ?>">
                                              </div>
                                          </div>
                                      </div>
                                </div>
                                <div class="modal-footer">
                                  <a class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line("btn_close"); ?></a>
                                  <button type="submit" class=" btn btn-primary"><?php echo $this->lang->line("btn_save"); ?></button>
                                </div>
                                </form>
                              </div>
                            </div>
                          </div>

                          <a class="btn btn-danger" href="<?= base_url('backend/home/delete_press/'.$val->media_id) ?>" title="delete" onclick="return confirm('Are you sure?')">
                            <i class="fa fa-trash-o"></i>
                          </a>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div id="modal_press" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title">Press</h4>
            </div>
            <div class="modal-body">
              <form id="form_press" method="post" action="<?php echo base_url(); ?>backend/home/save_press" enctype="multipart/form-data">
              <input type="hidden" name="press_id" value="">
                  <div class="row">
                      <div class="col-xs-12 col-sm-10 col-sm-offset-1 image-with-preview">
                          <div class="form-group">
                              <label class="control-label"><?php echo $this->lang->line("image"); ?></label>
                              <input class="form-control btn btn-default" type="file" name="press_image">
                          </div>
                          <img class="prev-img img-bordered img-responsive" alt="press image" src="<?php echo base_url('assets/img/blank.png'); ?>">
                      </div>
                  </div>
                  <br>
                      <div class="row">
                          <div class="col-xs-12 col-sm-10 col-sm-offset-1 upload-rules">
                              <ul>
                                  <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                  <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                  <li><?php echo $this->lang->line("min_resolution"); ?> <strong>60x40</strong> px</li>
                              </ul>
                          </div>
                      </div>
                  <hr>
                  <div class="row">
                      <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                          <div class="form-group">
                              <label class="control-label">Press url</label>
                              <input class="form-control" type="url" name="press_url" value="">
                          </div>
                      </div>
                  </div>
              </form>
            </div>
            <div class="modal-footer">

              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line("btn_close"); ?></button>
              <button type="button" class=" btn btn-primary" id="btn_press"><?php echo $this->lang->line("btn_save"); ?></button>
            </div>
          </div>

        </div>
      </div>
    </section>

</div>
