 <div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line("men"); ?>
        </h1>
    </section>

    <!-- Main content -->

    <!-- BANNER -->
    <section id="banner" class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("banner"); ?>
                    </div>
                    <div class="panel-body">
                    <?php echo $this->session->userdata('notif_banner'); ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="banner_img" method="post" action="<?php echo base_url(); ?>backend/men/save_banner" enctype="multipart/form-data">
                                <figure class="carousel-images">
                                    <div class="col-xs-12 col-sm-10 col-sm-offset-1 image-with-preview">
                                        <div class="form-group col-xs-12 col-sm-6 col-sm-offset-3">
                                            <label class="control-label col-xs-12"><?php echo $this->lang->line("image"); ?></label>
                                            <input type="hidden" name="banner_id" value="<?php echo $banner->media_id; ?>">
                                            <input class="form-control btn btn-default" type="file" name="banner">
                                        </div>
                                        <img class="prev-img img-bordered img-responsive" alt="banner" src="<?php echo !empty($banner->media_url)?base_url('assets/upload/men/'.$banner->media_url):base_url('assets/img/blank.png'); ?>">
                                    </div>
                                </figure>
                                
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-sm-offset-4 upload-rules">
                                <ul>
                                    <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                    <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                    <li><?php echo $this->lang->line("min_resolution"); ?> <strong>1024x275</strong> px</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-center">
                        <button class="btn btn-primary" ><?php echo $this->lang->line("btn_update"); ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

 <!-- CATEGORIES -->
    <section id="category" class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("category"); ?>
                    </div>
                    <div class="panel-body">
                    <?php echo $this->session->userdata('notif_category'); ?>
                    <?php foreach($category as $key => $val): ?>
                        <div class="col-xs-12 col-sm-6">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <?php
                                        $cname = explode("-",$val->category_name);
                                        for($i=0;$i < (count($cname)-1);$i++){
                                            echo ucfirst($cname[$i+1])." ";
                                        }
                                    ?>
                                </div>
                                <div class="panel-body">
                                    <form method="post" action="<?php echo base_url(); ?>backend/men/save_category" enctype="multipart/form-data">
                                    <input type="hidden" name="category_id" value="<?php echo $val->category_id; ?>">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-10 col-sm-offset-1 image-with-preview">
                                                <div class="form-group">
                                                    <label class="control-label"><?php echo $this->lang->line("img_sq"); ?></label>
                                                    <input class="form-control btn btn-default" type="file" name="category_image_sq">
                                                </div>
                                                <img class="prev-img img-bordered img-responsive" alt="adv image" src="<?php echo empty($val->category_image_sq)?base_url('assets/img/blank.png'):base_url('assets/upload/category/'.$val->category_image_sq); ?>">
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-10 col-sm-offset-1 upload-rules">
                                                    <ul>
                                                        <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                                        <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                                        <li><?php echo $this->lang->line("min_resolution"); ?> <strong>500x500</strong> px </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <?php
                                                $x = $this->db->query("SELECT * FROM product WHERE product_category LIKE '$val->category_name' ORDER BY product_category ASC")->result();
                                            ?>
                                            <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                                                <label class="control-label"><?= $this->lang->line('choose_display') ?> (max 2)</label>
                                                <select name="product_display[]" class="image-select form-control" multiple="multiple">
                                                    <?php foreach ($x as $i => $product) { ?>
                                                    <option
                                                        value="<?= $product->product_id; ?>"
                                                        data-img-src="<?= base_url('assets/upload/product').'/'.$product->product_image; ?>"
                                                        <?php if($product->product_display == 'true'){echo "selected=''";} ?>
                                                    >
                                                        <?= $product->product_name; ?>
                                                            
                                                    </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="text-center">
                                            <h3>
                                                <?php
                                                    $cname = explode("-",$val->category_name);
                                                    for($i=0;$i < (count($cname)-1);$i++){
                                                        echo ucfirst($cname[$i+1])." ";
                                                    }
                                                ?>
                                            </h3>
                                        </div>
                                </div>
                                <div class="panel-footer text-center">
                                    <input type="submit" class="btn btn-primary" value="<?php echo $this->lang->line("btn_update"); ?>">
                                    </form>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- PRODUCTS -->
    <section id="product" class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("products"); ?>
                         <div class="caption pull-right">
                            <a href="#" class="page btn btn-success btn-xs add-craft" title="Add Product" data-toggle="modal" data-target="#modal_product"><i class="fa fa-plus"></i> <?php echo $this->lang->line("item"); ?></a>
                        </div>
                    </div>
                    <div class="panel-body">
                    <?php echo $this->session->userdata('notif_product'); ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table id="table_product" class="table table-bordered table-hover">
                                        <thead>
                                            <th class="text-center">#</th>
                                            <th><?php echo $this->lang->line("image"); ?></th>
                                            <th><?php echo $this->lang->line("name"); ?></th>
                                            <th><?php echo $this->lang->line("category"); ?></th>
                                            <th class="text-center"><?php echo $this->lang->line("option"); ?></th>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($products as $key => $value):?>
                                            <tr>
                                                <td class="text-center"><?php echo $key+1; ?></td>
                                                <td>
                                                    <img class="img-responsive thumb-product" src="<?php echo base_url('assets/upload/product/'.$value->product_image); ?>">
                                                </td>
                                                <td><?php echo $value->product_name; ?></td>
                                                <td>
                                                    <?php
                                                        $cname = explode('-',$value->product_category);
                                                        for($i=0;$i<count($cname)-1;$i++){
                                                            echo ucfirst($cname[$i+1])." ";
                                                        }
                                                    ?>
                                                </td>
                                                <td class="text-center">
                                                    <a href="<?php echo base_url('backend/product/detail/'.$value->product_id);?>" class="btn btn-warning btn-md" title="edit"><i class="fa fa-list-alt"></i> <?php echo $this->lang->line("btn_detail"); ?></a>

                                                    <a href="<?php echo base_url('backend/product/delete/'.$value->product_id);?>" class="btn btn-danger btn-md" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="modal_product" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $this->lang->line("product"); ?></h4>
              </div>
              <div class="modal-body">
                <form id="form_product" method="post" action="<?php echo base_url(); ?>backend/product/save_product" enctype="multipart/form-data">
                <input type="hidden" name="product_id" value="">
                    <div class="row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1 image-with-preview">
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line("image"); ?></label>
                                <input class="form-control btn btn-default" type="file" name="product_image" required="">
                            </div>
                            <img class="prev-img img-bordered img-responsive" alt="product image" src="<?php echo base_url('assets/img/blank.png'); ?>">
                        </div>
                    </div>
                    <br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-10 col-sm-offset-1 upload-rules">
                                <ul>
                                    <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                    <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                    <li><?php echo $this->lang->line("min_resolution"); ?> <strong>500x750</strong> px</li>
                                </ul>
                            </div>
                        </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line("name"); ?></label>
                                <input class="form-control" type="text" name="product_name" value="">
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line("category"); ?></label>
                                <select class="form-control" name="product_category">
                                    <?php
                                        foreach ($category as $key => $value):
                                    ?>
                                    <option value="<?php echo $value->category_name; ?>">
                                        <?php
                                            $cname = explode('-',$value->category_name);
                                            for($i=0;$i<count($cname)-1;$i++){
                                                echo ucfirst($cname[$i+1])." ";
                                            }
                                        ?>
                                    </option>
                                    <?php endforeach;?>
                                </select>
                                </textarea>
                            </div>
                        </div>
                    </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line("btn_close"); ?></button>
                <button type="button" class=" btn btn-primary" id="btn_product"><?php echo $this->lang->line("btn_save"); ?></button>
              </div>
            </div>

          </div>
        </div>

    </section>

    <!-- /.content -->
</div>
