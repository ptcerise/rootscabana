<!DOCTYPE html>
<html>
<head>
	<title>Login | Rootscabana</title>
	<meta name="viewport" content="width=1360px, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/login.css">
</head>
<body style="background-image: url('<?php echo base_url(); ?>assets/img/corsica.png');background-repeat: no-repeat;background-attachment: fixed;background-position: center">
	<section>
		<br>
		<div class="logo">
			<img class="img-responsive" src="<?php echo base_url(); ?>assets/img/logo_brown.png" alt="Roots Cabana logo">
		</div>
		<p class="welcome">Backend of Rootscabana</p>
		<div class="container">
			<form class="form-signin" role="form" method="POST" action="<?php echo base_url('backend/login/process');?>">
			<?php echo $this->session->flashdata('login');?>
				<input type="text" name="user_name" class="form-control" placeholder="Username" autocomplete="off" required autofocus>
				<input type="password" name="user_pass" class="form-control" placeholder="Password" required autocomplete="off">
				<center><button class="btn btn-lg btn-default" type="submit">Sign in</button></center>
			</form>
		</div>
	</section>
	<footer>
		<div class="line"></div>
	</footer>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>