 <div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="row">
            <div class="col-xs-12">
            <?php echo $this->session->userdata('notif_detail'); ?>
        		<h1>
                    <?php echo $this->lang->line("product_detail"); ?>
                </h1>
            </div>
        </div>
    </section>
    
    <!-- Main content -->

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-body">
                        <form id="form_detail_product" method="post" action="<?php echo base_url(); ?>backend/product/update_product" enctype="multipart/form-data">
                        <input type="hidden" name="product_id" value="<?php echo $product->product_id; ?>">
                        <!-- GENERAL INFORMATION -->
                        <div class="well detail-title">
                            <h2><?php echo $this->lang->line("overview"); ?></h2>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-4">
                                <div class="row">
                                    <div class="col-xs-12 image-with-preview">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $this->lang->line("image"); ?></label>
                                            <input class="form-control btn btn-default" type="file" name="product_image">
                                        </div>
                                        <img class="prev-img img-bordered img-responsive detail" alt="product image" src="<?php echo base_url('assets/upload/product/'.$product->product_image); ?>">
                                    </div>
                                    <div class="col-xs-12 upload-rules">
                                        <ul>
                                            <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                            <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                            <li><?php echo $this->lang->line("min_resolution"); ?> <strong>500x750</strong> px</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-8">
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $this->lang->line("name"); ?></label>
                                        <input type="text" class="form-control" name="product_name" value="<?php echo $product->product_name; ?>">
                                    </div>
                                    <hr>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label"><?php echo $this->lang->line("category"); ?></label>
                                        <select class="form-control" name="product_category">
                                            <?php
                                                $cname = explode('-',$product->product_category);
                                                if($cname[0] == 'men'){
                                                   $category = $this->db->query("SELECT * FROM category WHERE category_name NOT LIKE '%women%'")->result(); 
                                               }else{
                                                    $category = $this->db->query("SELECT * FROM category WHERE category_name LIKE '%women%'")->result();
                                               }
                                            
                                                foreach ($category as $key => $value) :
                                            ?>
                                            <option
                                                <?php
                                                    if($product->product_category == $value->category_name){
                                                        echo 'selected=""';
                                                    }
                                                ?>
                                             value="<?php echo $value->category_name; ?>">
                                                <?php
                                                    $cname = explode('-',$value->category_name);
                                                    for($i=0;$i<count($cname)-1;$i++){
                                                        echo ucfirst($cname[$i+1])." ";
                                                    }
                                                ?>
                                            </option>
                                            <?php endforeach;?>
                                        </select>
                                    </div>
                                    <hr>
                                </div>

                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $this->lang->line("description"); ?> [French]</label>
                                            <textarea class="form-control textarea" name="product_content_fr" style="height:100%;">
                                                <?php echo $product->product_content_fr; ?>
                                            </textarea>
                                        </div>
                                        
                                    </div>


                                <hr>

                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $this->lang->line("description"); ?> [English]</label>
                                            <textarea class="form-control textarea" name="product_content_en" style="height:100%;">
                                                <?php echo $product->product_content_en; ?>
                                            </textarea>
                                        </div>
                                        
                                    </div>


                            </div>
                        </div>

                        </form>
                    </div>
                    <div class="panel-footer text-right">
                        <a href="<?php echo base_url('backend/'.$cname[0])."#product"; ?>" class="btn btn-default" ><?php echo $this->lang->line("btn_cancel"); ?></a>
                        <button type="submit" class="btn btn-primary" id="upd_product"><?php echo $this->lang->line("btn_update"); ?></button>
                    </div>
                </div>
            </div>
            <div id="color" class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-body">
                        <div class="well detail-title">
                            <h3>
                                <?= $this->lang->line('color'); ?>
                                <span class="pull-right">
                                    <a href="#" class="page btn btn-success btn-md add-variant" title="Add-color" data-toggle="modal" data-target="#modal_color"><i class="fa fa-plus"></i> <?php echo $this->lang->line("color"); ?></a>
                                </span>
                            </h3>
                        </div>
                        <!-- COLOR VARIANT -->
                        <?php echo $this->session->userdata('notif_color'); ?>
                        <div class="row">
                            <div class="col-xs-12">

                                <table id="table_color" class="table table-bordered table-hover">
                                    <thead>
                                    <th class="text-center" width="15%">#</th>
                                    <th><?php echo $this->lang->line("color"); ?></th>
                                    <th class="text-center" width="25%"><?php echo $this->lang->line("option"); ?></th>
                                    </thead>
                                    <tbody>
                                    <?php foreach($product_color as $key=>$value): ?>
                                        <tr>
                                            <td class="text-center"><?= $key+1; ?></td>
                                            <td class="text-center">
                                                <?php echo $value->color_code; ?>
                                                <div class="well" style="background-color: <?php echo $value->color_code; ?>;width: '100%';"></div>
                                            </td>
                                            <td class="text-center">
                                                <a href="#" class="btn btn-warning btn-md edit-variant" title="edit-color" data-toggle="modal" data-target="#modal_color" data-id="<?= $value->color_id; ?>" data-color="<?= $value->color_code; ?>"><i class="fa fa-pencil"></i></a>

                                                <a href="<?php echo base_url('backend/product/delete_color/'.$value->color_id);?>" class="btn btn-danger btn-md" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="variant" class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-body">
                        <div class="well detail-title">
                            <h3><?php echo $this->lang->line("variant"); ?>
                            <span class="pull-right">
                                <?php $count = count($product_detail);
                                    if($count < 2){
                                ?>
                                    <a href="#" class="page btn btn-success btn-md add-variant" title="Add Variant" data-toggle="modal" data-target="#modal_variant"><i class="fa fa-plus"></i> <?php echo $this->lang->line("variant"); ?></a>
                                <?php } ?>
                            </span>
                            </h3>
                        </div>
                        <!-- PRODUCT VARIANT -->
                        <?php echo $this->session->userdata('notif_variant'); ?>
                        <div class="row">
                            <div class="col-xs-12">

                                    <table id="table_variant" class="table table-bordered table-hover">
                                        <thead>
                                            <th class="text-center">#</th>
                                            <th><?php echo $this->lang->line("image"); ?></th>
                                            <th class="text-center"><?php echo $this->lang->line("option"); ?></th>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($product_detail as $key => $value):?>
                                            <tr class="variant-item">
                                                <td class="text-center"><?php echo $key+1; ?></td>
                                                <td>
                                                    <img class="img-responsive img-bordered thumb-product" src="<?php echo base_url('assets/upload/product/detail/'.$value->detail_image); ?>">
                                                </td>
                                                <td class="text-center">
                                                    <a href="#" class="btn btn-warning btn-md edit-variant" data-id="<?php echo $value->detail_id; ?>" data-color="<?php echo $value->detail_color; ?>" title="edit" data-toggle="modal" data-target="#edit_variant<?php echo $value->detail_id; ?>"><i class="fa fa-list-alt"></i> Detail</a>

                                                    <a href="<?php echo base_url('backend/product/delete_variant/'.$value->detail_id);?>" class="btn btn-danger btn-md" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="modal_variant" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $this->lang->line("variant"); ?></h4>
              </div>
              <div class="modal-body">
                <form id="form_variant" method="post" action="<?php echo base_url(); ?>backend/product/save_variant" enctype="multipart/form-data">
                <input type="hidden" name="product_id" value="<?php echo $product->product_id; ?>">
                <input type="hidden" name="detail_id" value="">
                    <div class="row">

                        <div class="col-xs-12 col-sm-10 col-sm-offset-1 image-with-preview">
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line("image"); ?></label>
                                <input class="form-control btn btn-default" type="file" name="detail_image" required="">
                            </div>
                            <img class="prev-img img-bordered img-responsive" alt="craft image" src="<?php echo base_url('assets/img/blank.png'); ?>">
                        </div>
                    </div>
                    <br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-10 col-sm-offset-1 upload-rules">
                                <ul>
                                    <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                    <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                    <li><?php echo $this->lang->line("min_resolution"); ?> <strong>500x750</strong> px</li>
                                </ul>
                            </div>
                        </div>
                    <hr>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line("btn_close"); ?></button>
                <button type="button" class=" btn btn-primary" id="btn_variant"><?php echo $this->lang->line("btn_save"); ?></button>
              </div>
            </div>

          </div>
        </div>
        <?php foreach ($product_detail as $key => $value):?>
        <div id="edit_variant<?php echo $value->detail_id; ?>" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $this->lang->line("variant"); ?></h4>
              </div>
              <div class="modal-body">
                <form method="post" action="<?php echo base_url(); ?>backend/product/save_variant" enctype="multipart/form-data">
                <input type="hidden" name="product_id" value="<?php echo $product->product_id; ?>">
                <input type="hidden" name="detail_id" value="<?php echo $value->detail_id; ?>">
                    <div class="row">

                        <div class="col-xs-12 col-sm-10 col-sm-offset-1 image-with-preview">
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line("image"); ?></label>
                                <input class="form-control btn btn-default" type="file" name="detail_image">
                            </div>
                            <img class="prev-img img-bordered img-responsive" alt="craft image" src="<?php echo base_url('assets/upload/product/detail/'.$value->detail_image); ?>">
                        </div>
                    </div>
                    <br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-10 col-sm-offset-1 upload-rules">
                                <ul>
                                    <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                    <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                    <li><?php echo $this->lang->line("min_resolution"); ?> <strong>500x750</strong> px</li>
                                </ul>
                            </div>
                        </div>
                    <hr>
                    <div class="row">
                        
                    </div>
                
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line("btn_close"); ?></button>
                <button type="submit" class=" btn btn-primary" id="btn_variant"><?php echo $this->lang->line("btn_save"); ?></button>
                </form>
              </div>
            </div>

          </div>
        </div>
        <?php endforeach;?>
    </section>
    <!-- /.content -->
</div>

 <div id="modal_color" class="modal fade" role="dialog">
     <div class="modal-dialog">

         <!-- Modal content-->
         <div class="modal-content">
             <div class="modal-header">
                 <button type="button" class="close" data-dismiss="modal">&times;</button>
                 <h4 class="modal-title"><?php echo $this->lang->line("color"); ?></h4>
             </div>
             <form id="form_color" method="post" action="<?php echo base_url(); ?>backend/product/save_color" enctype="multipart/form-data">
             <div class="modal-body">

                     <input type="hidden" name="product_id" value="<?php echo $product->product_id; ?>">
                     <input type="hidden" name="color_id" value="">
                     <div class="row">
                         <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                             <div class="form-group">
                                 <label class="control-label"><?php echo $this->lang->line("color"); ?></label>
                                 <div id="cpicker" class="input-group colorpicker-component">
                                     <input type="text" value="" class="form-control" name="color_code">
                                     <span class="input-group-addon"><i class="fa fa-eyedropper" aria-hidden="true" style="color:rgba(0,0,0,.23)"></i></span>
                                 </div>
                             </div>
                         </div>
                     </div>

             </div>
             <div class="modal-footer">
                 <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line("btn_close"); ?></button>
                 <button type="submit" class=" btn btn-primary" id="btn_variant"><?php echo $this->lang->line("btn_save"); ?></button>
             </div>
             </form>
         </div>

     </div>
 </div>
 
 <script>
     $('a[title="Add-color"]').click(function () {
         $('#form_color').find('input[name="color_id"]').val('');
         $('#form_color').find('input[name="color_code"]').val('');
         $('#form_color').find('.fa-eyedropper').css('background-color','transparent');
     });

     $('a[title="edit-color"]').click(function () {
         var id = $(this).data('id');
         var color = $(this).data('color');

         $('#form_color').find('input[name="color_id"]').val(id);
         $('#form_color').find('input[name="color_code"]').val(color);
         $('#form_color').find('.fa-eyedropper').css('background-color',color);
     });
 </script>
