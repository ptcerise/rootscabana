 <div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line("contact"); ?>
        </h1>
    </section>

    <!-- Main content -->

    <!-- BANNER -->
    <section id="banner" class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("banner"); ?>
                    </div>
                    <div class="panel-body">
                    <?php echo $this->session->userdata('notif_banner'); ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="banner_img" method="post" action="<?php echo base_url(); ?>backend/contact/save_banner" enctype="multipart/form-data">
                                <figure class="carousel-images">
                                    <div class="col-xs-12 col-sm-10 col-sm-offset-1 image-with-preview">
                                        <div class="form-group col-xs-12 col-sm-6 col-sm-offset-3">
                                            <label class="control-label col-xs-12"><?php echo $this->lang->line("image"); ?></label>
                                            <input type="hidden" name="banner_id" value="<?php echo $banner->media_id; ?>">
                                            <input class="form-control btn btn-default" type="file" name="banner">
                                        </div>
                                        <img class="prev-img img-bordered img-responsive" alt="banner" src="<?php echo !empty($banner->media_url)?base_url('assets/upload/contact/'.$banner->media_url):base_url('assets/img/blank.png'); ?>">
                                    </div>
                                </figure>
                                
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-sm-offset-4 upload-rules">
                                <ul>
                                    <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                    <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                    <li><?php echo $this->lang->line("min_resolution"); ?> <strong>1024x275</strong> px</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-center">
                        <button class="btn btn-primary" ><?php echo $this->lang->line("btn_update"); ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- CONTENT -->
    <section id="content" class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("content"); ?>
                    </div>
                    <div class="panel-body">
                    <?php echo $this->session->userdata('notif_content'); ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="form_content" method="post" action="<?php echo base_url(); ?>backend/contact/save_content" enctype="multipart/form-data">
                                <input type="hidden" name="content_id" value="<?php echo $content->general_id; ?>">
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $this->lang->line("content"); ?> [French]</label>
                                            <textarea class="form-control" name="content_fr"><?php echo $content->general_content_fr; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $this->lang->line("content"); ?> [English]</label>
                                            <textarea class="form-control" name="content_en"><?php echo $content->general_content_en; ?></textarea>
                                        </div>
                                    </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-center">
                        <button class="btn btn-primary"><?php echo $this->lang->line("btn_update"); ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

     <section id="map" class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("map_address"); ?>
                    </div>
                    <div class="panel-body">
                    <?php echo $this->session->userdata('notif_map'); ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="form_map" method="post" action="<?php echo base_url(); ?>backend/contact/save_gmap" enctype="multipart/form-data">
                                <input type="hidden" name="id" value="<?php echo isset($map->media_id)?$map->media_id:""; ?>">
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $this->lang->line("address"); ?></label>
                                            <input class="form-control" name="address" value="<?php echo isset($map->media_content_en)?$map->media_content_en:""; ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">latitude, longitude</label>
                                            <input class="form-control" disabled="" name="latlong" value="<?php echo isset($map->media_url)?$map->media_url:""; ?>">
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-center">
                        <button class="btn btn-primary"><?php echo $this->lang->line("btn_update"); ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- BRANCH -->
    <!-- <section id="branch" class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <?php echo $this->lang->line("branch"); ?>
                            </div>
                            <div class="col-xs-12 col-sm-6 text-right">
                                <button id="addBranch" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalBranch"><strong>+ <?= $this->lang->line("btn_add"); ?></strong></button>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?= $this->session->userdata('notif_branch') ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <table id="table_branch" class="table table-hover table-bordered">
                                    <thead>
                                        <th class="text-center" width="5%">#</th>
                                        <th width="20%"><?= $this->lang->line('image') ?></th>
                                        <th width="30%"><?= $this->lang->line('name') ?></th>
                                        <th><?= $this->lang->line('address') ?></th>
                                        <th class="text-center" width="10%"><?= $this->lang->line('option') ?></th>
                                    </thead>
                                    <tbody>
                                        <?php
                                            foreach ($branch as $key => $val):
                                        ?>
                                            <tr>
                                                <td class="text-center"><?= $key+1; ?></td>
                                                <td class="text-center">
                                                    <img class="img-responsive" src="<?= base_url('assets/upload/contact/'.$val->media_url); ?>">
                                                </td>
                                                <td><?= $val->media_title_fr; ?></td>
                                                <td><?= $val->media_content_fr; ?></td>
                                                <td class="text-center">
                                                    <a href="#" data-id="<?= $val->media_id; ?>" data-toggle="modal" data-target="#modalBranch" class="btn btn-warning btn-md" title="edit"><i class="fa fa-pencil"></i></a>

                                                    <a href="<?php echo base_url('backend/contact/delete_branch/'.$val->media_id);?>" class="btn btn-danger btn-md" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="modalBranch" class="modal fade" tabindex="-1" role="dialog">
         <div class="modal-dialog" role="document">
             <div class="modal-content">
                 <form id="form_content" method="post" action="<?php echo base_url(); ?>backend/contact/save_branch" enctype="multipart/form-data">
                     <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                     </div>
                     <div class="modal-body">

                         <div class="row">
                             <div class="col-xs-12">

                                 <input type="hidden" name="branch_id" value="">
                                 <div class="col-xs-12 image-with-preview">
                                     <div class="form-group">
                                         <label class="control-label"><?php echo $this->lang->line("image"); ?></label>
                                         <input class="form-control btn btn-default" type="file" name="branch_img" required>
                                     </div>
                                     <img class="prev-img img-bordered img-responsive" alt="content image" src="<?= base_url('assets/img/blank.png') ?>">
                                     <br>
                                     <div class="row">
                                         <div class="col-xs-12 upload-rules">
                                             <ul>
                                                <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                                <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                                <li><?php echo $this->lang->line("min_resolution"); ?> <strong>450x300</strong> px</li>
                                             </ul>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="col-xs-12">
                                     <div class="form-group">
                                         <label class="control-label"><?php echo $this->lang->line("name"); ?></label>
                                         <input type="text" class="form-control" name="branch_name" required>
                                     </div>
                                 </div>
                                 <div class="col-xs-12">

                                     <div class="form-group">
                                         <label class="control-label"><?php echo $this->lang->line("address"); ?></label>
                                         <textarea class="form-control" name="branch_address" id="branch_address"></textarea>
                                     </div>
                                 </div>

                             </div>
                         </div>
                     </div>
                     <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal"><?= $this->lang->line('btn_close'); ?></button>
                         <button type="submit" class="btn btn-primary"><?= $this->lang->line('btn_save'); ?></button>
                     </div>
                 </form>
             </div>
         </div>
     </div> -->

    <!--. BRANCH -->

    <!-- MESSAGE -->
    <section id="messages" class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("messages"); ?>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
<!--                                <div class="table-responsive">-->
                                    <table id="table_message" class="table table-bordered table-hover">
                                        <thead>
                                            <th class="text-center">#</th>
                                            <th><?php echo $this->lang->line("name"); ?> (<?php echo $this->lang->line("sender"); ?>)</th>
                                            <th><?php echo $this->lang->line("email"); ?></th>
                                            <th><?php echo $this->lang->line("subject"); ?></th>
                                            <th><?php echo $this->lang->line("date"); ?></th>
                                            <th class="text-center"><?php echo $this->lang->line("option"); ?></th>
                                        </thead>
                                        <tbody>
                                            <?php foreach($messages as $key=>$val): ?>
                                            <tr data-status="<?php echo $val->contact_status; ?>">
                                                <td class="text-center"><?php echo $key+1; ?></td>
                                                <td class="msg_name"><?php echo $val->contact_name; ?></td>
                                                <td class="msg_email"><?php echo $val->contact_email; ?></td>
                                                <td class="msg_subject"><?php echo $val->contact_subject; ?></td>
                                                <td class="msg_date"><?php echo date('m/d/Y',strtotime($val->contact_date)); ?></td>
                                                <td class="text-center">
                                                    <a href="#" class="btn btn-md btn-primary btn-read" data-toggle="modal" data-target="#modal_message<?php echo $val->contact_id ?>" data-id="<?php echo $val->contact_id; ?>">
                                                    <?php
                                                        if($val->contact_status == 0){
                                                            echo '<i class="fa fa-envelope-square" aria-hidden="true"></i>';
                                                        }else{
                                                           echo '<i class="fa fa-envelope-o" aria-hidden="true"></i>'; 
                                                        }
                                                    ?>
                                                    </a>
                                                    <a href="<?php echo base_url(); ?>backend/contact/delete_message/<?php echo $val->contact_id; ?>" class="btn btn-md btn-danger" onclick="return confirm('Delete this message?');">
                                                        <i class="fa fa-trash" aria-hidden="true"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
<!--                                </div>-->
                            </div>
                        </div>
                    </div>
                    
                    <?php foreach($messages as $key=>$val): ?>
                    <div id="modal_message<?php echo $val->contact_id; ?>" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title"><?php echo $this->lang->line("read_msg"); ?></h4>
                          </div>
                          <div class="modal-body">
                            <p><?php echo $this->lang->line("from"); ?> : <?php echo $val->contact_name; ?> ( <?php echo $val->contact_email; ?> )</p>
                            <p><?php echo $this->lang->line("subject"); ?> : <?php echo $val->contact_subject; ?></p>
                            <p class="contact_date"><?php echo date('m/d/Y',strtotime($val->contact_date)); ?></p>
                            <hr>
                            <div class="well contact_message">
                                <?php echo $val->contact_message; ?>
                            </div>
                          </div>
                          <div class="modal-footer">
                            <form method="post" action="<?php echo base_url(); ?>backend/contact/read_message">
                            <input type="hidden" name="contact_id" value="<?php echo $val->contact_id; ?>">
                            <button type="submit" class="btn btn-default"><?php echo $this->lang->line("btn_close"); ?></button>
                            <a href="mailto:<?php echo $val->contact_email; ?>" class="btn btn-primary reply"><?php echo $this->lang->line("btn_reply"); ?></a>
                            
                            </form>
                          </div>
                        </div>

                      </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </section>

    <!-- /.content -->
</div>
 <script>
     $('#addBranch').click(function () {
         $('#modalBranch').find('input').val('');
         $('#modalBranch').find('input[type="file"]').attr("required","");
         $('#modalBranch').find('img').attr('src','<?= base_url("assets/img/blank.png"); ?>');
         tinyMCE.get('branch_address').setContent('');
     });


     $('a[title="edit"]').click(function () {
         var id = $(this).data('id');
         $('#modalBranch').find('input[type="file"]').removeAttr("required");

         jQuery.ajax({
             type: "POST",
             url: "<?= base_url('backend/contact/getdata_branch'); ?>",
             dataType: 'json',

             data: {id: id},
             success: function(x) {
                 $('#modalBranch').find('input[name="branch_id"]').val(x.media_id);
                 $('#modalBranch').find('img.prev-img').attr('src','<?= base_url('assets/upload/contact') ?>/'+x.media_url);
                 $('#modalBranch').find('input[name="branch_name"]').val(x.media_title_fr);
                 tinyMCE.get('branch_address').setContent(x.media_content_fr);
                 $('#branch_address').val(x.media_content_fr);
             },
         });

     });
 </script>