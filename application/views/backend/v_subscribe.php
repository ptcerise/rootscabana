 <div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="col-xs-12">
    		<h1>
                <?php echo $this->lang->line("subscription"); ?>
            </h1>
        </div>
    </section>

    <!-- Main content -->
    <!-- CRAFT ITEMS -->
    <section id="crafts" class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("subscribers"); ?>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table id="table_subs" class="table table-bordered table-hover">
                                        <thead>
                                            <th class="text-center">#</th>
                                            <th><?php echo $this->lang->line("email"); ?></th>
                                            <th class="text-center noExl"><?php echo $this->lang->line("option"); ?></th>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($subscriber as $key => $value) { ?>
                                                <tr class="craft-item">
                                                    <td class="text-center"><?php echo $key+1; ?></td>
                                                    <td><?php echo $value->subs_email; ?></td>
                                                    <td class="text-center">
                                                        <a href="<?php echo base_url('backend/subscribe/delete/'.$value->subs_id);?>" class="btn btn-danger btn-md" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
