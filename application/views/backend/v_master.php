<?php
  if($this->session->userdata('status_login') != TRUE){
    redirect('backend/Login');
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Backend | <?php echo ucfirst($current); ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="icon" href="<?php echo base_url('assets/img/favicon.ico');?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">

      <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/css/skins/_all-skins.min.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/daterangepicker/daterangepicker-bs3.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/colorpicker/bootstrap-colorpicker.min.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
   <link rel="stylesheet" href="<?php echo site_url()?>assets/css/backend/chosen.css">
  <link rel="stylesheet" href="<?php echo site_url()?>assets/css/backend/Flat.css">
  <link rel="stylesheet" href="<?php echo site_url()?>assets/css/backend/ImageSelect.css">
  <link rel="stylesheet" href="<?php echo site_url()?>assets/css/custom_be.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>


</head>
<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(); ?>backend/home" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">RC</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">Roots Cabana</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <?php $language = $this->session->userdata('be_lang'); ?>
            <?php if($language == "" || $language == "french"){ ?>
                <a href="<?php echo base_url(); ?>backend/LanguageSwitcher/switchLang/english">
                    EN
                </a>
            <?php }else{ ?>
                <a href="<?php echo base_url(); ?>backend/LanguageSwitcher/switchLang/french">
                    FR
                </a>
            <?php } ?>
          </li>

          <li class="dropdown messages-menu">
            <a href="<?php echo base_url();?>backend/contact#messages">
              <i class="fa fa-envelope-o"></i>
              <?php
                $msg = $this->db->query("SELECT * FROM contact WHERE contact_status='0'")->num_rows();
                if($msg > 0){
              
                  echo '<span class="label label-danger">';
                    
                      echo $msg;
                  
                  echo '</span>';
                 }
              ?>
            </a>
          </li>
          
         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <?php echo $this->session->userdata('user_name');?>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url('assets/img/default-user.png');?>" class="img-circle" alt="User Image">
                <p><?php echo $this->session->userdata('user_full_name');?></p>
              </li>
             
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="text-center">
                  <a href="<?php echo base_url('backend/Login/logout');?>" class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i> <?php echo $this->lang->line("sign_out"); ?></a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu">
        <li class="header"><?php echo $this->lang->line("main_menu"); ?></li>
        <li <?php if ($current == "home") { echo "class='active treeview'"; } ?>>
          <a href="<?php echo base_url();?>backend/home">
            <i class="fa fa-home"></i> <span><?php echo $this->lang->line("home"); ?></span>
          </a>
        </li>
        <li class='active treeview'>
          <a href="#">
            <i class="fa fa-shopping-bag"></i>
            <span><?php echo $this->lang->line("product"); ?></span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li <?php if ($current == "women") { echo "class='active treeview'"; } ?>>
              <a href="<?php echo base_url();?>backend/women">
                <i class="fa fa-female"></i> <span><?php echo $this->lang->line("women"); ?></span>
              </a>
            </li>
            <li <?php if ($current == "men") { echo "class='active treeview'"; } ?>>
              <a href="<?php echo base_url();?>backend/men">
                <i class="fa fa-male"></i> <span><?php echo $this->lang->line("men"); ?></span>
              </a>
            </li>
            <li <?php if ($current == "crafts") { echo "class='active treeview'"; } ?>>
              <a href="<?php echo base_url();?>backend/crafts">
                <i class="fa fa-diamond"></i> <span><?php echo $this->lang->line("crafts"); ?></span>
              </a>
            </li>
          </ul>
        <li>
        <li class='active treeview'>
          <a href="<?php echo base_url();?>backend/about">
            <i class="fa fa-info-circle"></i> <span><?php echo $this->lang->line("about"); ?></span>
            <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
            <li <?php if ($current == "about-handmade") { echo "class='active treeview'"; } ?>>
              <a href="<?php echo base_url();?>backend/about/handmade">
                <i class="fa fa-gift"></i> <span><?php echo $this->lang->line("handmade"); ?></span>
              </a>
            </li>
            <li <?php if ($current == "about-brand") { echo "class='active treeview'"; } ?>>
              <a href="<?php echo base_url();?>backend/about/brand">
                <i class="fa fa-star"></i> <span><?php echo $this->lang->line("brand"); ?></span>
              </a>
            </li>
            <li <?php if ($current == "about-shop") { echo "class='active treeview'"; } ?>>
              <a href="<?php echo base_url();?>backend/about/shop">
                <i class="fa fa-shopping-basket"></i> <span><?php echo $this->lang->line("shop"); ?></span>
              </a>
            </li>
          </ul>
        </li>
        <li <?php if ($current == "contact") { echo "class='active treeview'"; } ?>>
          <a href="<?php echo base_url();?>backend/contact">
            <i class="fa fa-envelope"></i> <span><?php echo $this->lang->line("contact"); ?></span>
          </a>
        </li>
        <li <?php if ($current == "subscribe") { echo "class='active treeview'"; } ?>>
          <a href="<?php echo base_url();?>backend/subscribe">
            <i class="fa fa-bullhorn"></i> <span><?php echo $this->lang->line("subscription"); ?></span>
          </a>
        </li>
        <li <?php if ($current == "socmed") { echo "class='active treeview'"; } ?>>
          <a href="<?php echo base_url();?>backend/socmed">
            <i class="fa fa-at"></i> <span><?php echo $this->lang->line("socmed"); ?></span>
          </a>
        </li>
        <?php if($this->session->userdata('user_level') == "1"){ ?>
        <li <?php if ($current == "user") { echo "class='active treeview'"; } ?>>
          <a href="<?php echo base_url();?>backend/user">
            <i class="fa fa-group"></i> <span><?php echo $this->lang->line("user"); ?></span>
          </a>
        </li>
        <?php } ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  
  <?php echo $content;?>
  
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs"></div>
    <strong>Copyright &copy; <?php echo date('Y'); ?> Roots Cabana | developed by <a href="http://ptcerise.com" target="blank">Cerise</a>.</strong> All rights reserved.
  </footer>

  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->


<!-- DATATABLES -->
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<!-- DATATABLES -->

<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.0.0/js/bootstrap-datetimepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.0.0/js/bootstrap-datetimepicker.min.js"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery.table2excel.min.js"></script>
 <!-- Bootstrap 3.3.6 -->
 <script src="<?php echo base_url();?>assets/plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>
 <script src="<?php echo base_url();?>assets/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js"></script>
<script src="<?php echo base_url();?>assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js"></script>

<!-- TO USE BASE_URL IN JS FILE -->
<script src="<?php echo base_url();?>assets/js/backend/chosen.jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/backend/ImageSelect.jquery.js"></script>

<!-- AdminLTE App -->
<script src="<?php echo base_url();?>assets/js/app.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('assets/js/backend/scripts.js'); ?>"></script>
<script type="text/javascript">
    var js_base_url = function( urlText ){
    var urlTmp = "<?php echo base_url('" + urlText + "'); ?>";
    return urlTmp;
    }
    $(".image-select").chosen({
      max_selected_options: 2
    }); 
</script>
</body>
</html>
