 <div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
		<h1>
            <?php echo $this->lang->line("crafts"); ?>
        </h1>
    </section>

    <!-- Main content -->

    <!-- BANNER -->
    <section id="banner" class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("banner"); ?>
                    </div>
                    <div class="panel-body">
                    <?php echo $this->session->userdata('notif_banner'); ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <form id="banner_img" method="post" action="<?php echo base_url(); ?>backend/crafts/save_banner" enctype="multipart/form-data">
                                <figure class="carousel-images">
                                    <div class="col-xs-12 col-sm-10 col-sm-offset-1 image-with-preview">
                                        <div class="form-group col-xs-12 col-sm-6 col-sm-offset-3">
                                            <label class="control-label col-xs-12"><?php echo $this->lang->line("image"); ?></label>
                                            <input type="hidden" name="banner_id" value="<?php echo $banner->media_id; ?>">
                                            <input class="form-control btn btn-default" type="file" name="banner">
                                        </div>
                                        <img class="prev-img img-bordered img-responsive" alt="banner" src="<?php echo !empty($banner->media_url)?base_url('assets/upload/crafts/'.$banner->media_url):base_url('assets/img/blank.png'); ?>">
                                    </div>
                                </figure>
                                
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-sm-offset-4 upload-rules">
                                <ul>
                                    <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                    <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                    <li><?php echo $this->lang->line("min_resolution"); ?> <strong>1024x275</strong> px</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer text-center">
                        <button class="btn btn-primary" ><?php echo $this->lang->line("btn_update"); ?></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <!-- CRAFT ITEMS -->
    <section id="crafts" class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("products"); ?>
                         <div class="caption pull-right">
                            <a href="#" class="page btn btn-success btn-xs add-craft" title="Add Craft" data-toggle="modal" data-target="#modal_crafts"><i class="fa fa-plus"></i> <?php echo $this->lang->line("item"); ?></a>
                        </div>
                    </div>
                    <div class="panel-body">
                    <?php echo $this->session->userdata('notif_crafts'); ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table id="table_crafts" class="table table-bordered table-hover">
                                        <thead>
                                            <th class="text-center">#</th>
                                            <th><?php echo $this->lang->line("image"); ?></th>
                                            <th><?php echo $this->lang->line("name"); ?></th>
                                            <th><?php echo $this->lang->line("specification"); ?> [French]</th>
                                            <th><?php echo $this->lang->line("specification"); ?> [English]</th>
                                            <th class="text-center"><?php echo $this->lang->line("option"); ?></th>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($crafts as $key => $value) { ?>
                                                <tr class="craft-item">
                                                    <td class="text-center"><?php echo $key+1; ?></td>
                                                    <td>
                                                        <img class="img-responsive thumb-craft" src="<?php echo base_url('assets/upload/crafts/'.$value->crafts_image); ?>">
                                                    </td>
                                                    <td class="craft-name"><?php echo $value->crafts_name; ?></td>
                                                    <td class="craft-spec-fr"><?php echo $value->crafts_spec_fr; ?></td>
                                                    <td class="craft-spec-en"><?php echo $value->crafts_spec_en; ?></td>
                                                    <td class="text-center">
                                                        <a href="#" data-id="<?php echo $value->crafts_id; ?>" class="btn btn-warning btn-md edit-craft" title="edit" data-toggle="modal" data-target="#update_crafts<?php echo $value->crafts_id; ?>"><i class="fa fa-edit" ></i></a>

                                                        <a href="<?php echo base_url('backend/crafts/delete_craft/'.$value->crafts_id);?>" class="btn btn-danger btn-md" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="modal_crafts" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $this->lang->line("product"); ?></h4>
              </div>
              <div class="modal-body">
                <form id="form_craft" method="post" action="<?php echo base_url(); ?>backend/crafts/save_craft" enctype="multipart/form-data">
                <input type="hidden" name="crafts_id" value="">
                    <div class="row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1 image-with-preview">
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line("image"); ?></label>
                                <input class="form-control btn btn-default" type="file" name="crafts_image">
                            </div>
                            <img class="prev-img img-bordered img-responsive" alt="craft image" src="<?php echo base_url('assets/img/blank.png'); ?>">
                        </div>
                    </div>
                    <br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-10 col-sm-offset-1 upload-rules">
                                <ul>
                                    <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                    <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                    <li><?php echo $this->lang->line("min_resolution"); ?> <strong>500x500</strong> px</li>
                                </ul>
                            </div>
                        </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line("name"); ?></label>
                                <input class="form-control" type="text" name="crafts_name" value="">
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line("specification"); ?> [French]</label>
                                <textarea id="textcraft" class="form-control" name="crafts_spec_fr">
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line("specification"); ?> [English]</label>
                                <textarea id="textcraft2" class="form-control" name="crafts_spec_en">
                                </textarea>
                            </div>
                        </div>
                    </div>
                </form>
              </div>
              <div class="modal-footer">
                
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line("btn_close"); ?></button>
                <button type="button" class=" btn btn-primary" id="btn_craft"><?php echo $this->lang->line("btn_save"); ?></button>
              </div>
            </div>

          </div>
        </div>
        
        <?php foreach($crafts as $key => $value){?>
        <div id="update_crafts<?php echo $value->crafts_id; ?>" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo $this->lang->line("product"); ?></h4>
              </div>
              <div class="modal-body">
                <form id="form_craft" method="post" action="<?php echo base_url(); ?>backend/crafts/save_craft" enctype="multipart/form-data">
                <input type="hidden" name="crafts_id" value="<?php echo $value->crafts_id; ?>">
                    <div class="row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1 image-with-preview">
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line("image"); ?></label>
                                <input class="form-control btn btn-default" type="file" name="crafts_image">
                            </div>
                            <img class="prev-img img-bordered img-responsive" alt="craft image" src="<?php echo base_url('assets/upload/crafts/'.$value->crafts_image); ?>">
                        </div>
                    </div>
                    <br>
                        <div class="row">
                            <div class="col-xs-12 col-sm-10 col-sm-offset-1 upload-rules">
                                <ul>
                                    <li><?php echo $this->lang->line("file_format"); ?> <strong>*.jpg / *.jpeg</strong></li>
                                    <li><?php echo $this->lang->line("max_file_size"); ?> <strong>2 MB</strong></li>
                                    <li><?php echo $this->lang->line("min_resolution"); ?> <strong>500x500</strong> px</li>
                                </ul>
                            </div>
                        </div>
                    <hr>
                    <div class="row">
                        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line("name"); ?></label>
                                <input class="form-control" type="text" name="crafts_name" value="<?php echo $value->crafts_name; ?>">
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line("specification"); ?> [French]</label>
                                <textarea class="form-control" name="crafts_spec_fr">
                                    <?php echo $value->crafts_spec_fr; ?>
                                </textarea>
                            </div>
                            <div class="form-group">
                                <label class="control-label"><?php echo $this->lang->line("specification"); ?> [English]</label>
                                <textarea class="form-control" name="crafts_spec_en">
                                    <?php echo $value->crafts_spec_en; ?>
                                </textarea>
                            </div>
                        </div>
                    </div>
                
              </div>
              <div class="modal-footer">
                
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line("btn_close"); ?></button>
                <button type="submit" class=" btn btn-primary"><?php echo $this->lang->line("btn_save"); ?></button>
                </form>
              </div>
            </div>

          </div>
        </div>
        <?php } ?>
    </section>

    <!-- /.content -->
</div>
