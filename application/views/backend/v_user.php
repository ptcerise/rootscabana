 <div class="content-wrapper">
	<!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="col-xs-12">
    		<h1>
                <?php echo $this->lang->line("user_manage"); ?>
            </h1>
        </div>
    </section>

    <!-- Main content -->

    <section id="user" class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <?php echo $this->lang->line("users"); ?>
                        <div class="caption pull-right">
                            <a href="#" class="page btn btn-success btn-xs add-user" title="Add User" data-toggle="modal" data-target="#modal_user"><i class="fa fa-plus"></i> <?php echo $this->lang->line("user"); ?></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <?php echo $this->session->userdata('notif'); ?>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table id="table_crafts" class="table table-bordered table-hover">
                                        <thead>
                                            <th class="text-center">#</th>
                                            <th><?php echo $this->lang->line("name"); ?></th>
                                            <th><?php echo $this->lang->line("full_name"); ?></th>
                                            <th><?php echo $this->lang->line("email"); ?></th>
                                            <th><?php echo $this->lang->line("level"); ?></th>
                                            <th class="text-center"><?php echo $this->lang->line("option"); ?></th>
                                        </thead>
                                        <tbody>
                                            <?php foreach($users as $key=>$value): ?>
                                            <tr class="tbl-row">
                                                <td class="text-center"><?php echo $key+1; ?></td>
                                                <td class="user-name"><?php echo $value->user_name; ?></td>
                                                <td class="user-fname"><?php echo $value->user_full_name; ?></td>
                                                <td class="email"><?php echo $value->user_email; ?></td>
                                                <td class="level">
                                                    <?php
                                                        if($value->user_level == "1"){
                                                            echo "Administrator";
                                                        }else{
                                                            echo "Operator";
                                                        }
                                                    ?>
                                                </td>
                                                <td class="text-center">
                                                    <a href="#" data-id="<?php echo $value->user_id; ?>" data-level="<?php echo $value->user_level; ?>" class="btn btn-warning btn-md edit-user" title="edit" data-toggle="modal" data-target="#edit_user<?php echo $value->user_id; ?>"><i class="fa fa-edit"></i></a>

                                                    <a href="<?php echo base_url('backend/user/delete/'.$value->user_id); ?>" class="btn btn-danger btn-md" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash-o"></i></a>
                                                </td>
                                            </tr>
                                            <?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div id="modal_user" class="modal fade modal_user" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo $this->lang->line("user_manage"); ?></h4>
          </div>
          <div class="modal-body">
            <form class="form_user" method="post" action="<?php echo base_url(); ?>backend/user/save">
                <input type="hidden" name="user_id" value="">
                <div class="form-group">
                    <label class="control-label"><?php echo $this->lang->line("name"); ?></label>
                    <input class="form-control" type="text" name="user_name" value="" required="">
                </div>
                <div class="form-group">
                    <label class="control-label"><?php echo $this->lang->line("full_name"); ?></label>
                    <input class="form-control" type="text" name="user_full_name" value="">
                </div>
                <div class="form-group">
                    <label class="control-label"><?php echo $this->lang->line("email"); ?></label>
                    <input class="form-control" type="email" name="user_email" value="">
                </div>
                <div class="form-group">
                    <label class="control-label"><?php echo $this->lang->line("level"); ?></label>
                    <select name="user_level" class="form-control">
                        <option value="2">Operator</option>
                        <option value="1">Administrator</option>
                    </select>
                </div>
                <hr>
                <div class="form-group">
                    <label class="control-label"><?php echo $this->lang->line("password"); ?></label>
                    <input class="form-control" type="password" name="user_pass" value="" required="">
                    <input type="checkbox" class="show_pass" name="show_pass"> <?php echo $this->lang->line("show_password"); ?>
                </div>
                <div class="form-group">
                    <label class="control-label"><?php echo $this->lang->line("c_password"); ?></label>
                    <input class="form-control" type="password" name="user_pass2" value="" required="">
                </div>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line("btn_close"); ?></button>
            <button id="save_user" type="submit" class="btn btn-primary"><?php echo $this->lang->line("btn_save"); ?></button>
            </form>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    
    <?php foreach($users as $key=>$value): ?>
    <div id="edit_user<?php echo $value->user_id; ?>" class="modal fade modal_user" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><?php echo $this->lang->line("user_manage"); ?></h4>
          </div>
          <div class="modal-body">
            <form class="form_user" method="post" action="<?php echo base_url(); ?>backend/user/save">
                <input type="hidden" name="user_id" value="<?php echo $value->user_id; ?>">
                <div class="form-group">
                    <label class="control-label"><?php echo $this->lang->line("name"); ?></label>
                    <input class="form-control" type="text" name="user_name" value="<?php echo $value->user_name; ?>" required="">
                </div>
                <div class="form-group">
                    <label class="control-label"><?php echo $this->lang->line("full_name"); ?></label>
                    <input class="form-control" type="text" name="user_full_name" value="<?php echo $value->user_full_name; ?>">
                </div>
                <div class="form-group">
                    <label class="control-label"><?php echo $this->lang->line("email"); ?></label>
                    <input class="form-control" type="email" name="user_email" value="<?php echo $value->user_email; ?>">
                </div>
                <div class="form-group">
                    <label class="control-label"><?php echo $this->lang->line("level"); ?></label>
                    <select name="user_level" class="form-control">
                        <option value="2" <?php if($value->user_level == "2"){echo "selected";}?>>
                           Operator
                        </option>
                        <option value="1" <?php if($value->user_level == "1"){echo "selected";}?>>
                            Administrator
                        </option>
                    </select>
                </div>
                <hr>
                <div class="form-group">
                    <label class="control-label"><?php echo $this->lang->line("password"); ?></label>
                    <input class="form-control" type="password" name="user_pass" value="" required="">
                    <input type="checkbox" class="show_pass" name="show_pass"> <?php echo $this->lang->line("show_password"); ?>
                </div>
                <div class="form-group">
                    <label class="control-label"><?php echo $this->lang->line("c_password"); ?></label>
                    <input class="form-control" type="password" name="user_pass2" value="" required="">
                </div>
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $this->lang->line("btn_close"); ?></button>
            <button type="submit" class="btn btn-primary"><?php echo $this->lang->line("btn_save"); ?></button>
            </form>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <?php endforeach;?>
    <!-- /.content -->
</div>
