
    <!-- Page header -->
<!--     <header class="text-center">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="header-background" style="background-image: url('img/category_header.png');">
                        <h1>Women basic</h1>
                    </div>
                </div>
            </div>
        </div>
    </header> -->
    <!-- Main content starts here -->
    <main id="mainContent">
        <section>
            <div class="container">
                <div class="row">
                    <ol class="breadcrumb">
                        <li>
                            <a href="<?php echo base_url() ?>home">
                                <?php echo ucfirst($this->lang->line("home")); ?>        
                            </a>
                        </li>
                        <li class="active"><?php echo $this->lang->line("result_for"); ?> <?php echo $keyword; ?></li>
                    </ol>
                </div>
                <div class="row product-list">
                    <?php
                        if(empty($results)){
                            echo "<h1 class='text-center'><i class='material-icons' style='font-size:30px;'>&#xE8B6;</i> ".$this->lang->line("no_result")."</h1>";
                        }else{
                    ?>
                    <?php foreach($results as $key=>$value): ?>
                        <div class="col-sm-6 col-md-3 product-item">
                            <article class="card text-center">
                                <a href="<?php echo base_url("product/detail/".$value->product_id."-".strtolower($value->product_name)) ?>">
                                    <figure class="card-img">
                                        <img alt="<?php echo $value->product_name; ?>" src="<?php echo base_url('assets/upload/product/'.$value->product_image); ?>" class="img-responsive img-hover">
                                    </figure>
                                    <div class="card-title-block">
                                        <h2 class="h4"><?php echo $value->product_name; ?></h2>
                                    </div>
                                </a>
                            </article>
                        </div>
                    <?php endforeach; ?>
                    <?php } ?>
                </div>
            </div>
        </section>
    </main>
