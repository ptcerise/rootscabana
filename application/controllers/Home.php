<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Access');
		$fe_lang = $this->session->userdata('fe_lang');
        if ($fe_lang) {
            $this->lang->load('rootscabana_frontend',$fe_lang);
        } else {
            $this->lang->load('rootscabana_frontend','french');
        }
	}



	public function index()
	{
		$data['lang'] = $this->session->userdata('fe_lang');

        $data['video'] = $this->Access->readtable('media','',array('media_page'=>'home','media_section'=>'video'))->row();

        $data['content1'] = $this->Access->readtable('general','',array('general_page'=>'home', 'general_section'=>'content1'))->row();
        $data['content2'] = $this->Access->readtable('general','',array('general_page'=>'home', 'general_section'=>'content2'))->row();
				$data['content3'] = $this->Access->readtable('general','',array('general_page'=>'home', 'general_section'=>'content3'))->row();

        $data['imagelink1'] = $this->Access->readtable('media','',array('media_page'=>'home', 'media_section'=>'imagelink1'))->row();

        $data['imagelink2'] = $this->Access->readtable('media','',array('media_page'=>'home', 'media_section'=>'imagelink2'))->row();

        $data['imagelink3'] = $this->Access->readtable('media','',array('media_page'=>'home', 'media_section'=>'imagelink3'))->row();

        $data['imagelink_about'] = $this->Access->readtable('media','',array('media_page'=>'about-handmade', 'media_section'=>'banner'))->row();
        $data['imagelink_crafts'] = $this->Access->readtable('media','',array('media_page'=>'crafts', 'media_section'=>'banner'))->row();

				$data['press'] = $this->Access->readtable('media','',array('media_page'=>'home', 'media_section'=>'press'))->result();

        $data['access'] = $this->Access;
        $data['body_class'] = "home";

        // --INSTAGRAM-- [START]
        function processURL($url)
        {
            $ch = curl_init();
            curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => 2
            ));

            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
        }

        $instagram = $this->Access->readtable("media","",array("media_page"=>"home","media_section"=>"instagram"))->row();

        $access_token = $instagram->media_url;
        $x = explode('.',$access_token);
        $user_id = $x[0];
        $url = 'https://api.instagram.com/v1/users/'.$user_id.'/media/recent/?access_token='.$access_token;

        $all_result  = processURL($url);
        $decoded_results = json_decode($all_result, true);

        $data['images'] = $decoded_results['data'];
        $data['ig_tag'] = $instagram->media_title_fr;
        // --INSTAGRAM-- [END]

		$view['content'] = $this->load->view('v_home',$data,TRUE);
		$this->load->view('v_master', $view, FALSE);
	}
}
