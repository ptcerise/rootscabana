<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Access');
		$fe_lang = $this->session->userdata('fe_lang');
        if ($fe_lang) {
            $this->lang->load('rootscabana_frontend',$fe_lang);
        } else {
            $this->lang->load('rootscabana_frontend','french');
        }
	}

	public function index()
	{	
		$data['lang'] = $this->session->userdata('fe_lang');
		$data['products'] = $this->db->query("SELECT * FROM product ORDER BY product_date DESC")->result();
		$data['filter'] = "all product";
		
		$data['body_class'] = "category";
		$view['content'] = $this->load->view('v_category',$data,TRUE);
		$this->load->view('v_master',$view);
	}

	public function category($category){
		$c = explode("-",$category);
		$data['banner'] = $this->Access->readtable('media','',array('media_page'=>$c[0],'media_section'=>'banner'))->row();
		$data['products'] = $this->db->query("SELECT * FROM product WHERE product_category='$category' ORDER BY product_date DESC")->result();
		$data['prev'] = $c[0];
		$data['filter'] = $c;

		if(strpos($category, 'casual-chic') === TRUE || strpos($category, 'beach-wear') === TRUE){
			$data['products'] = $this->db->query("SELECT * FROM product WHERE product_category LIKE '%$category%' ORDER BY product_date DESC")->result();
			$data['prev'] = "product";
			$data['filter'] = $category;
		}
		$data['lang'] = $this->session->userdata('fe_lang');

		$data['body_class'] = "category";
		$view['content'] = $this->load->view('v_category',$data,TRUE);
		$this->load->view('v_master',$view);
	}

	public function detail($id){
    	if($id == ""){
    		redirect('product');
    	}

    	$x = explode("-",$id);
    	$id = $x[0];

    	$data['lang'] = $this->session->userdata('fe_lang');
        $product = $this->Access->readtable('product','',array('product_id'=>$id))->row();
        $x = explode('-',$product->product_category);
        $category = $x[0];

        $data['product'] = $product;
        $data['product_detail'] = $this->Access->readtable('product_detail','',array('temp_id'=>$id))->result();
        $data['product_color'] = $this->Access->readtable('product_color','',array('temp_id'=>$id))->result();

        if($category == 'women'){
            $data['other_product'] = $this->db->query("SELECT * FROM product WHERE product_id!='$id' AND product_category LIKE '%$category%' ORDER BY rand() LIMIT 3")->result();
        }else{
            $data['other_product'] = $this->db->query("SELECT * FROM product WHERE product_id!='$id' AND product_category NOT LIKE '%women%' ORDER BY rand() LIMIT 3")->result();
        }


        $data['body_class'] = "item-single";
        $view['content'] = $this->load->view('v_item_single',$data,TRUE);
        $this->load->view('v_master',$view);
    }
    
    public function search(){
        $keyword = $this->input->get("keyword",TRUE);
        
        $data['lang'] = $this->session->userdata('fe_lang');
        $data['keyword'] = $keyword;
        $data['results'] = $this->db->query("SELECT * FROM product WHERE product_name LIKE '%$keyword%' ORDER BY product_date DESC")->result();
        $data['body_class'] = "search-results";
		$view['content'] = $this->load->view('v_search_results',$data,TRUE);
		$this->load->view('v_master',$view);
    }

}
