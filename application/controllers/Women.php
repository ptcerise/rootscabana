<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Women extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Access');
		$fe_lang = $this->session->userdata('fe_lang');
        if ($fe_lang) {
            $this->lang->load('rootscabana_frontend',$fe_lang);
        } else {
            $this->lang->load('rootscabana_frontend','french');
        }
	}

	public function index()
	{	
		$data['lang'] = $this->session->userdata('fe_lang');
		
		$data['banner'] = $this->Access->readtable("media","",array("media_page"=>"women","media_section"=>"banner"))->row();
		$data['women_basic'] = $this->Access->readtable("category","",array("category_name"=>"women-basic"))->row();
		$data['women_print'] = $this->Access->readtable("category","",array("category_name"=>"women-print"))->row();
		$data['women_beachwear'] = $this->Access->readtable("category","",array("category_name"=>"women-beach-wear"))->row();
		$data['women_casualchic'] = $this->Access->readtable("category","",array("category_name"=>"women-casual-chic"))->row();

		$data['products_basic'] = $this->db->query("SELECT * FROM product WHERE product_category='women-basic' AND product_display='true' ORDER BY product_date DESC")->result();
		$data['products_print'] = $this->db->query("SELECT * FROM product WHERE product_category='women-print' AND product_display='true' ORDER BY product_date DESC")->result();
		$data['products_beachwear'] = $this->db->query("SELECT * FROM product WHERE product_category='women-beach-wear' AND product_display='true' ORDER BY product_date DESC")->result();
		$data['products_casualchic'] = $this->db->query("SELECT * FROM product WHERE product_category='women-casual-chic' AND product_display='true' ORDER BY product_date DESC")->result();

		$data['body_class'] = "section-women";
		
		$view['content'] = $this->load->view('v_women',$data,TRUE);
		$this->load->view('v_master',$view);
	}

}
