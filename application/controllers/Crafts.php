<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crafts extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Access');
		$fe_lang = $this->session->userdata('fe_lang');
        if ($fe_lang) {
            $this->lang->load('rootscabana_frontend',$fe_lang);
        } else {
            $this->lang->load('rootscabana_frontend','french');
        }
	}

	public function index()
	{	
		$data['lang'] = $this->session->userdata('fe_lang');
		$data['banner'] = $this->Access->readtable('media','',array('media_page'=>'crafts','media_section'=>'banner'))->row();
        $data['crafts'] = $this->Access->readtable('crafts')->result();
        
		$data['body_class'] = "crafts";
		$view['content'] = $this->load->view('v_crafts',$data,TRUE);
		$this->load->view('v_master',$view);
	}
}
