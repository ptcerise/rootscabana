<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('rootscabana_backend',$be_lang);
        } else {
            $this->lang->load('rootscabana_backend','french');
        }
    }

    public function index()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }

        $data['lang'] = $this->session->userdata('be_lang');
        $data['banner'] = $this->Access->readtable('media','',array('media_page'=>'contact','media_section'=>'banner'))->row();
        $data['content'] = $this->Access->readtable('general','',array('general_page'=>'contact','general_section'=>'content'))->row();
        $data['messages'] = $this->db->query("SELECT * FROM contact ORDER BY contact_date DESC")->result();
        $data['branch'] = $this->Access->readtable('media','',array('media_page'=>'contact','media_section'=>'branch'))->result();

        $data['map'] = $this->Access->readtable('media','',array('media_page'=>'contact','media_section'=>'map'))->row();

        $data['current'] = "contact";
        $view['content'] = $this->load->view('backend/v_contact',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }# func index

    public function save_banner(){
        $id = $this->input->post('banner_id');

        if(!empty($_FILES['banner']['name'])){
	        $gettoId = $this->db->query("SELECT * FROM media WHERE media_page = 'contact' AND media_section = 'banner' AND media_id = '".$id."' ")->row()->media_url;

	        $media_url  =   $_FILES['banner']['name'];
	        $break      =   explode('.', $media_url);
	        $ext        =   strtolower($break[count($break) - 1]);
	        $date       =   date('dmYHis');
	        $media_url  =   'banner_'.$date.'.'.$ext;
	        $path       =   './assets/upload/contact';

	        if( ! file_exists( $path ) ){
	            $create = mkdir($path, 0777, TRUE);
	            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
	            if( ! $create || ! $createTemp )
	                return;
	        }

	        $this->piclib->get_config($media_url, $path);
	        if( $this->upload->do_upload('banner') ){
	            $image = array('upload_data' => $this->upload->data());
	            $source_path = $image['upload_data']['full_path'];
	            $width = $image['upload_data']['image_width'];
	            $height = $image['upload_data']['image_height'];

	            if( $width < 1024 || $height < 275 ){
	                unlink( realpath( APPPATH.'../assets/upload/contact/'.$media_url ));
	                unlink( realpath( APPPATH.'../assets/upload/contact/thumbnail/'.$media_url ));
	                $image_1024px = "Minimum image size 1024 x 275 px or bigger in the same ratio";
	                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_1024px.'</div>';
	            }else{
	                $orientation = $this->piclib->orientation($source_path);
	                if( $orientation == 'portrait' || $orientation == 'square'){
	                    unlink( realpath( APPPATH.'../assets/upload/contact/'.$media_url ));
	                    unlink( realpath( APPPATH.'../assets/upload/contact/thumbnail/'.$media_url ));
	                    $lands_square = "Image orientation must be 'landscape'";
	                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';
	                }else{
	                    $this->piclib->resize_image($source_path, $width, $height, 1024, 275);
	                    if( $this->image_lib->resize() ){
	                        $this->image_lib->clear();
	                        $this->piclib->resize_image($source_path, $width, $height, 1024, 275, $path.'/thumbnail');
	                        $this->image_lib->resize();

	                        unlink( realpath( APPPATH.'../assets/upload/contact/'.$gettoId ));
	                        unlink( realpath(APPPATH.'../assets/upload/contact/thumbnail/'.$gettoId ));

	                        $media_url = array(
	                            'media_url'=> $media_url
	                         );

	                        $this->db->trans_begin();
	                        $this->db->set('media_date', 'NOW()', FALSE);
	                        $this->db->where('media_id', $id)->update('media',$media_url);
	                        $this->db->trans_complete();

	                        if ($this->db->trans_status() === FALSE){
	                            $this->db->trans_rollback();
	                            $updd = "Updated failed!";
	                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	                        }else{
	                            $updd = "Updated successfully!";
	                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	                        }

                            #### COMPRESS IMAGE FILE SIZE ####
                            function compress_image($source_url, $destination_url, $quality) {
                                $info = getimagesize($source_url);

                                if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
                                elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
                                elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

                                //save file
                                imagejpeg($image, $destination_url, $quality);

                                //return destination file
                                return $destination_url;
                            }

                            //usage
                            $src = 'assets/upload/contact'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
                            // echo $src;
                            $dest = 'assets/upload/contact'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
                            $compressed = compress_image($src, $dest, 80);
	                    }
	                }
	            }
	        }else{
	            $error = $this->upload->display_errors();
	            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';
	        }
	    }

        $this->session->set_flashdata('notif_banner', $notif);
        redirect('backend/contact#banner');

    }# func save carousel

    public function save_content(){
    	$content_en = $this->input->post('content_en');
    	$content_fr = $this->input->post('content_fr');
    	$id = $this->input->post('content_id');

    	$save_content = array(
    		'general_content_en'=>$content_en,
    		'general_content_fr'=>$content_fr,
    		'general_date'=>date("Y-m-d H:i:s")
    		);
    	$this->db->trans_begin();
        $this->Access->updatetable('general',$save_content,array('general_page'=>'contact','general_section'=>'content','general_id'=>$id));
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $updd = "Updated failed!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }else{
            $updd = "Updated successfully!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }

	    $this->session->set_flashdata('notif_content', $notif);
        redirect('backend/contact#content');

    }# func save content

    public function read_message(){
    	$id = $this->input->post('contact_id');
    	$save = array(
    		'contact_status'=>"1",
    		);

    	$this->db->trans_begin();
    	$this->Access->updatetable('contact', $save ,array('contact_id'=>$id));
    	$this->db->trans_complete();

    	redirect("backend/contact#messages");
    }

    public function delete_message($id){
    	$this->db->trans_begin();
    	$this->Access->deletetable('contact',array('contact_id'=>$id));
    	$this->db->trans_complete();

    	redirect("backend/contact#messages");
    }

    public function save_branch(){
        $branch_name = $this->input->post('branch_name');
        $branch_address = $this->input->post('branch_address');
        $id = $this->input->post('branch_id');

        if($branch_address == ""){
            $updd = "Some fields cannot be empty!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
            $this->session->set_flashdata('notif_branch', $notif);
            redirect('backend/contact#branch');
        }

        if(!empty($_FILES['branch_img']['name'])){
            if(!empty($id)){
                $gettoId = $this->db->query("SELECT * FROM media WHERE media_id='".$id."'")->row()->media_url;
            }

            $media_url  =   $_FILES['branch_img']['name'];
            $break      =   explode('.', $media_url);
            $ext        =   strtolower($break[count($break) - 1]);
            $date       =   date('dmYHis');
            $media_url  =   'branch_'.$date.'.'.$ext;
            $path       =   './assets/upload/contact';

            if( ! file_exists( $path ) ){
                $create = mkdir($path, 0777, TRUE);
                $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
                if( ! $create || ! $createTemp )
                    return;
            }

            $this->piclib->get_config($media_url, $path);
            if( $this->upload->do_upload('branch_img') ){
                $image = array('upload_data' => $this->upload->data());
                $source_path = $image['upload_data']['full_path'];
                $width = $image['upload_data']['image_width'];
                $height = $image['upload_data']['image_height'];

                if( $width < 450 || $height < 300 ){
                    unlink( realpath( APPPATH.'../assets/upload/contact/'.$media_url ));
                    unlink( realpath( APPPATH.'../assets/upload/contact/thumbnail/'.$media_url ));
                    $image_res = "Minimum image size 450 x 300 px or bigger in the same ratio";
                    $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_res.'</div>';
                }else{
                    $orientation = $this->piclib->orientation($source_path);
                    if( $orientation == 'portrait' || $orientation == 'square'){
                        unlink( realpath( APPPATH.'../assets/upload/contact/'.$media_url ));
                        unlink( realpath( APPPATH.'../assets/upload/contact/thumbnail/'.$media_url ));
                        $lands_square = "Image orientation must be 'landscape'";
                        $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';
                    }else{
                        $this->piclib->resize_image($source_path, $width, $height, 450, 300);
                        if( $this->image_lib->resize() ){
                            $this->image_lib->clear();
                            $this->piclib->resize_image($source_path, $width, $height, 450, 300, $path.'/thumbnail');
                            $this->image_lib->resize();

                            if(!empty($id)){
                                unlink( realpath( APPPATH.'../assets/upload/contact/'.$gettoId ));
                                unlink( realpath(APPPATH.'../assets/upload/contact/thumbnail/'.$gettoId ));
                            }

                            $save_content = array(
                                'media_title_fr'=>$branch_name,
                                'media_content_fr'=>$branch_address,
                                'media_url'=> $media_url,
                                'media_page'=>'contact',
                                'media_section'=>'branch',
                                'media_date'=> date('Y-m-d H:i:s')
                            );

                            $this->db->trans_begin();
                            if(!empty($id)){
                                $this->Access->updatetable('media',$save_content,array('media_id'=>$id));
                            }else{
                                $this->Access->inserttable('media',$save_content);
                            }
                            $this->db->trans_complete();

                            if ($this->db->trans_status() === FALSE){
                                $this->db->trans_rollback();
                                $updd = "Updated failed!";
                                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                            }else{
                                $updd = "Updated successfully!";
                                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                            }

                            #### COMPRESS IMAGE FILE SIZE ####
                            function compress_image($source_url, $destination_url, $quality) {
                                $info = getimagesize($source_url);

                                if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
                                elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
                                elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

                                //save file
                                imagejpeg($image, $destination_url, $quality);

                                //return destination file
                                return $destination_url;
                            }

                            //usage
                            $src = 'assets/upload/contact'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
                            // echo $src;
                            $dest = 'assets/upload/contact'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
                            $compressed = compress_image($src, $dest, 80);
                        }
                    }
                }
            }else{
                $error = $this->upload->display_errors();
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';
            }
        }else{
            $save_content = array(
                'media_title_fr'=>$branch_name,
                'media_content_fr'=>$branch_address,
                'media_page'=>'contact',
                'media_section'=>'branch',
                'media_date'=> date('Y-m-d H:i:s')
            );
            $this->db->trans_begin();
            if(!empty($id)){
                $this->Access->updatetable('media',$save_content,array('media_id'=>$id));
            }else{
                $this->Access->inserttable('media',$save_content);
            }
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $updd = "Updated failed!";
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
            }else{
                $updd = "Updated successfully!";
                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
            }
        }

        $this->session->set_flashdata('notif_branch', $notif);
        redirect('backend/contact#branch');

    }# func save branch

    public function getdata_branch(){
        $id = $this->input->post('id');

        $q = $this->db->get_where('media',array('media_id'=>$id))->row();

        $data = array(
            'media_id'=>$q->media_id,
            'media_title_fr'=>$q->media_title_fr,
            'media_content_fr'=>$q->media_content_fr,
            'media_url'=>$q->media_url
        );

        echo json_encode($data);
    }

    public function delete_branch($id){
        $this->db->trans_begin();
        $img = $this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
        unlink( realpath( APPPATH.'../assets/upload/contact/'.$img ));
        unlink( realpath( APPPATH.'../assets/upload/contact/thumbnail/'.$img ));

        $this->db->delete('media',array('media_id'=>$id));
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }

        redirect('backend/contact#branch');
    }

    public function save_gmap(){
        $id = $this->input->post("id");
        $address = $this->input->post("address");

        # SETTING UP MAPS
        function geocode($address){
            // url encode the address
            $address = urlencode($address);

            // google map geocode api url
            $url = "http://maps.google.com/maps/api/geocode/json?sensor=false&address={$address}";

            // get the json response
            $resp_json = file_get_contents($url);

            // decode the json
            $resp = json_decode($resp_json, true);

            // response status will be 'OK', if able to geocode given address
            if($resp['status']='OK'){

                // get the important data
                $lat = $resp['results'][0]['geometry']['location']['lat'];
                $lng = $resp['results'][0]['geometry']['location']['lng'];
                $formatted_address = $resp['results'][0]['formatted_address'];
                // $locality = $resp['results'][0]['address_components'][3]['long_name'];

                // verify if data is complete
                if($lat && $lng && $formatted_address){

                    // put the data in the array
                    $data_arr = array(
                            'latitude'=>$lat,
                            'longitude'=>$lng,
                            'formatted_address'=>$formatted_address
                    );
                    return $data_arr;

                }else{
                    return false;
                }

            }else{
                return false;
            }
        }

        $data_location = geocode($address);
        $lat = $data_location['latitude'];
        $lng = $data_location['longitude'];

        $latlong = $lat.', '.$lng;

        $save_content = array(
            'media_page'=>'contact',
            'media_section'=>'map',
            'media_url'=>$latlong,
            'media_content_en'=>$address,
            'media_date'=>date("Y-m-d H:i:s")
        );

        if(!empty($id)){
            $this->db->trans_begin();
            $this->Access->updatetable('media',$save_content,array('media_page'=>'contact','media_section'=>'map','media_id'=>$id));
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $updd = "Updated failed!";
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
            }else{
                $updd = "Updated successfully!";
                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
            }
        }else{
            $this->db->trans_begin();
            $this->Access->inserttable('media',$save_content);
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $updd = "Updated failed!";
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
            }else{
                $updd = "Updated successfully!";
                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
            }
        }

        $this->session->set_flashdata('notif_map', $notif);
        redirect('backend/contact#map');


    }

}
