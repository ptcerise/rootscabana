<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Login extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Access');
	}

	function index()
	{	       
		if($this->session->userdata('status_login') == TRUE){
			redirect('backend/home');
		}
		$this->load->view('backend/v_login');
	}

	function process(){
		$this->load->model('Access');
		$user_name = $this->input->post('user_name');
		$user_pass = md5($this->input->post('user_pass'));
		$check = $this->Access->readtable('user', '', array('user_name' => $user_name, 'user_pass' => $user_pass));

		if($check->num_rows() > 0){
			$login = array(
					'status_login' => TRUE,
					'user_id' => $check->row()->user_id,
					'user_name' => $check->row()->user_name,
					'user_pass' => $check->row()->user_pass,
					'user_full_name' => $check->row()->user_full_name,
					'user_level' => $check->row()->user_level
					);
			$this->session->set_userdata($login);
			
			$user_id = $this->session->userdata('user_id');
			redirect('backend/home');
		}

		else{
			// echo $check->row()->user_id;
			$notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>Username or password is wrong.</div>';
			$_SESSION['login'] = $notif;
			$this->session->mark_as_flash('login');
			redirect('backend/login');
		}
	}

	function logout(){
		$user_id = $this->session->userdata('user_id');
		$this->session->sess_destroy();
		redirect('backend/login');
	}
}