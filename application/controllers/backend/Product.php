<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('rootscabana_backend',$be_lang);
        } else {
            $this->lang->load('rootscabana_backend','french');
        }
    }

    public function index()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }
        $data['lang'] = $this->session->userdata('be_lang');
        redirect('backend/home');
    }# func index

    public function detail($id){
    	if($id == ""){
    		redirect('backend/product');
    	}

    	if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }

        $data['lang'] = $this->session->userdata('be_lang');
        $data['product'] = $this->Access->readtable('product','',array('product_id'=>$id))->row();
        $data['product_detail'] = $this->Access->readtable('product_detail','',array('temp_id'=>$id))->result();
        $data['product_color'] = $this->Access->readtable('product_color','',array('temp_id'=>$id))->result();

        $data['current'] = "detail";
        $view['content'] = $this->load->view('backend/v_detail',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    public function save_product(){
    	$name = $this->input->post('product_name');
    	$category = $this->input->post('product_category');
    	$id = $this->input->post('product_id');

    	if(!empty($_FILES['product_image']['name'])){
	        if(!empty($id)){$gettoId = $this->db->query("SELECT * FROM product WHERE product_id='".$id."'")->row()->product_image;}

	        $media_url  =   $_FILES['product_image']['name'];
	        $break      =   explode('.', $media_url);
	        $ext        =   strtolower($break[count($break) - 1]);
	        $date       =   date('dmYHis');
	        $media_url  =   'product_'.$date.'.'.$ext;
	        $path       =   './assets/upload/product';

	        if( ! file_exists( $path ) ){
	            $create = mkdir($path, 0777, TRUE);
	            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
	            if( ! $create || ! $createTemp )
	                return;
	        }

	        $this->piclib->get_config($media_url, $path);
	        if( $this->upload->do_upload('product_image') ){
	            $image = array('upload_data' => $this->upload->data());
	            $source_path = $image['upload_data']['full_path'];
	            $width = $image['upload_data']['image_width'];
	            $height = $image['upload_data']['image_height'];

	            if( $width < 500 || $height < 750 ){
	                unlink( realpath( APPPATH.'../assets/upload/product/'.$media_url ));
	                unlink( realpath( APPPATH.'../assets/upload/product/thumbnail/'.$media_url ));
	                $image_res = "Minimum image size 500 x 750 px or bigger in the same ratio";
	                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_res.'</div>';
	            }else{
	                $orientation = $this->piclib->orientation($source_path);
	                if( $orientation == 'landscape' || $orientation == 'square'){
	                    unlink( realpath( APPPATH.'../assets/upload/product/'.$media_url ));
	                    unlink( realpath( APPPATH.'../assets/upload/product/thumbnail/'.$media_url ));
	                    $lands_square = "Image orientation must be 'portrait'";
	                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';
	                }else{
	                    $this->piclib->resize_image($source_path, $width, $height, 500, 750);
	                    if( $this->image_lib->resize() ){
	                        $this->image_lib->clear();
	                        $this->piclib->resize_image($source_path, $width, $height, 500, 750, $path.'/thumbnail');
	                        $this->image_lib->resize();

	                        if(!empty($id)){
		                        unlink( realpath( APPPATH.'../assets/upload/product/'.$gettoId ));
		                        unlink( realpath(APPPATH.'../assets/upload/product/thumbnail/'.$gettoId ));
		                    }

	                        $save = array(
	                        	'product_name'=> $name,
	                            'product_image'=> $media_url,
	                            'product_category'=> $category,
	                            'product_date'=>date('Y-m-d H:i:s'),
	                         );


	                        $this->db->trans_begin();
	                        $this->Access->inserttable('product',$save);
	                        $this->db->trans_complete();

	                        if ($this->db->trans_status() === FALSE){
	                            $this->db->trans_rollback();
	                            $updd = "Insert data failed!";
	                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	                        }else{
	                            $updd = "Insert data success!";
	                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	                        }

	                        #### COMPRESS IMAGE FILE SIZE ####
	                        function compress_image($source_url, $destination_url, $quality) {
								$info = getimagesize($source_url);

								if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
								elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
								elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

								//save file
								imagejpeg($image, $destination_url, $quality);

								//return destination file
								return $destination_url;
							}

							//usage
							$src = 'assets/upload/product'.'/'.$media_url;
							$dest = 'assets/upload/product'.'/'.$media_url;
							$compressed = compress_image($src, $dest, 80);
	                    }
	                }
	            }
	        }else{
	            $error = $this->upload->display_errors();
	            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';
	        }
	    }
	    $this->session->set_flashdata(array('notif_product'=>$notif));
	    redirect($_SERVER['HTTP_REFERER']."#product");
    }

    public function update_product(){
    	$id = $this->input->post('product_id');
    	$name = $this->input->post('product_name');
    	$category = $this->input->post('product_category');
    	$content_en = $this->input->post('product_content_en');
    	$spec_en = $this->input->post('product_spec_en');
    	$content_fr = $this->input->post('product_content_fr');
    	$spec_fr = $this->input->post('product_spec_fr');

    	if(!empty($_FILES['product_image']['name'])){
	        $gettoId = $this->db->query("SELECT * FROM product WHERE product_id='".$id."'")->row()->product_image;

	        $media_url  =   $_FILES['product_image']['name'];
	        $break      =   explode('.', $media_url);
	        $ext        =   strtolower($break[count($break) - 1]);
	        $date       =   date('dmYHis');
	        $media_url  =   'product_'.$date.'.'.$ext;
	        $path       =   './assets/upload/product';

	        if( ! file_exists( $path ) ){
	            $create = mkdir($path, 0777, TRUE);
	            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
	            if( ! $create || ! $createTemp )
	                return;
	        }

	        $this->piclib->get_config($media_url, $path);
	        if( $this->upload->do_upload('product_image') ){
	            $image = array('upload_data' => $this->upload->data());
	            $source_path = $image['upload_data']['full_path'];
	            $width = $image['upload_data']['image_width'];
	            $height = $image['upload_data']['image_height'];

	            if( $width < 500 || $height < 750 ){
	                unlink( realpath( APPPATH.'../assets/upload/product/'.$media_url ));
	                unlink( realpath( APPPATH.'../assets/upload/product/thumbnail/'.$media_url ));
	                $image_res = "Minimum image size 500 x 750 px or bigger in the same ratio";
	                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_res.'</div>';
	            }else{
	                $orientation = $this->piclib->orientation($source_path);
	                if( $orientation == 'landscape' || $orientation == 'square'){
	                    unlink( realpath( APPPATH.'../assets/upload/product/'.$media_url ));
	                    unlink( realpath( APPPATH.'../assets/upload/product/thumbnail/'.$media_url ));
	                    $lands_square = "Image orientation must be 'portrait'";
	                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';
	                }else{
	                    $this->piclib->resize_image($source_path, $width, $height, 500, 750);
	                    if( $this->image_lib->resize() ){
	                        $this->image_lib->clear();
	                        $this->piclib->resize_image($source_path, $width, $height, 500, 750, $path.'/thumbnail');
	                        $this->image_lib->resize();

	                        unlink( realpath( APPPATH.'../assets/upload/product/'.$gettoId ));
	                        unlink( realpath(APPPATH.'../assets/upload/product/thumbnail/'.$gettoId ));

	                        $save = array(
	                        	'product_name'=> $name,
	                            'product_image'=> $media_url,
	                            'product_content_en'=> $content_en,
	                            'product_spec_en'=> $spec_en,
	                            'product_content_fr'=> $content_fr,
	                            'product_spec_fr'=> $spec_fr,
	                            'product_category'=> $category,
	                         );


	                        $this->db->trans_begin();
	                        $this->Access->updatetable('product',$save,array('product_id'=>$id));
	                        $this->db->trans_complete();

	                        if ($this->db->trans_status() === FALSE){
	                            $this->db->trans_rollback();
	                            $updd = "Insert data failed!";
	                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	                        }else{
	                            $updd = "Insert data success!";
	                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	                        }

	                        #### COMPRESS IMAGE FILE SIZE ####
	                        function compress_image($source_url, $destination_url, $quality) {
								$info = getimagesize($source_url);

								if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
								elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
								elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

								//save file
								imagejpeg($image, $destination_url, $quality);

								//return destination file
								return $destination_url;
							}

							//usage
							$src = 'assets/upload/product'.'/'.$this->db->get_where('product',array('product_id'=>$id))->row()->product_image;

							$dest = 'assets/upload/product'.'/'.$this->db->get_where('product',array('product_id'=>$id))->row()->product_image;
							$compressed = compress_image($src, $dest, 80);
	                    }
	                }
	            }
	        }else{
	            $error = $this->upload->display_errors();
	            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';
	        }
	    }else{
	    	$save = array(
            	'product_name'=> $name,
                'product_content_en'=> $content_en,
                'product_spec_en'=> $spec_en,
                'product_content_fr'=> $content_fr,
                'product_spec_fr'=> $spec_fr,
                'product_category'=> $category,
                'product_date'=>date('Y-m-d H:i:s'),
             );


            $this->db->trans_begin();
            $this->Access->updatetable('product',$save,array('product_id'=>$id));
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $updd = "Insert data failed!";
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
            }else{
                $updd = "Insert data success!";
                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
            }
	    }

	    $this->session->set_flashdata(array('notif_detail'=>$notif));
	    redirect($_SERVER['HTTP_REFERER']);
    }

    public function delete($id){
    	$this->db->trans_begin();
    	$this->Access->deletetable('product',array('product_id'=>$id));
    	$this->Access->deletetable('product_detail',array('temp_id'=>$id));
    	$this->db->trans_complete();

    	redirect($_SERVER['HTTP_REFERER']."#product");
    }

    public function save_variant(){
    	$product_id = $this->input->post("product_id");
    	$detail_id = $this->input->post("detail_id");
    	$detail_color = "#fff";

    	if($detail_id == ""){
    		# insert
    		if(!empty($_FILES['detail_image']['name'])){
//		        $gettoId = $this->db->query("SELECT * FROM product_detail WHERE detail_id='".$detail_id."'")->row()->detail_image;

		        $media_url  =   $_FILES['detail_image']['name'];
		        $break      =   explode('.', $media_url);
		        $ext        =   strtolower($break[count($break) - 1]);
		        $date       =   date('dmYHis');
		        $media_url  =   'detail_'.$product_id.'_'.$date.'.'.$ext;
		        $path       =   './assets/upload/product/detail';

		        if( ! file_exists( $path ) ){
		            $create = mkdir($path, 0777, TRUE);
		            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
		            if( ! $create || ! $createTemp )
		                return;
		        }

		        $this->piclib->get_config($media_url, $path);
		        if( $this->upload->do_upload('detail_image') ){
		            $image = array('upload_data' => $this->upload->data());
		            $source_path = $image['upload_data']['full_path'];
		            $width = $image['upload_data']['image_width'];
		            $height = $image['upload_data']['image_height'];

		            if( $width < 500 || $height < 750 ){
		                // unlink( realpath( APPPATH.'../assets/upload/product/detail/'.$media_url ));
		                // unlink( realpath( APPPATH.'../assets/upload/product/detail/thumbnail/'.$media_url ));
		                $image_res = "Minimum image size 500 x 750 px or bigger in the same ratio";
		                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_res.'</div>';
		            }else{
		                $orientation = $this->piclib->orientation($source_path);
		                if( $orientation == 'landscape' || $orientation == 'square'){
		                    // unlink( realpath( APPPATH.'../assets/upload/product/detail/'.$media_url ));
		                    // unlink( realpath( APPPATH.'../assets/upload/product/detail/thumbnail/'.$media_url ));
		                    $lands_square = "Image orientation must be 'portrait'";
		                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';
		                }else{
		                    $this->piclib->resize_image($source_path, $width, $height, 500, 750);
		                    if( $this->image_lib->resize() ){
		                        $this->image_lib->clear();
		                        $this->piclib->resize_image($source_path, $width, $height, 500, 750, $path.'/thumbnail');
		                        $this->image_lib->resize();

//		                        unlink( realpath( APPPATH.'../assets/upload/product/detail/'.$gettoId ));
//		                        unlink( realpath(APPPATH.'../assets/upload/product/detail/thumbnail/'.$gettoId ));

		                        $save = array(
						    		'detail_image'=>$media_url,
						    		'detail_color'=>$detail_color,
						    		'temp_id'=>$product_id,
						    		);


		                        $this->db->trans_begin();
		                        $this->Access->inserttable('product_detail',$save);
		                        $this->db->trans_complete();

		                        if ($this->db->trans_status() === FALSE){
		                            $this->db->trans_rollback();
		                            $updd = "Insert data failed!";
		                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
		                        }else{
		                            $updd = "Insert data success!";
		                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
		                        }

		                        #### COMPRESS IMAGE FILE SIZE ####
		                        function compress_image($source_url, $destination_url, $quality) {
									$info = getimagesize($source_url);

									if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
									elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
									elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

									//save file
									imagejpeg($image, $destination_url, $quality);

									//return destination file
									return $destination_url;
								}

								//usage
								$src = 'assets/upload/product/detail'.'/'.$media_url;

								$dest = 'assets/upload/product/detail'.'/'.$media_url;
								$compressed = compress_image($src, $dest, 80);
			                    }
		                }
		            }
		        }else{
		            $error = $this->upload->display_errors();
		            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';
		        }
		    }else{
		    	$save = array(
		    		'detail_color'=>$detail_color,
		    		'temp_id'=>$product_id,
		    		);

                $this->db->trans_begin();
                $this->Access->inserttable('product_detail',$save);
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    $updd = "Insert data failed!";
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                }else{
                    $updd = "Insert data success!";
                    $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                }
		    }
    	}else{
    		# update
    		if(!empty($_FILES['detail_image']['name'])){
		        $gettoId = $this->db->query("SELECT * FROM product_detail WHERE detail_id='".$detail_id."'")->row()->detail_image;

		        $media_url  =   $_FILES['detail_image']['name'];
		        $break      =   explode('.', $media_url);
		        $ext        =   strtolower($break[count($break) - 1]);
		        $date       =   date('dmYHis');
		        $media_url  =   'detail_'.$product_id.'_'.$date.'.'.$ext;
		        $path       =   './assets/upload/product/detail';

		        if( ! file_exists( $path ) ){
		            $create = mkdir($path, 0777, TRUE);
		            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
		            if( ! $create || ! $createTemp )
		                return;
		        }

		        $this->piclib->get_config($media_url, $path);
		        if( $this->upload->do_upload('detail_image') ){
		            $image = array('upload_data' => $this->upload->data());
		            $source_path = $image['upload_data']['full_path'];
		            $width = $image['upload_data']['image_width'];
		            $height = $image['upload_data']['image_height'];

		            if( $width < 500 || $height < 750 ){
		                unlink( realpath( APPPATH.'../assets/upload/product/detail/'.$media_url ));
		                unlink( realpath( APPPATH.'../assets/upload/product/detail/thumbnail/'.$media_url ));
		                $image_res = "Minimum image size 500 x 750 px or bigger in the same ratio";
		                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_res.'</div>';
		            }else{
		                $orientation = $this->piclib->orientation($source_path);
		                if( $orientation == 'landscape' || $orientation == 'square'){
		                    unlink( realpath( APPPATH.'../assets/upload/product/detail/'.$media_url ));
		                    unlink( realpath( APPPATH.'../assets/upload/product/detail/thumbnail/'.$media_url ));
		                    $lands_square = "Image orientation must be 'portrait'";
		                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';
		                }else{
		                    $this->piclib->resize_image($source_path, $width, $height, 500, 750);
		                    if( $this->image_lib->resize() ){
		                        $this->image_lib->clear();
		                        $this->piclib->resize_image($source_path, $width, $height, 500, 750, $path.'/thumbnail');
		                        $this->image_lib->resize();

		                        unlink( realpath( APPPATH.'../assets/upload/product/detail/'.$gettoId ));
		                        unlink( realpath(APPPATH.'../assets/upload/product/detail/thumbnail/'.$gettoId ));

		                        $save = array(
						    		'detail_image'=>$media_url,
						    		'detail_color'=>$detail_color,
						    		'temp_id'=>$product_id,
						    		);


		                        $this->db->trans_begin();
		                        $this->Access->updatetable('product_detail',$save,array('detail_id'=>$detail_id));
		                        $this->db->trans_complete();

		                        if ($this->db->trans_status() === FALSE){
		                            $this->db->trans_rollback();
		                            $updd = "Update data failed!";
		                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
		                        }else{
		                            $updd = "Update data success!";
		                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
		                        }

		                        #### COMPRESS IMAGE FILE SIZE ####
		                        function compress_image($source_url, $destination_url, $quality) {
									$info = getimagesize($source_url);

									if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
									elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
									elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

									//save file
									imagejpeg($image, $destination_url, $quality);

									//return destination file
									return $destination_url;
								}

								//usage
								$src = 'assets/upload/product/detail'.'/'.$this->db->get_where('product_detail',array('detail_id'=>$id))->row()->detail_image;
								
								$dest = 'assets/upload/product/detail'.'/'.$this->db->get_where('product_detail',array('detail_id'=>$id))->row()->detail_image;
								$compressed = compress_image($src, $dest, 80);
		                    }
		                }
		            }
		        }else{
		            $error = $this->upload->display_errors();
		            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';
		        }
		    }else{
		    	$save = array(
		    		'detail_color'=>$detail_color,
		    		'temp_id'=>$product_id,
		    		);

                $this->db->trans_begin();
                $this->Access->updatetable('product_detail',$save,array('detail_id'=>$detail_id));
                $this->db->trans_complete();

                if ($this->db->trans_status() === FALSE){
                    $this->db->trans_rollback();
                    $updd = "Update data failed!";
                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                }else{
                    $updd = "Update data success!";
                    $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
                }
		    }
    	}


	    $this->session->set_flashdata(array('notif_variant'=>$notif));
	    redirect($_SERVER['HTTP_REFERER']."#variant");
    }

    public function delete_variant($id){
        $gettoId = $this->db->query("SELECT * FROM product_detail WHERE detail_id='".$id."'")->row()->detail_image;
        unlink( realpath( APPPATH.'../assets/upload/product/detail/'.$gettoId ));
        unlink( realpath(APPPATH.'../assets/upload/product/detail/thumbnail/'.$gettoId ));
    	$this->db->trans_begin();
    	$this->Access->deletetable('product_detail',array('detail_id'=>$id));
    	$this->db->trans_complete();

    	redirect($_SERVER['HTTP_REFERER']."#variant");
    }

    public function save_color(){
        $color_code = $this->input->post('color_code');
        $temp_id = $this->input->post('product_id');
        $color_id = $this->input->post('color_id');

        $save = array(
            'color_code'=>$color_code,
            'temp_id'=>$temp_id,
        );

        $this->db->trans_begin();
        if(!empty($color_id)){
            # UPDATE

            $this->Access->updatetable('product_color',$save ,array('color_id'=>$color_id));
        }else{
            # INSERT

            $this->Access->inserttable('product_color',$save);
        }
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $updd = "Update data failed!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }else{
            $updd = "Update data success!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }

        $this->session->set_flashdata(array('notif_color'=>$notif));
        redirect($_SERVER['HTTP_REFERER']."#color");
    }

    public function delete_color($id){
        $this->db->trans_begin();
        $this->Access->deletetable('product_color',array('color_id'=>$id));
        $this->db->trans_complete();

        redirect($_SERVER['HTTP_REFERER']."#color");
    }

}
