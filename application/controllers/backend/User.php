<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('rootscabana_backend',$be_lang);
        } else {
            $this->lang->load('rootscabana_backend','french');
        }
    }
    
    public function index()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }

        if($this->session->userdata('user_level') != "1"){
            redirect('backend/home');
        }
        
        $data['lang'] = $this->session->userdata('be_lang');
        $data['users'] = $this->Access->readtable("user")->result();

        $data['current'] = "user";
        $view['content'] = $this->load->view('backend/v_user',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }# func index

    public function save(){
        $user_id = $this->input->post("user_id");
        $user_name = $this->input->post("user_name");
        $user_full_name = $this->input->post("user_full_name");
        $user_email = $this->input->post("user_email");
        $user_level = $this->input->post("user_level");
        $user_pass = $this->input->post("user_pass");

        if($user_id == ""){
            #insert
            $save = array(
                'user_name'=>$user_name,
                'user_pass'=>md5($user_pass),
                'user_email'=>$user_email,
                'user_full_name'=>$user_full_name,
                'user_level'=>$user_level,
                );
            $this->db->trans_begin();
            $this->Access->inserttable("user",$save);
            $this->db->trans_complete();
        }else{
            #update
            if($user_pass == ""){
                $save = array(
                'user_name'=>$user_name,
                'user_email'=>$user_email,
                'user_full_name'=>$user_full_name,
                'user_level'=>$user_level,
                );
            }else{
                $save = array(
                'user_name'=>$user_name,
                'user_pass'=>md5($user_pass),
                'user_email'=>$user_email,
                'user_full_name'=>$user_full_name,
                'user_level'=>$user_level,
                );
            }
            $this->db->trans_begin();
            $this->Access->updatetable("user",$save,array("user_id"=>$user_id));
            $this->db->trans_complete();
        }

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $updd = "Update error!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }else{
            $updd = "Update success!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }
        $this->session->set_flashdata(array("notif"=>$notif));
        redirect($_SERVER['HTTP_REFERER']);
    }#end save()

    public function delete($id){
        $this->db->trans_begin();
        $this->Access->deletetable("user",array("user_id"=>$id));
        $this->db->trans_complete();
        redirect($_SERVER['HTTP_REFERER']);
    }# end delete()

}

