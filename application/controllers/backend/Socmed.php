<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Socmed extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Access');
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('rootscabana_backend',$be_lang);
        } else {
            $this->lang->load('rootscabana_backend','french');
        }
	}

	function index()
	{	       
		if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }
        
        $data['lang'] = $this->session->userdata('be_lang');
        $data['facebook'] = $this->Access->readtable("media","",array("media_page"=>"all","media_section"=>"link_facebook"))->row();
        $data['twitter'] = $this->Access->readtable("media","",array("media_page"=>"all","media_section"=>"link_twitter"))->row();
        $data['instagram'] = $this->Access->readtable("media","",array("media_page"=>"all","media_section"=>"link_instagram"))->row();
        $data['pinterest'] = $this->Access->readtable("media","",array("media_page"=>"all","media_section"=>"link_pinterest"))->row();
        $data['email'] = $this->Access->readtable("media","",array("media_page"=>"all","media_section"=>"link_email"))->row();

        $data['igsetting'] = $this->Access->readtable("media","",array("media_page"=>"home","media_section"=>"instagram"))->row();

        $data['current'] = "socmed";
        $view['content'] = $this->load->view('backend/v_socmed',$data,TRUE);
        $this->load->view('backend/v_master',$view);
	}

	public function save(){
		$link_facebook = $this->input->post("link_facebook");
		$link_twitter = $this->input->post("link_twitter");
		$link_instagram = $this->input->post("link_instagram");
		$link_pinterest = $this->input->post("link_pinterest");
		$link_email = $this->input->post("link_email");

		$this->db->trans_begin();
		$this->Access->updatetable("media",array("media_url"=>$link_facebook),array("media_section"=>"link_facebook"));
		$this->Access->updatetable("media",array("media_url"=>$link_twitter),array("media_section"=>"link_twitter"));
		$this->Access->updatetable("media",array("media_url"=>$link_instagram),array("media_section"=>"link_instagram"));
		$this->Access->updatetable("media",array("media_url"=>$link_pinterest),array("media_section"=>"link_pinterest"));
		$this->Access->updatetable("media",array("media_url"=>$link_email),array("media_section"=>"link_email"));
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $updd = "Update error!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }else{
            $updd = "Update success!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }
        $this->session->set_flashdata(array("notif"=>$notif));
        redirect($_SERVER['HTTP_REFERER']."#socmed");

	}

    public function save_igsetting(){
        $tag = $this->input->post('tag');
        $access_token = $this->input->post('access_token');

        $check = $this->Access->readtable("media","",array("media_page"=>"home","media_section"=>"instagram"))->num_rows();

        $this->db->trans_begin();
        if($check == 0){
            #insert
            $data = array(
                'media_title_fr'=>$tag,
                'media_url'=>$access_token,
                'media_page'=>'home',
                'media_section'=>'instagram',
            );
            $this->Access->inserttable("media",$data);
        }else{
            #update
            $data = array(
                'media_title_fr'=>$tag,
                'media_url'=>$access_token,
            );
            $this->Access->updatetable("media",$data,array("media_page"=>"home","media_section"=>"instagram"));
        }
        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $updd = "Update error!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }else{
            $updd = "Update success!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }
        $this->session->set_flashdata(array("notif_ig"=>$notif));
        redirect($_SERVER['HTTP_REFERER']."#instagram");
    }

}