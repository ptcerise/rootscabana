<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crafts extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('rootscabana_backend',$be_lang);
        } else {
            $this->lang->load('rootscabana_backend','french');
        }

    }

    public function index()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }

        $data['lang'] = $this->session->userdata('be_lang');
        $data['banner'] = $this->Access->readtable('media','',array('media_page'=>'crafts','media_section'=>'banner'))->row();
        $data['crafts'] = $this->Access->readtable('crafts')->result();

        $data['current'] = "crafts";
        $view['content'] = $this->load->view('backend/v_crafts',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }# func index

    public function save_banner(){
        $id = $this->input->post('banner_id');

        if(!empty($_FILES['banner']['name'])){
	        $gettoId = $this->db->query("SELECT * FROM media WHERE media_page = 'crafts' AND media_section = 'banner' AND media_id = '".$id."' ")->row()->media_url;

	        $media_url  =   $_FILES['banner']['name'];
	        $break      =   explode('.', $media_url);
	        $ext        =   strtolower($break[count($break) - 1]);
	        $date       =   date('dmYHis');
	        $media_url  =   'banner_'.$date.'.'.$ext;
	        $path       =   './assets/upload/crafts';

	        if( ! file_exists( $path ) ){
	            $create = mkdir($path, 0777, TRUE);
	            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
	            if( ! $create || ! $createTemp )
	                return;
	        }

	        $this->piclib->get_config($media_url, $path);
	        if( $this->upload->do_upload('banner') ){
	            $image = array('upload_data' => $this->upload->data());
	            $source_path = $image['upload_data']['full_path'];
	            $width = $image['upload_data']['image_width'];
	            $height = $image['upload_data']['image_height'];

	            if( $width < 1024 || $height < 275 ){
	                unlink( realpath( APPPATH.'../assets/upload/crafts/'.$media_url ));
	                unlink( realpath( APPPATH.'../assets/upload/crafts/thumbnail/'.$media_url ));
	                $image_1024px = "Minimum image size 1024 x 275 px or bigger in the same ratio";
	                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_1024px.'</div>';
	            }else{
	                $orientation = $this->piclib->orientation($source_path);
	                if( $orientation == 'portrait' || $orientation == 'square'){
	                    unlink( realpath( APPPATH.'../assets/upload/crafts/'.$media_url ));
	                    unlink( realpath( APPPATH.'../assets/upload/crafts/thumbnail/'.$media_url ));
	                    $lands_square = "Image orientation must be 'landscape'";
	                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';
	                }else{
	                    $this->piclib->resize_image($source_path, $width, $height, 1024, 275);
	                    if( $this->image_lib->resize() ){
	                        $this->image_lib->clear();
	                        $this->piclib->resize_image($source_path, $width, $height, 1024, 275, $path.'/thumbnail');
	                        $this->image_lib->resize();

	                        unlink( realpath( APPPATH.'../assets/upload/crafts/'.$gettoId ));
	                        unlink( realpath(APPPATH.'../assets/upload/crafts/thumbnail/'.$gettoId ));

	                        $media_url = array(
	                            'media_url'=> $media_url
	                         );

	                        $this->db->trans_begin();
	                        $this->db->set('media_date', 'NOW()', FALSE);
	                        $this->db->where('media_id', $id)->update('media',$media_url);
	                        $this->db->trans_complete();

	                        if ($this->db->trans_status() === FALSE){
	                            $this->db->trans_rollback();
	                            $updd = "Updated failed!";
	                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	                        }else{
	                            $updd = "Updated successfully!";
	                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	                        }
	                        #### COMPRESS IMAGE FILE SIZE ####
	                        function compress_image($source_url, $destination_url, $quality) {
								$info = getimagesize($source_url);

								if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
								elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
								elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

								//save file
								imagejpeg($image, $destination_url, $quality);

								//return destination file
								return $destination_url;
							}

							//usage
							$src = 'assets/upload/crafts'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;

							$dest = 'assets/upload/crafts'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
							$compressed = compress_image($src, $dest, 80);
	                    }
	                }
	            }
	        }else{
	            $error = $this->upload->display_errors();
	            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';
	        }
	    }

        $this->session->set_flashdata('notif_banner', $notif);
        redirect('backend/crafts#banner');

    }# func save carousel

    public function save_craft(){
    	$name = $this->input->post('crafts_name');
    	$spec_en = $this->input->post('crafts_spec_en');
    	$spec_fr = $this->input->post('crafts_spec_fr');
    	$id = $this->input->post('crafts_id');

    	if($id == ""){
    		#INSERT
    		if(!empty($_FILES['crafts_image']['name'])){
		        if(!empty($id)){$gettoId = $this->db->query("SELECT * FROM crafts WHERE crafts_id='".$id."'")->row()->crafts_image;}

		        $media_url  =   $_FILES['crafts_image']['name'];
		        $break      =   explode('.', $media_url);
		        $ext        =   strtolower($break[count($break) - 1]);
		        $date       =   date('dmYHis');
		        $media_url  =   'craft_'.$date.'.'.$ext;
		        $path       =   './assets/upload/crafts';

		        if( ! file_exists( $path ) ){
		            $create = mkdir($path, 0777, TRUE);
		            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
		            if( ! $create || ! $createTemp )
		                return;
		        }

		        $this->piclib->get_config($media_url, $path);
		        if( $this->upload->do_upload('crafts_image') ){
		            $image = array('upload_data' => $this->upload->data());
		            $source_path = $image['upload_data']['full_path'];
		            $width = $image['upload_data']['image_width'];
		            $height = $image['upload_data']['image_height'];

		            if( $width < 500 || $height < 500 ){
		                unlink( realpath( APPPATH.'../assets/upload/crafts/'.$media_url ));
		                unlink( realpath( APPPATH.'../assets/upload/crafts/thumbnail/'.$media_url ));
		                $image_res = "Minimum image size 500 x 500 px or bigger in the same ratio";
		                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_res.'</div>';
		            }else{
		                $orientation = $this->piclib->orientation($source_path);
		                if( $orientation == 'portrait' || $orientation == 'landscape'){
		                    unlink( realpath( APPPATH.'../assets/upload/crafts/'.$media_url ));
		                    unlink( realpath( APPPATH.'../assets/upload/crafts/thumbnail/'.$media_url ));
		                    $lands_square = "Image orientation must be 'square'";
		                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';
		                }else{
		                    $this->piclib->resize_image($source_path, $width, $height, 500, 500);
		                    if( $this->image_lib->resize() ){
		                        $this->image_lib->clear();
		                        $this->piclib->resize_image($source_path, $width, $height, 500, 500, $path.'/thumbnail');
		                        $this->image_lib->resize();

		                        if(!empty($id)){
			                        unlink( realpath( APPPATH.'../assets/upload/crafts/'.$gettoId ));
			                        unlink( realpath(APPPATH.'../assets/upload/crafts/thumbnail/'.$gettoId ));
			                    }

		                        $save = array(
		                        	'crafts_name'=> $name,
		                        	'crafts_spec_en'=> $spec_en,
		                        	'crafts_spec_fr'=> $spec_fr,
		                            'crafts_image'=> $media_url,
		                            'crafts_date'=>date('Y-m-d H:i:s')
		                         );


		                        $this->db->trans_begin();
		                        $this->Access->inserttable('crafts',$save);
		                        $this->db->trans_complete();

		                        if ($this->db->trans_status() === FALSE){
		                            $this->db->trans_rollback();
		                            $updd = "Updated failed!";
		                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
		                        }else{
		                            $updd = "Updated successfully!";
		                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
		                        }

		                        #### COMPRESS IMAGE FILE SIZE ####
		                        function compress_image($source_url, $destination_url, $quality) {
									$info = getimagesize($source_url);

									if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
									elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
									elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

									//save file
									imagejpeg($image, $destination_url, $quality);

									//return destination file
									return $destination_url;
								}

								//usage
								$src = 'assets/upload/crafts'.'/'.$this->db->get_where('crafts',array('crafts_id'=>$id))->row()->crafts_image;
								
								$dest = 'assets/upload/crafts'.'/'.$this->db->get_where('crafts',array('crafts_id'=>$id))->row()->crafts_image;
								$compressed = compress_image($src, $dest, 80);
			                    }
		                }
		            }
		        }else{
		            $error = $this->upload->display_errors();
		            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';
		        }
		    }else{
		    	$save = array(
	            	'crafts_name'=> $name,
	            	'crafts_spec_en'=> $spec_en,
	            	'crafts_spec_fr'=> $spec_fr,
	                'crafts_date'=>date('Y-m-d H:i:s')
	             );


	            $this->db->trans_begin();
	            $this->Access->inserttable('crafts',$save);
	            $this->db->trans_complete();

	            if ($this->db->trans_status() === FALSE){
	                $this->db->trans_rollback();
	                $updd = "Updated failed!";
	                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	            }else{
	                $updd = "Updated successfully!";
	                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	            }
		    }
    	}else{
    		#UPDATE
    		if(!empty($_FILES['crafts_image']['name'])){
		        $gettoId = $this->db->query("SELECT * FROM crafts WHERE crafts_id='".$id."'")->row()->crafts_image;

		        $media_url  =   $_FILES['crafts_image']['name'];
		        $break      =   explode('.', $media_url);
		        $ext        =   strtolower($break[count($break) - 1]);
		        $date       =   date('dmYHis');
		        $media_url  =   'craft_'.$date.'.'.$ext;
		        $path       =   './assets/upload/crafts';

		        if( ! file_exists( $path ) ){
		            $create = mkdir($path, 0777, TRUE);
		            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
		            if( ! $create || ! $createTemp )
		                return;
		        }

		        $this->piclib->get_config($media_url, $path);
		        if( $this->upload->do_upload('crafts_image') ){
		            $image = array('upload_data' => $this->upload->data());
		            $source_path = $image['upload_data']['full_path'];
		            $width = $image['upload_data']['image_width'];
		            $height = $image['upload_data']['image_height'];

		            if( $width < 500 || $height < 500 ){
		                unlink( realpath( APPPATH.'../assets/upload/crafts/'.$media_url ));
		                unlink( realpath( APPPATH.'../assets/upload/crafts/thumbnail/'.$media_url ));
		                $image_res = "Minimum image size 500 x 500 px or bigger in the same ratio";
		                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_res.'</div>';
		            }else{
		                $orientation = $this->piclib->orientation($source_path);
		                if( $orientation == 'portrait' || $orientation == 'landscape'){
		                    unlink( realpath( APPPATH.'../assets/upload/crafts/'.$media_url ));
		                    unlink( realpath( APPPATH.'../assets/upload/crafts/thumbnail/'.$media_url ));
		                    $lands_square = "Image orientation must be 'square'";
		                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';
		                }else{
		                    $this->piclib->resize_image($source_path, $width, $height, 500, 500);
		                    if( $this->image_lib->resize() ){
		                        $this->image_lib->clear();
		                        $this->piclib->resize_image($source_path, $width, $height, 500, 500, $path.'/thumbnail');
		                        $this->image_lib->resize();

		                        unlink( realpath( APPPATH.'../assets/upload/crafts/'.$gettoId ));
		                        unlink( realpath(APPPATH.'../assets/upload/crafts/thumbnail/'.$gettoId ));

		                        $save = array(
		                        	'crafts_name'=> $name,
		                        	'crafts_spec_en'=> $spec_en,
	            					'crafts_spec_fr'=> $spec_fr,
		                            'crafts_image'=> $media_url,
		                            'crafts_date'=>date('Y-m-d H:i:s')
		                         );


		                        $this->db->trans_begin();
		                        $this->Access->updatetable('crafts',$save,array('crafts_id'=>$id));
		                        $this->db->trans_complete();

		                        if ($this->db->trans_status() === FALSE){
		                            $this->db->trans_rollback();
		                            $updd = "Updated failed!";
		                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
		                        }else{
		                            $updd = "Updated successfully!";
		                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
		                        }
		                    }
		                }
		            }
		        }else{
		            $error = $this->upload->display_errors();
		            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';
		        }
		    }else{
		    	$save = array(
	            	'crafts_name'=> $name,
	            	'crafts_spec_en'=> $spec_en,
	            	'crafts_spec_fr'=> $spec_fr,
	                'crafts_date'=>date('Y-m-d H:i:s')
	             );


	            $this->db->trans_begin();
	            $this->Access->updatetable('crafts',$save,array('crafts_id'=>$id));
	            $this->db->trans_complete();

	            if ($this->db->trans_status() === FALSE){
	                $this->db->trans_rollback();
	                $updd = "Updated failed!";
	                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	            }else{
	                $updd = "Updated successfully!";
	                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	            }
		    }
    	}


	    $this->session->set_flashdata('notif_crafts', $notif);
        redirect('backend/crafts#crafts');

    }# func save craft

    public function delete_craft($id){
        $gettoId = $this->db->query("SELECT * FROM crafts WHERE crafts_id='".$id."'")->row()->media_url;
        $this->db->trans_begin();
        $this->Access->deletetable("crafts",array("crafts_id"=>$id));
        $this->db->trans_complete();

        unlink( realpath( APPPATH.'../assets/upload/crafts/'.$gettoId ));
        unlink( realpath(APPPATH.'../assets/upload/crafts/thumbnail/'.$gettoId ));
        redirect('backend/crafts#crafts');
    }
}
