<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Women extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('rootscabana_backend',$be_lang);
        } else {
            $this->lang->load('rootscabana_backend','french');
        }

    }

    public function index()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }

        $data['lang'] = $this->session->userdata('be_lang');
        $data['banner'] = $this->Access->readtable('media','',array('media_page'=>'women','media_section'=>'banner'))->row();
        $data['category'] = $this->db->query("SELECT * FROM category WHERE category_name LIKE '%women%'")->result();

        $data['products'] = $this->db->query("SELECT * FROM product WHERE product_category LIKE '%women%' ORDER BY product_category ASC")->result();

        $data['current'] = "women";
        $view['content'] = $this->load->view('backend/v_women',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }# func index

    public function save_banner(){
        $id = $this->input->post('banner_id');

        if(!empty($_FILES['banner']['name'])){
	        $gettoId = $this->db->query("SELECT * FROM media WHERE media_page = 'women' AND media_section = 'banner' AND media_id = '".$id."' ")->row()->media_url;

	        $media_url  =   $_FILES['banner']['name'];
	        $break      =   explode('.', $media_url);
	        $ext        =   strtolower($break[count($break) - 1]);
	        $date       =   date('dmYHis');
	        $media_url  =   'banner_'.$date.'.'.$ext;
	        $path       =   './assets/upload/women';

	        if( ! file_exists( $path ) ){
	            $create = mkdir($path, 0777, TRUE);
	            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
	            if( ! $create || ! $createTemp )
	                return;
	        }

	        $this->piclib->get_config($media_url, $path);
	        if( $this->upload->do_upload('banner') ){
	            $image = array('upload_data' => $this->upload->data());
	            $source_path = $image['upload_data']['full_path'];
	            $width = $image['upload_data']['image_width'];
	            $height = $image['upload_data']['image_height'];

	            if( $width < 1024 || $height < 275 ){
	                unlink( realpath( APPPATH.'../assets/upload/women/'.$media_url ));
	                unlink( realpath( APPPATH.'../assets/upload/women/thumbnail/'.$media_url ));
	                $image_1024px = "Minimum image size 1024 x 275 px or bigger in the same ratio";
	                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_1024px.'</div>';
	            }else{
	                $orientation = $this->piclib->orientation($source_path);
	                if( $orientation == 'portrait' || $orientation == 'square'){
	                    unlink( realpath( APPPATH.'../assets/upload/women/'.$media_url ));
	                    unlink( realpath( APPPATH.'../assets/upload/women/thumbnail/'.$media_url ));
	                    $lands_square = "Image orientation must be 'landscape'";
	                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';
	                }else{
	                    $this->piclib->resize_image($source_path, $width, $height, 1024, 275);
	                    if( $this->image_lib->resize() ){
	                        $this->image_lib->clear();
	                        $this->piclib->resize_image($source_path, $width, $height, 1024, 275, $path.'/thumbnail');
	                        $this->image_lib->resize();

	                        unlink( realpath( APPPATH.'../assets/upload/women/'.$gettoId ));
	                        unlink( realpath(APPPATH.'../assets/upload/women/thumbnail/'.$gettoId ));

	                        $media_url = array(
	                            'media_url'=> $media_url
	                         );

	                        $this->db->trans_begin();
	                        $this->db->set('media_date', 'NOW()', FALSE);
	                        $this->db->where('media_id', $id)->update('media',$media_url);
	                        $this->db->trans_complete();

	                        if ($this->db->trans_status() === FALSE){
	                            $this->db->trans_rollback();
	                            $updd = "Updated failed!";
	                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	                        }else{
	                            $updd = "Updated successfully!";
	                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	                        }

	                        #### COMPRESS IMAGE FILE SIZE ####
	                        function compress_image($source_url, $destination_url, $quality) {
								$info = getimagesize($source_url);

								if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
								elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
								elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

								//save file
								imagejpeg($image, $destination_url, $quality);

								//return destination file
								return $destination_url;
							}

							//usage
							$src = 'assets/upload/women'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
							
							$dest = 'assets/upload/women'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
							$compressed = compress_image($src, $dest, 90);

	                    }
	                }
	            }
	        }else{
	            $error = $this->upload->display_errors();
	            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';
	        }
	    }

        $this->session->set_flashdata('notif_banner', $notif);
        redirect('backend/women#banner');

    }# func save banner

    public function save_category(){
    	$id = $this->input->post('category_id');
    	$display = $this->input->post('product_display');
    	$category_name = $this->db->query("SELECT * FROM category WHERE category_id='".$id."'")->row()->category_name;

    	if(!empty($display)){
    		$save_display = array(
        		'product_display'=>'true',
	        );

	    	foreach($display as $key=>$val){
	        	$this->Access->updatetable('product',$save_display,array('product_id'=>$val));
	        }

	        $save_display_false = array(
        		'product_display'=>'false',
	        );

	        foreach($display as $key=>$val){
	        	$this->db->where('product_id !=', $val);
	        }
	        $this->db->where('product_category', $category_name);
	        $this->db->update('product',$save_display_false);
    	}else{

    		$save_display_false = array(
        		'product_display'=>'false',
	        );

	        $this->db->where('product_category', $category_name);
	        $this->db->update('product',$save_display_false);
    	}

    	$updd = "Updated successfully!";
	    $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';

		if(!empty($_FILES['category_image_sq']['name'])){
	        $gettoId = $this->db->query("SELECT * FROM category WHERE category_id='".$id."'")->row()->category_image_sq;

	        $media_url  =   $_FILES['category_image_sq']['name'];
	        $break      =   explode('.', $media_url);
	        $ext        =   strtolower($break[count($break) - 1]);
	        $date       =   date('dmYHis');
	        $media_url  =   'imgsq_'.$date.'.'.$ext;
	        $path       =   './assets/upload/category';

	        if( ! file_exists( $path ) ){
	            $create = mkdir($path, 0777, TRUE);
	            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
	            if( ! $create || ! $createTemp )
	                return;
	        }

	        $this->piclib->get_config($media_url, $path);
	        if( $this->upload->do_upload('category_image_sq') ){
	            $image = array('upload_data' => $this->upload->data());
	            $source_path = $image['upload_data']['full_path'];
	            $width = $image['upload_data']['image_width'];
	            $height = $image['upload_data']['image_height'];

	            if( $width < 500 || $height < 500 ){
	                unlink( realpath( APPPATH.'../assets/upload/category/'.$media_url ));
	                unlink( realpath( APPPATH.'../assets/upload/category/thumbnail/'.$media_url ));
	                $image_res = "Minimum image size 500 x 500 px or bigger in the same ratio";
	                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_res.'</div>';
	            }else{
	                $orientation = $this->piclib->orientation($source_path);
	                if( $orientation == 'landscape' || $orientation == 'portrait'){
	                    unlink( realpath( APPPATH.'../assets/upload/category/'.$media_url ));
	                    unlink( realpath( APPPATH.'../assets/upload/category/thumbnail/'.$media_url ));
	                    $lands_square = "Image orientation must be 'square'";
	                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';
	                }else{
	                    $this->piclib->resize_image($source_path, $width, $height, 500, 500);
	                    if( $this->image_lib->resize() ){
	                        $this->image_lib->clear();
	                        $this->piclib->resize_image($source_path, $width, $height, 500, 500, $path.'/thumbnail');
	                        $this->image_lib->resize();

	                        unlink( realpath( APPPATH.'../assets/upload/category/'.$gettoId ));
	                        unlink( realpath(APPPATH.'../assets/upload/category/thumbnail/'.$gettoId ));

	                        $save = array(
	                            'category_image_sq'=> $media_url,
	                        );

	                        $this->db->trans_begin();
	                        $this->Access->updatetable('category',$save,array('category_id'=>$id));
	                        $this->db->trans_complete();

	                        if ($this->db->trans_status() === FALSE){
	                            $this->db->trans_rollback();
	                            $updd = "Updated failed!";
	                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	                        }else{
	                            $updd = "Updated successfully!";
	                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	                        }

	                        #### COMPRESS IMAGE FILE SIZE ####
	                        function compress_image($source_url, $destination_url, $quality) {
								$info = getimagesize($source_url);

								if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
								elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
								elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

								//save file
								imagejpeg($image, $destination_url, $quality);

								//return destination file
								return $destination_url;
							}

							//usage
							$src = 'assets/upload/category'.'/'.$this->db->get_where('category',array('category_id'=>$id))->row()->category_image_sq;
							$dest = 'assets/upload/category'.'/'.$this->db->get_where('category',array('category_id'=>$id))->row()->category_image_sq;

							$compressed = compress_image($src, $dest, 90);
	                    }
	                }
	            }
	        }else{
	            $error = $this->upload->display_errors();
	            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';
	        }
	    }

	    $this->session->set_flashdata('notif_category', $notif);
        redirect('backend/women#category');
    }
}
