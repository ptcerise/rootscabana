<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Subscribe extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Access');
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('rootscabana_backend',$be_lang);
        } else {
            $this->lang->load('rootscabana_backend','french');
        }
	}

	function index()
	{	       
		if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }
        
        $data['lang'] = $this->session->userdata('be_lang');
        $data['subscriber'] = $this->Access->readtable("subscribe")->result();

        $data['current'] = "subscribe";
        $view['content'] = $this->load->view('backend/v_subscribe',$data,TRUE);
        $this->load->view('backend/v_master',$view);
	}

	public function register(){
		$email = $this->input->post("subscribe");
		$save = array(
			"subs_email"=>$email,
			"subs_date"=>date("Y-m-d H:i:s")
			);
		
		$check = $this->Access->readtable("subscribe","",array("subs_email"=>$email))->num_rows();

		if($check == 0){
			$this->db->trans_begin();
	        $this->Access->inserttable('subscribe',$save);
	        $this->db->trans_complete();
		}else{
			$this->db->trans_begin();
	        $this->Access->updatetable('subscribe',$save,array('subs_email'=>$email));
	        $this->db->trans_complete();
		}

        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $updd = "Oops sorry, there's some error. Try again";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }else{
            $updd = "Thanks for subscribing!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }
        $this->session->set_flashdata(array("subscribtion"=>$notif));
        redirect($_SERVER['HTTP_REFERER']."#subscribe");
	}

	public function delete($id){
		$this->db->trans_begin();
		$this->Access->deletetable("subscribe",array("subs_id"=>$id));
		$this->db->trans_complete();
		redirect($_SERVER['HTTP_REFERER']);
	}

}