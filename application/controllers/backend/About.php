<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('rootscabana_backend',$be_lang);
        } else {
            $this->lang->load('rootscabana_backend','french');
        }
    }
    
    public function index()
    {
        redirect('backend/about/handmade');
    }# func index

    public function handmade()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }
        
        $data['lang'] = $this->session->userdata('be_lang');
        $data['banner'] = $this->Access->readtable('media','',array('media_page'=>'about-handmade','media_section'=>'banner'))->row();
        $data['contents'] = $this->Access->readtable('media','',array('media_page'=>'about-handmade','media_section'=>'content'))->result();

        $data['current'] = "about-handmade";
        $data['submenu'] = "about-handmade";
        $view['content'] = $this->load->view('backend/v_about',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    public function brand()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }
        
        $data['lang'] = $this->session->userdata('be_lang');
        $data['banner'] = $this->Access->readtable('media','',array('media_page'=>'about-brand','media_section'=>'banner'))->row();
        $data['contents'] = $this->Access->readtable('media','',array('media_page'=>'about-brand','media_section'=>'content'))->result();

        $data['current'] = "about-brand";
        $data['submenu'] = "about-brand";
        $view['content'] = $this->load->view('backend/v_about',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    public function shop()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }
        
        $data['lang'] = $this->session->userdata('be_lang');
        $data['banner'] = $this->Access->readtable('media','',array('media_page'=>'about-shop','media_section'=>'banner'))->row();
        $data['contents'] = $this->Access->readtable('media','',array('media_page'=>'about-shop','media_section'=>'content'))->result();

        $data['current'] = "about-shop";
        $data['submenu'] = "about-shop";
        $view['content'] = $this->load->view('backend/v_about',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }

    public function save_banner(){
        $id = $this->input->post('banner_id');
        $submenu = $this->input->post('banner_submenu');

        if(!empty($_FILES['banner']['name'])){
	        $gettoId = $this->db->query("SELECT * FROM media WHERE media_id = '".$id."' ")->row()->media_url;

	        $media_url  =   $_FILES['banner']['name'];
	        $break      =   explode('.', $media_url);
	        $ext        =   strtolower($break[count($break) - 1]);
	        $date       =   date('dmYHis');
	        $media_url  =   'banner_'.$date.'.'.$ext;
	        $path       =   './assets/upload/about';

	        if( ! file_exists( $path ) ){
	            $create = mkdir($path, 0777, TRUE);
	            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
	            if( ! $create || ! $createTemp )
	                return;
	        }
	        
	        $this->piclib->get_config($media_url, $path);
	        if( $this->upload->do_upload('banner') ){
	            $image = array('upload_data' => $this->upload->data());
	            $source_path = $image['upload_data']['full_path'];
	            $width = $image['upload_data']['image_width'];
	            $height = $image['upload_data']['image_height'];
	            
	            if( $width < 1024 || $height < 275 ){
	                unlink( realpath( APPPATH.'../assets/upload/about/'.$media_url ));
	                unlink( realpath( APPPATH.'../assets/upload/about/thumbnail/'.$media_url ));
	                $image_1024px = "Minimum image size 1024 x 275 px or bigger in the same ratio";
	                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_1024px.'</div>';
	            }else{
	                $orientation = $this->piclib->orientation($source_path);
	                if( $orientation == 'portrait' || $orientation == 'square'){
	                    unlink( realpath( APPPATH.'../assets/upload/about/'.$media_url ));
	                    unlink( realpath( APPPATH.'../assets/upload/about/thumbnail/'.$media_url ));
	                    $lands_square = "Image orientation must be 'landscape'";
	                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';     
	                }else{
	                    $this->piclib->resize_image($source_path, $width, $height, 1024, 275);
	                    if( $this->image_lib->resize() ){
	                        $this->image_lib->clear();
	                        $this->piclib->resize_image($source_path, $width, $height, 1024, 275, $path.'/thumbnail');
	                        $this->image_lib->resize();
	                        
	                        unlink( realpath( APPPATH.'../assets/upload/about/'.$gettoId ));
	                        unlink( realpath(APPPATH.'../assets/upload/about/thumbnail/'.$gettoId ));

	                        $media_url = array(
	                            'media_url'=> $media_url
	                         );

	                        $this->db->trans_begin();
	                        $this->db->set('media_date', 'NOW()', FALSE);
	                        $this->db->where('media_id', $id)->update('media',$media_url); 
	                        $this->db->trans_complete();

	                        if ($this->db->trans_status() === FALSE){
	                            $this->db->trans_rollback();
	                            $updd = "Updated failed!";
	                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	                        }else{
	                            $updd = "Updated successfully!";
	                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	                        }

	                        #### COMPRESS IMAGE FILE SIZE ####
	                        function compress_image($source_url, $destination_url, $quality) {
								$info = getimagesize($source_url);
							 
								if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
								elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
								elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
							 
								//save file
								imagejpeg($image, $destination_url, $quality);
							 
								//return destination file
								return $destination_url;
							}
			 
							//usage
							$src = 'assets/upload/about'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
							
							$dest = 'assets/upload/about'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
							$compressed = compress_image($src, $dest, 80);
	                    }
	                }
	            }
	        }else{
	            $error = $this->upload->display_errors();
	            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';  
	        }
	    }

        $this->session->set_flashdata('notif_banner', $notif);
        $prev = explode('-',$submenu);
        redirect('backend/about/'.$prev[1].'#banner');

    }# func save carousel


    public function save_content(){
        $title_fr = $this->input->post('title_fr');
        $title_en = $this->input->post('title_en');
    	$content_en = $this->input->post('content_en');
    	$content_fr = $this->input->post('content_fr');
    	$id = $this->input->post('content_id');
    	$submenu = $this->input->post('content_submenu');

        if($content_en == "" && $content_fr == ""){
            $updd = "Some fields cannot be empty!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
            $this->session->set_flashdata('notif_content', $notif);
            redirect('backend/about#content');
        }

    	if(!empty($_FILES['content_img']['name'])){
    	    if(!empty($id)){
                $gettoId = $this->db->query("SELECT * FROM media WHERE media_id='".$id."'")->row()->media_url;
            }

	        $media_url  =   $_FILES['content_img']['name'];
	        $break      =   explode('.', $media_url);
	        $ext        =   strtolower($break[count($break) - 1]);
	        $date       =   date('dmYHis');
	        $media_url  =   'content_'.$date.'.'.$ext;
	        $path       =   './assets/upload/about';

	        if( ! file_exists( $path ) ){
	            $create = mkdir($path, 0777, TRUE);
	            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
	            if( ! $create || ! $createTemp )
	                return;
	        }
	        
	        $this->piclib->get_config($media_url, $path);
	        if( $this->upload->do_upload('content_img') ){
	            $image = array('upload_data' => $this->upload->data());
	            $source_path = $image['upload_data']['full_path'];
	            $width = $image['upload_data']['image_width'];
	            $height = $image['upload_data']['image_height'];
	            
	            if( $width < 450 || $height < 300 ){
	            	if(!empty($id)){
		                unlink( realpath( APPPATH.'../assets/upload/about/'.$media_url ));
		                unlink( realpath( APPPATH.'../assets/upload/about/thumbnail/'.$media_url ));
		            }
	                $image_res = "Minimum image size 500 x 500 px or bigger in the same ratio";
	                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_res.'</div>';
	            }else{
	                    $this->piclib->resize_image($source_path, $width, $height, 500, 500);
	                    if( $this->image_lib->resize() ){
	                        $this->image_lib->clear();
	                        $this->piclib->resize_image($source_path, $width, $height, 500, 500, $path.'/thumbnail');
	                        $this->image_lib->resize();

                            if(!empty($id)){
                                unlink( realpath( APPPATH.'../assets/upload/about/'.$gettoId ));
                                unlink( realpath(APPPATH.'../assets/upload/about/thumbnail/'.$gettoId ));
                            }

	                        $save_content = array(
	                            'media_title_fr'=>$title_fr,
                                'media_title_en'=>$title_en,
	                        	'media_content_en'=>$content_en,
	                        	'media_content_fr'=>$content_fr,
	                            'media_url'=> $media_url,
                                'media_page'=>$submenu,
                                'media_section'=>'content',
	                            'media_date'=> date('Y-m-d H:i:s')
	                         );

	                        $this->db->trans_begin();
                            if(!empty($id)){
                                $this->Access->updatetable('media',$save_content,array('media_id'=>$id));
                            }else{
                                $this->Access->inserttable('media',$save_content);
                            }
	                        $this->db->trans_complete();

	                        if ($this->db->trans_status() === FALSE){
	                            $this->db->trans_rollback();
	                            $updd = "Updated failed!";
	                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	                        }else{
	                            $updd = "Updated successfully!";
	                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	                        }

	                        #### COMPRESS IMAGE FILE SIZE ####
	                        function compress_image($source_url, $destination_url, $quality) {
								$info = getimagesize($source_url);
							 
								if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
								elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
								elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);
							 
								//save file
								imagejpeg($image, $destination_url, $quality);
							 
								//return destination file
								return $destination_url;
							}
			 
							//usage
							$id = $this->db->query("SELECT * FROM media WHERE media_page='$submenu' ORDER BY media_date DESC")->row()->media_id;
							$src = 'assets/upload/about'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
							
							$dest = 'assets/upload/about'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
							$compressed = compress_image($src, $dest, 80);
	                    }
	                
	            }
	        }else{
	            $error = $this->upload->display_errors();
	            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';  
	        }
	    }else{
	    	$save_content = array(
                'media_title_fr'=>$title_fr,
                'media_title_en'=>$title_en,
	    		'media_content_en'=>$content_en,
	            'media_content_fr'=>$content_fr,
                'media_page'=>$submenu,
                'media_section'=>'content',
	            'media_date'=> date('Y-m-d H:i:s')
	    		);
	    	$this->db->trans_begin();
            if(!empty($id)){
                $this->Access->updatetable('media',$save_content,array('media_id'=>$id));
            }else{
                $this->Access->inserttable('media',$save_content);
            }
            $this->db->trans_complete();

            if ($this->db->trans_status() === FALSE){
                $this->db->trans_rollback();
                $updd = "Updated failed!";
                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
            }else{
                $updd = "Updated successfully!";
                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
            }
	    }

	    $this->session->set_flashdata('notif_content', $notif);
        $prev = explode('-',$submenu);
        redirect('backend/about/'.$prev[1].'#content');

    }# func save content

    public function getdata_content(){
        $id = $this->input->post('id');

        $q = $this->db->get_where('media',array('media_id'=>$id))->row();

        $data = array(
            'media_id'=>$q->media_id,
            'media_title_fr'=>$q->media_title_fr,
            'media_title_en'=>$q->media_title_en,
            'media_content_fr'=>$q->media_content_fr,
            'media_content_en'=>$q->media_content_en,
            'media_url'=>$q->media_url
        );

        echo json_encode($data);
    }

    public function delete_content($id, $submenu){
        $this->db->trans_begin();
        $img = $this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
        unlink( realpath( APPPATH.'../assets/upload/about/'.$img ));
        unlink( realpath( APPPATH.'../assets/upload/about/thumbnail/'.$img ));

        $this->db->delete('media',array('media_id'=>$id));
        $this->db->trans_complete();
        if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
        }

        $prev = explode('-',$submenu);
        redirect('backend/about/'.$prev[1].'#banner');
    }

}
