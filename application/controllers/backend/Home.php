<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Access');
        $be_lang = $this->session->userdata('be_lang');
        if ($be_lang) {
            $this->lang->load('rootscabana_backend',$be_lang);
        } else {
            $this->lang->load('rootscabana_backend','french');
        }
    }

    public function index()
    {
        if($this->session->userdata('status_login') == FALSE){
            redirect('backend/login');
        }

        $data['lang'] = $this->session->userdata('be_lang');
        $data['notif'] = $this->Access->readtable('general','',array('general_page'=>'all','general_section'=>'notif'))->row();

        $data['video'] = $this->Access->readtable('media','',array('media_page'=>'home', 'media_section'=>'video'))->row();

        $data['content1'] = $this->Access->readtable('general','',array('general_page'=>'home', 'general_section'=>'content1'))->row();
        $data['content2'] = $this->Access->readtable('general','',array('general_page'=>'home', 'general_section'=>'content2'))->row();
        $data['content3'] = $this->Access->readtable('general','',array('general_page'=>'home', 'general_section'=>'content3'))->row();

        $data['imagelink1'] = $this->Access->readtable('media','',array('media_page'=>'home', 'media_section'=>'imagelink1'))->row();

        $data['imagelink2'] = $this->Access->readtable('media','',array('media_page'=>'home', 'media_section'=>'imagelink2'))->row();

        $data['imagelink3'] = $this->Access->readtable('media','',array('media_page'=>'home', 'media_section'=>'imagelink3'))->row();

        $data['press'] = $this->Access->readtable('media','',array('media_page'=>'home', 'media_section'=>'press'))->result();

        $data['current'] = "home";
        $view['content'] = $this->load->view('backend/v_home',$data,TRUE);
        $this->load->view('backend/v_master',$view);
    }# func index

    public function save_video_banner(){
    	$configVideo['upload_path'] = 'assets/upload/video'; # check path is correct
		$configVideo['max_size'] = '10240';
		$configVideo['allowed_types'] = 'mp4|flv|avi'; # add video extenstion on here
		$configVideo['overwrite'] = FALSE;
		$configVideo['remove_spaces'] = TRUE;
		$video_name = 'video_'.date('dmYHis');
		$configVideo['file_name'] = $video_name;
		$ext = explode(".", $_FILES['banner']['name']);

		$this->load->library('upload', $configVideo);
		$this->upload->initialize($configVideo);

		if (!$this->upload->do_upload('banner')) # form input field attribute
		{
		    # Upload Failed
		    $this->session->set_flashdata('notif_banner', '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$this->upload->display_errors().'</div>');
		}
		else
		{
		    # Upload Successfull
		    $url = 'assets/upload/video/'.$video_name.'.'.$ext[1];

		    $data = array(
		    		'media_url'=>$video_name.'.'.$ext[1],
		    		'media_page'=>'home',
		    		'media_section'=>'video',
		    	);

		    if($this->input->post('banner_id')){
		    	# UPDATE
		    	$id = $this->input->post('banner_id');

		    	$del = $this->db->get_where('media',array('media_id'=>$id))->row();
		    	unlink( realpath( APPPATH.'../assets/upload/video/'.$del->media_url ));

		    	$this->db->trans_begin();
		    	$this->db->where('media_id', $id);
		    	$this->db->set('media_date','NOW()',FALSE);
		    	$this->db->update('media', $data);
		    	$this->db->trans_complete();

		    }else{
		    	# INSERT
		    	$this->db->trans_begin();
		    	$this->db->set('media_date','NOW()',FALSE);
		    	$this->db->insert('media',$data);
		    	$this->db->trans_complete();

		    }


		    $this->session->set_flashdata('notif_banner', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>Video Has been Uploaded !</div>');
		}

		redirect('backend/Home#banner');
    }# func save video banner

    public function save_content1(){
    	$title_fr = $this->input->post('content1_title_fr');
    	$title_en = $this->input->post('content1_title_en');
    	$content_fr = $this->input->post('content1_content_fr');
    	$content_en = $this->input->post('content1_content_en');
    	$id = $this->input->post('content1_id');

    	$data = array(
    			'general_title_fr'=>$title_fr,
    			'general_title_en'=>$title_en,
    			'general_content_fr'=>$content_fr,
    			'general_content_en'=>$content_en,
    		);
    	$this->db->trans_begin();
    	if(!empty($id)){
    		# UPDATE

	    	$this->db->where('general_id', $id);
	    	$this->db->set('general_date','NOW()',FALSE);
	    	$this->db->update('general', $data);

    	}else{
    		# INSERT

    		$this->db->set('general_page','home');
    		$this->db->set('general_section','content1');
	    	$this->db->set('general_date','NOW()',FALSE);
	    	$this->db->insert('general', $data);

    	}
    	$this->db->trans_complete();
    	if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $msg = 'Update data error!';
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$msg.'</div>';
        }else{
        	$msg = 'Data updated successfully!';
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$msg.'</div>';
        }

        $this->session->set_flashdata('notif_content1',$notif);
    	redirect('backend/Home#content1');
    }# func save content 1

    public function save_content2(){
    	$title_fr = $this->input->post('content2_title_fr');
    	$title_en = $this->input->post('content2_title_en');
    	$content_fr = $this->input->post('content2_content_fr');
    	$content_en = $this->input->post('content2_content_en');
    	$id = $this->input->post('content2_id');

    	$data = array(
    			'general_title_fr'=>$title_fr,
    			'general_title_en'=>$title_en,
    			'general_content_fr'=>$content_fr,
    			'general_content_en'=>$content_en,
    		);
    	$this->db->trans_begin();
    	if(!empty($id)){
    		# UPDATE

	    	$this->db->where('general_id', $id);
	    	$this->db->set('general_date','NOW()',FALSE);
	    	$this->db->update('general', $data);

    	}else{
    		# INSERT

    		$this->db->set('general_page','home');
    		$this->db->set('general_section','content2');
	    	$this->db->set('general_date','NOW()',FALSE);
	    	$this->db->insert('general', $data);

    	}
    	$this->db->trans_complete();
    	if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $msg = 'Update data error!';
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$msg.'</div>';
        }else{
        	$msg = 'Data updated successfully!';
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$msg.'</div>';
        }

        $this->session->set_flashdata('notif_content2',$notif);
    	redirect('backend/Home#content2');
    }# func save content 2

    public function save_content3(){
    	$title_fr = $this->input->post('content3_title_fr');
    	$title_en = $this->input->post('content3_title_en');
    	$content_fr = $this->input->post('content3_content_fr');
    	$content_en = $this->input->post('content3_content_en');
    	$id = $this->input->post('content3_id');

    	$data = array(
    			'general_title_fr'=>$title_fr,
    			'general_title_en'=>$title_en,
    			'general_content_fr'=>$content_fr,
    			'general_content_en'=>$content_en,
    		);
    	$this->db->trans_begin();
    	if(!empty($id)){
    		# UPDATE

	    	$this->db->where('general_id', $id);
	    	$this->db->set('general_date','NOW()',FALSE);
	    	$this->db->update('general', $data);

    	}else{
    		# INSERT

    		$this->db->set('general_page','home');
    		$this->db->set('general_section','content3');
	    	$this->db->set('general_date','NOW()',FALSE);
	    	$this->db->insert('general', $data);

    	}
    	$this->db->trans_complete();
    	if($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $msg = 'Update data error!';
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$msg.'</div>';
        }else{
        	$msg = 'Data updated successfully!';
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$msg.'</div>';
        }

        $this->session->set_flashdata('notif_content3',$notif);
    	redirect('backend/Home#content3');
    }# func save content 3

    public function save_imagelink(){
    	# check first image
    	if($_FILES['imagelink1']['name']){
    		$config['upload_path'] = 'assets/upload/home'; # check path is correct
			$config['max_size'] = '2048';
			$config['allowed_types'] = 'jpg|jpeg|png'; # add video extenstion on here
			$config['overwrite'] = FALSE;
			$config['remove_spaces'] = TRUE;
			$config['min_width'] = 1200;
	        $config['min_height'] = 1200;
	        $config['max_width'] = 2000;
	        $config['max_height'] = 2000;
			$filename = 'imagelink1_'.date('dmYHis');
			$config['file_name'] = $filename;
			$ext = explode(".", $_FILES['imagelink1']['name']);

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('imagelink1')) # form input field attribute
			{
			    # Upload Failed
			    $this->session->set_flashdata('notif_imagelink', '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$this->upload->display_errors().'</div>');
			}else{
				$url = 'assets/upload/home/'.$filename.'.'.$ext[1];

		    	$data = array(
		    		'media_url'=>$filename.'.'.$ext[1],
		    		'media_page'=>'home',
		    		'media_section'=>'imagelink1',
		    	);

				$id = $this->input->post('imagelink1_id');

				if(!empty($id)){
					# UPDATE

					$del = $this->db->get_where('media',array('media_id'=>$id))->row();
			    	unlink( realpath( APPPATH.'../assets/upload/home/'.$del->media_url ));

			    	$this->db->trans_begin();
			    	$this->db->where('media_id', $id);
			    	$this->db->set('media_date','NOW()',FALSE);
			    	$this->db->update('media', $data);
			    	$this->db->trans_complete();
				}else{
					# INSERT

					$this->db->trans_begin();
			    	$this->db->set('media_date','NOW()',FALSE);
			    	$this->db->insert('media',$data);
			    	$this->db->trans_complete();
				}

				function compress_image($source_url, $destination_url, $quality) {
					$info = getimagesize($source_url);

					if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
					elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
					elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

					//save file
					imagejpeg($image, $destination_url, $quality);

					//return destination file
					return $destination_url;
				}

				//usage
				$src = 'assets/upload/home'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;

				$dest = 'assets/upload/home'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
				$compressed = compress_image($src, $dest, 90);

				$this->session->set_flashdata('notif_imagelink', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>Image(s) Has been Uploaded !</div>');
			}

    	}# first image

    	# check second image
    	if($_FILES['imagelink2']['name']){
    		$config['upload_path'] = 'assets/upload/home'; # check path is correct
			$config['max_size'] = '2048';
			$config['allowed_types'] = 'jpg|jpeg|png'; # add video extenstion on here
			$config['overwrite'] = FALSE;
			$config['remove_spaces'] = TRUE;
			$config['min_width'] = 1200;
	        $config['min_height'] = 1200;
	        $config['max_width'] = 2000;
	        $config['max_height'] = 2000;
			$filename = 'imagelink2_'.date('dmYHis');
			$config['file_name'] = $filename;
			$ext = explode(".", $_FILES['imagelink2']['name']);

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('imagelink2')) # form input field attribute
			{
			    # Upload Failed
			    $this->session->set_flashdata('notif_imagelink', '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$this->upload->display_errors().'</div>');
			}else{
				$url = 'assets/upload/home/'.$filename.'.'.$ext[1];

		    	$data = array(
		    		'media_url'=>$filename.'.'.$ext[1],
		    		'media_page'=>'home',
		    		'media_section'=>'imagelink2',
		    	);

				$id = $this->input->post('imagelink2_id');

				if(!empty($id)){
					# UPDATE

					$del = $this->db->get_where('media',array('media_id'=>$id))->row();
			    	unlink( realpath( APPPATH.'../assets/upload/home/'.$del->media_url ));

			    	$this->db->trans_begin();
			    	$this->db->where('media_id', $id);
			    	$this->db->set('media_date','NOW()',FALSE);
			    	$this->db->update('media', $data);
			    	$this->db->trans_complete();
				}else{
					# INSERT

					$this->db->trans_begin();
			    	$this->db->set('media_date','NOW()',FALSE);
			    	$this->db->insert('media',$data);
			    	$this->db->trans_complete();
				}

				function compress_image($source_url, $destination_url, $quality) {
					$info = getimagesize($source_url);

					if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
					elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
					elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

					//save file
					imagejpeg($image, $destination_url, $quality);

					//return destination file
					return $destination_url;
				}

				//usage
				$src = 'assets/upload/home'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;

				$dest = 'assets/upload/home'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
				$compressed = compress_image($src, $dest, 90);

				$this->session->set_flashdata('notif_imagelink', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>Image(s) Has been Uploaded !</div>');
			}

    	}# second image

    	# check third image
    	if($_FILES['imagelink3']['name']){
    		$config['upload_path'] = 'assets/upload/home'; # check path is correct
			$config['max_size'] = '2048';
			$config['allowed_types'] = 'jpg|jpeg|png'; # add video extenstion on here
			$config['overwrite'] = FALSE;
			$config['remove_spaces'] = TRUE;
			$config['min_width'] = 1200;
	        $config['min_height'] = 600;
	        $config['max_width'] = 2000;
	        $config['max_height'] = 1000;
			$filename = 'imagelink3_'.date('dmYHis');
			$config['file_name'] = $filename;
			$ext = explode(".", $_FILES['imagelink3']['name']);

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if (!$this->upload->do_upload('imagelink3')) # form input field attribute
			{
			    # Upload Failed
			    $this->session->set_flashdata('notif_imagelink', '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$this->upload->display_errors().'</div>');
			}else{
				$url = 'assets/upload/home/'.$filename.'.'.$ext[1];

		    	$data = array(
		    		'media_url'=>$filename.'.'.$ext[1],
		    		'media_page'=>'home',
		    		'media_section'=>'imagelink3',
		    	);

				$id = $this->input->post('imagelink3_id');

				if(!empty($id)){
					# UPDATE

					$del = $this->db->get_where('media',array('media_id'=>$id))->row();
			    	unlink( realpath( APPPATH.'../assets/upload/home/'.$del->media_url ));

			    	$this->db->trans_begin();
			    	$this->db->where('media_id', $id);
			    	$this->db->set('media_date','NOW()',FALSE);
			    	$this->db->update('media', $data);
			    	$this->db->trans_complete();
				}else{
					# INSERT

					$this->db->trans_begin();
			    	$this->db->set('media_date','NOW()',FALSE);
			    	$this->db->insert('media',$data);
			    	$this->db->trans_complete();
				}

				function compress_image($source_url, $destination_url, $quality) {
					$info = getimagesize($source_url);

					if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
					elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
					elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

					//save file
					imagejpeg($image, $destination_url, $quality);

					//return destination file
					return $destination_url;
				}

				//usage
				$src = 'assets/upload/home'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;

				$dest = 'assets/upload/home'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
				$compressed = compress_image($src, $dest, 90);

				$this->session->set_flashdata('notif_imagelink', '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>Image(s) Has been Uploaded !</div>');
			}

    	}# third image

    	redirect('backend/Home#imagelink');
    }

    public function save_press(){
      $press_url = $this->input->post('press_url');
    	$id = $this->input->post('press_id');

    	if($id == ""){
    		#INSERT
    		if(!empty($_FILES['press_image']['name'])){
		        if(!empty($id)){$gettoId = $this->db->query("SELECT * FROM media WHERE media_id='".$id."'")->row()->media_url;}

		        $media_url  =   $_FILES['press_image']['name'];
		        $break      =   explode('.', $media_url);
		        $ext        =   strtolower($break[count($break) - 1]);
		        $date       =   date('dmYHis');
		        $media_url  =   'press_'.$date.'.'.$ext;
		        $path       =   './assets/upload/home/press';

		        if( ! file_exists( $path ) ){
		            $create = mkdir($path, 0777, TRUE);
		            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
		            if( ! $create || ! $createTemp )
		                return;
		        }

		        $this->piclib->get_config($media_url, $path);
		        if( $this->upload->do_upload('press_image') ){
		            $image = array('upload_data' => $this->upload->data());
		            $source_path = $image['upload_data']['full_path'];
		            $width = $image['upload_data']['image_width'];
		            $height = $image['upload_data']['image_height'];

		            if( $height < 40 ){
		                unlink( realpath( APPPATH.'../assets/upload/home/press/'.$media_url ));
		                unlink( realpath( APPPATH.'../assets/upload/home/press/thumbnail/'.$media_url ));
		                $image_res = "Minimum image size 60 x 40 px or bigger in the same ratio";
		                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_res.'</div>';
		            }else{
		                $orientation = $this->piclib->orientation($source_path);
		                if( $orientation == 'portrait' || $orientation == 'square'){
		                    unlink( realpath( APPPATH.'../assets/upload/home/press/'.$media_url ));
		                    unlink( realpath( APPPATH.'../assets/upload/home/press/thumbnail/'.$media_url ));
		                    $lands_square = "Image orientation must be 'landscape'";
		                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';
		                }else{
		                        if(!empty($id)){
			                        unlink( realpath( APPPATH.'../assets/upload/home/press/'.$gettoId ));
			                        unlink( realpath(APPPATH.'../assets/upload/home/press/thumbnail/'.$gettoId ));
			                      }

		                        $save = array(
		                        	'media_content_en'=> $press_url,
		                          'media_url'=> $media_url,
                              'media_page'=> 'home',
                              'media_section'=> 'press',
		                          'media_date'=>date('Y-m-d H:i:s')
		                         );


		                        $this->db->trans_begin();
		                        $this->Access->inserttable('media',$save);
		                        $this->db->trans_complete();

		                        if ($this->db->trans_status() === FALSE){
		                            $this->db->trans_rollback();
		                            $updd = "Update failed!";
		                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
		                        }else{
		                            $updd = "Updated successfully!";
		                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
		                        }

		                        #### COMPRESS IMAGE FILE SIZE ####
		                        function compress_image($source_url, $destination_url, $quality) {
            									$info = getimagesize($source_url);

            									if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
            									elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
            									elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

            									//save file
            									imagejpeg($image, $destination_url, $quality);

            									//return destination file
            									return $destination_url;
            								}

            								//usage
            								$src = 'assets/upload/home/press'.'/'.$media_url;
            								
            								$dest = 'assets/upload/home/press'.'/'.$media_url;
            								$compressed = compress_image($src, $dest, 80);
			                    }

		            }
		        }else{
		            $error = $this->upload->display_errors();
		            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';
		        }
		    }else{
		    	$save = array(
                'media_content_en'=> $press_url,
                'media_page'=> 'home',
                'media_section'=> 'press',
                'media_date'=>date('Y-m-d H:i:s')
	             );


	            $this->db->trans_begin();
	            $this->Access->inserttable('media',$save);
	            $this->db->trans_complete();

	            if ($this->db->trans_status() === FALSE){
	                $this->db->trans_rollback();
	                $updd = "Update failed!";
	                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	            }else{
	                $updd = "Updated successfully!";
	                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	            }
		    }
    	}else{
    		#UPDATE
    		if(!empty($_FILES['press_image']['name'])){
		        $gettoId = $this->db->query("SELECT * FROM media WHERE media_id='".$id."'")->row()->media_url;

		        $media_url  =   $_FILES['press_image']['name'];
		        $break      =   explode('.', $media_url);
		        $ext        =   strtolower($break[count($break) - 1]);
		        $date       =   date('dmYHis');
		        $media_url  =   'press_'.$date.'.'.$ext;
		        $path       =   './assets/upload/home/press';

		        if( ! file_exists( $path ) ){
		            $create = mkdir($path, 0777, TRUE);
		            $createTemp = mkdir($path.'/thumbnail', 0777, TRUE);
		            if( ! $create || ! $createTemp )
		                return;
		        }

		        $this->piclib->get_config($media_url, $path);
		        if( $this->upload->do_upload('press_image') ){
		            $image = array('upload_data' => $this->upload->data());
		            $source_path = $image['upload_data']['full_path'];
		            $width = $image['upload_data']['image_width'];
		            $height = $image['upload_data']['image_height'];

		            if( $height < 40 ){
		                unlink( realpath( APPPATH.'../assets/upload/home/press/'.$media_url ));
		                unlink( realpath( APPPATH.'../assets/upload/home/press/thumbnail/'.$media_url ));
		                $image_res = "Minimum image size 60 x 40 px or bigger in the same ratio";
		                $notif = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>'.$image_res.'</div>';
		            }else{
		                $orientation = $this->piclib->orientation($source_path);
		                if( $orientation == 'portrait' || $orientation == 'stats_cdf_noncentral_chisquare'){
		                    unlink( realpath( APPPATH.'../assets/upload/home/press/'.$media_url ));
		                    unlink( realpath( APPPATH.'../assets/upload/home/press/thumbnail/'.$media_url ));
		                    $lands_square = "Image orientation must be 'landscape'";
		                    $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true"> &times;</span><span class="sr-only">Close</span></button>'.$lands_square.'</div>';
		                }else{

		                        unlink( realpath( APPPATH.'../assets/upload/home/press/'.$gettoId ));
		                        unlink( realpath(APPPATH.'../assets/upload/home/press/thumbnail/'.$gettoId ));

		                        $save = array(
		                        	'media_content_en'=> $press_url,
		                          'media_url'=> $media_url,
		                          'media_date'=>date('Y-m-d H:i:s')
		                         );


		                        $this->db->trans_begin();
		                        $this->Access->updatetable('media',$save,array('media_id'=>$id));
		                        $this->db->trans_complete();

		                        if ($this->db->trans_status() === FALSE){
		                            $this->db->trans_rollback();
		                            $updd = "Update failed!";
		                            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
		                        }else{
		                            $updd = "Updated successfully!";
		                            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
		                        }

                            #### COMPRESS IMAGE FILE SIZE ####
		                        function compress_image($source_url, $destination_url, $quality) {
            									$info = getimagesize($source_url);

            									if ($info['mime'] == 'image/jpeg') $image = imagecreatefromjpeg($source_url);
            									elseif ($info['mime'] == 'image/gif') $image = imagecreatefromgif($source_url);
            									elseif ($info['mime'] == 'image/png') $image = imagecreatefrompng($source_url);

            									//save file
            									imagejpeg($image, $destination_url, $quality);

            									//return destination file
            									return $destination_url;
            								}

            								//usage
            								$src = 'assets/upload/home/press'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
            								$dest = 'assets/upload/home/press'.'/'.$this->db->get_where('media',array('media_id'=>$id))->row()->media_url;
            								$compressed = compress_image($src, $dest, 80);

		                }
		            }
		        }else{
		            $error = $this->upload->display_errors();
		            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$error.'</div>';
		        }
		    }else{
		    	$save = array(
            'media_content_en'=> $press_url,
            'media_date'=>date('Y-m-d H:i:s')
	             );


	            $this->db->trans_begin();
	            $this->Access->updatetable('media',$save,array('media_id'=>$id));
	            $this->db->trans_complete();

	            if ($this->db->trans_status() === FALSE){
	                $this->db->trans_rollback();
	                $updd = "Update failed!";
	                $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	            }else{
	                $updd = "Updated successfully!";
	                $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
	            }
		    }
    	}


	    $this->session->set_flashdata('notif_press', $notif);
        redirect('backend/home#press');

    }

    public function delete_press($id){
        $gettoId = $this->db->query("SELECT * FROM media WHERE media_id='".$id."'")->row()->media_url;
        $this->db->trans_begin();
        $this->Access->deletetable("media",array("media_id"=>$id));
        $this->db->trans_complete();

        unlink( realpath( APPPATH.'../assets/upload/home/press/'.$gettoId ));
        unlink( realpath(APPPATH.'../assets/upload/home/press/thumbnail/'.$gettoId ));
        redirect('backend/home#press');
    }

}
