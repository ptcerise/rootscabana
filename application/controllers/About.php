<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Access');
		$fe_lang = $this->session->userdata('fe_lang');
        if ($fe_lang) {
            $this->lang->load('rootscabana_frontend',$fe_lang);
        } else {
            $this->lang->load('rootscabana_frontend','french');
        }
	}

	public function index()
	{  
        redirect('about/handmade');
	}

	public function handmade(){
		$data['lang'] = $this->session->userdata('fe_lang');

		$data['banner'] = $this->Access->readtable('media','',array('media_page'=>'about-handmade','media_section'=>'banner'))->row();

        $data['contents'] = $this->Access->readtable('media','',array('media_page'=>'about-handmade','media_section'=>'content'))->result();
        
		$data['body_class'] = "about";
		$data['submenu'] = "handmade";
		$view['content'] = $this->load->view('v_about',$data,TRUE);
		$this->load->view('v_master',$view);
	}

	public function brand(){
		$data['lang'] = $this->session->userdata('fe_lang');

		$data['banner'] = $this->Access->readtable('media','',array('media_page'=>'about-brand','media_section'=>'banner'))->row();

        $data['contents'] = $this->Access->readtable('media','',array('media_page'=>'about-brand','media_section'=>'content'))->result();
        
		$data['body_class'] = "about";
		$data['submenu'] = "brand";
		$view['content'] = $this->load->view('v_about',$data,TRUE);
		$this->load->view('v_master',$view);
	}

	public function shop(){
		$data['lang'] = $this->session->userdata('fe_lang');

		$data['banner'] = $this->Access->readtable('media','',array('media_page'=>'about-shop','media_section'=>'banner'))->row();

        $data['contents'] = $this->Access->readtable('media','',array('media_page'=>'about-shop','media_section'=>'content'))->result();
        
		$data['body_class'] = "about";
		$data['submenu'] = "shop";
		$view['content'] = $this->load->view('v_about',$data,TRUE);
		$this->load->view('v_master',$view);
	}
}
