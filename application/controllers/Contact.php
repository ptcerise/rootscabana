<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Access');
		$fe_lang = $this->session->userdata('fe_lang');
        if ($fe_lang) {
            $this->lang->load('rootscabana_frontend',$fe_lang);
        } else {
            $this->lang->load('rootscabana_frontend','french');
        }
	}

	public function index()
	{	
		$data['lang'] = $this->session->userdata('fe_lang');
		$data['banner'] = $this->Access->readtable('media','',array('media_page'=>'contact','media_section'=>'banner'))->row();
        $data['content'] = $this->Access->readtable('general','',array('general_page'=>'contact','general_section'=>'content'))->row();
        // $data['branch'] = $this->Access->readtable('media','',array('media_page'=>'contact','media_section'=>'branch'))->result();
        $data['branch'] = $this->Access->readtable('media','',array('media_page'=>'about-shop','media_section'=>'content'))->result();

        $map = $this->Access->readtable('media','',array('media_page'=>'contact','media_section'=>'map'))->row()->media_url;

        $config['center'] = $map;
        $config['zoom'] = '19';
        $config['places'] = TRUE;
        $this->googlemaps->initialize($config);

        $marker = array();
        $marker['position'] = $map;
        $this->googlemaps->add_marker($marker);
        $data['map'] = $this->googlemaps->create_map();
		
		$data['body_class'] = "contact";
		$view['content'] = $this->load->view('v_contact',$data,TRUE);
		$this->load->view('v_master',$view);
	}

	public function send(){
		$contact_name = $this->input->post('contact_name');
		$contact_email = $this->input->post('contact_email');
		$contact_subject = $this->input->post('contact_subject');
		$contact_message = $this->input->post('contact_message');

		$save = array(
			'contact_name'=>$contact_name,
			'contact_email'=>$contact_email,
			'contact_subject'=>$contact_subject,
			'contact_message'=>$contact_message,
			'contact_date'=>date('Y-m-d H:i:s'),
			);
		$this->db->trans_begin();
		$this->Access->inserttable('contact',$save);
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE){
            $this->db->trans_rollback();
            $updd = "Sending message error!";
            $notif = '<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }else{
            $updd = "Message sent!";
            $notif = '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'.$updd.'</div>';
        }

        // SEND
            #Configure the E-mail content
            $config = array();
            $config['charset'] = 'utf-8';
            $config['useragent'] = 'Codeigniter';
            $config['protocol']= "smtp";
            $config['mailtype']= "html";
            $config['smtp_host']= "ssl://n1plcpnl0024.prod.ams1.secureserver.net";//smtp
            $config['smtp_port']= "465";
            $config['smtp_timeout']= "10";
            $config['smtp_user']= "info@rootscabana.com"; // sender email
            $config['smtp_pass']= "Corsica1982"; // sender password
            $config['crlf']="\r\n";
            $config['newline']="\r\n";
            $config['wordwrap'] = TRUE;
            $config['validate'] = TRUE;
            $config['priority'] = 2;
            $this->email->initialize($config);

            $this->email->from($contact_email,$contact_name);

            $sendto = $this->Access->readtable("media","",array("media_page"=>"all","media_section"=>"link_email"))->row()->media_url;
            $this->email->to($sendto);
            $this->email->reply_to($contact_email);
            $this->email->subject($contact_subject);
            $this->email->message($contact_message);

            if($this->email->send()){ #Send the E-mail
                // echo "<script type='text/javascript'>alert('Success!');</script>";
                $_SESSION['info_send'] = $notif;
                $this->session->mark_as_flash('info_send');
            }else{
                // echo "<script type='text/javascript'>alert('Fail!');</script>";
                $_SESSION['info_send'] = '<div class="alert alert-danger alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>Failed</div>';
                $this->session->mark_as_flash('info_send');
                
            }#Send E-mail END

	    $this->session->set_flashdata('notif_message', $notif);
        redirect('contact#message');
       // $this->email->print_debugger();
	}
}
