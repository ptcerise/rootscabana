<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Men extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Access');
		$fe_lang = $this->session->userdata('fe_lang');
        if ($fe_lang) {
            $this->lang->load('rootscabana_frontend',$fe_lang);
        } else {
            $this->lang->load('rootscabana_frontend','french');
        }
	}

	public function index()
	{	
		$data['lang'] = $this->session->userdata('fe_lang');

		$data['banner'] = $this->Access->readtable("media","",array("media_page"=>"men","media_section"=>"banner"))->row();
		$data['men_basic'] = $this->Access->readtable("category","",array("category_name"=>"men-basic"))->row();
		$data['men_print'] = $this->Access->readtable("category","",array("category_name"=>"men-print"))->row();
		$data['men_beachwear'] = $this->Access->readtable("category","",array("category_name"=>"men-beach-wear"))->row();
		$data['men_casualchic'] = $this->Access->readtable("category","",array("category_name"=>"men-casual-chic"))->row();

		$data['products_basic'] = $this->db->query("SELECT * FROM product WHERE product_category='men-basic' AND product_display='true' ORDER BY product_date DESC")->result();
		$data['products_print'] = $this->db->query("SELECT * FROM product WHERE product_category='men-print' AND product_display='true' ORDER BY product_date DESC")->result();
		$data['products_beachwear'] = $this->db->query("SELECT * FROM product WHERE product_category='men-beach-wear' AND product_display='true' ORDER BY product_date DESC")->result();
		$data['products_casualchic'] = $this->db->query("SELECT * FROM product WHERE product_category='men-casual-chic' AND product_display='true' ORDER BY product_date DESC")->result();

		$data['body_class'] = "section-men";
		
		$view['content'] = $this->load->view('v_men',$data,TRUE);
		$this->load->view('v_master',$view);
	}

}
