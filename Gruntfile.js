module.exports = function(grunt) {

	// Configure task(s)
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		// convert SASS to CSS
		  // sass: {
		  //   dist: {
		  //     files: {
		  //       'assets/src/css/styles.css': 'assets/src/css/styles.scss'
		  //     },
		  //   },
		  // },

		// concat | combine files
		// concat: {
		// 	js: {
		// 		src: ['assets/src/js/*.js'],// source
		// 		dest: 'assets/js/scripts.js',// destination
		// 	},
		// 	css: {
		// 		src: ['assets/src/css/*.css'],// source
		// 		dest: 'assets/css/styles.css',// destination
		// 	},
		// },

		// uglifyjs | COMPRESS JS FILE
		uglify: {
			js: {
				src: 'assets/js/scripts.js',
				dest: 'assets/js/scripts.min.js',
			},
		},

		// autoprefixer | AUTO PREFIX CSS FILE
		autoprefixer: {
			dist:{
        files:{
          'assets/css/main.css':'assets/css/main.css'
        }
			}
	  },

		// cssmin | COMPRESS CSS FILE
		cssmin: {
		  target: {
		    files: [{
		      expand: true,
		      cwd: 'assets/css',
		      src: ['main.css', '!*.min.css'],
		      dest: 'assets/css',
		      ext: '.min.css',
		    }],
		  },
		},

		// watch
		watch: {
		  css: {
		    files: ['assets/css/main.css'],
		    tasks: ['autoprefixer','cssmin'],
		  },
		  js: {
		    files: ['assets/js/scripts.js'],
		    tasks: ['uglify'],
		  },
		},
	});

	// Load the plugins
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Register task(s)
	grunt.registerTask('default', ['uglify', 'autoprefixer', 'cssmin', 'watch']);

};
