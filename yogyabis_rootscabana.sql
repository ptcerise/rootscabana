-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 24, 2016 at 12:01 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yogyabis_rootscabana`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(25) NOT NULL,
  `category_image_sq` varchar(100) NOT NULL,
  `category_image_pt` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `category_image_sq`, `category_image_pt`) VALUES
(1, 'women-basic', 'imgsq_19082016194730.jpg', 'imgpt_19082016194506.jpg'),
(2, 'women-print', 'imgsq_22082016051048.png', 'imgpt_19082016194523.jpg'),
(3, 'women-beach-wear', 'imgsq_22082016051530.png', 'imgpt_22082016104819.jpg'),
(4, 'women-casual-chic', 'imgsq_19082016194547.jpg', 'imgpt_19082016194547.png'),
(5, 'men-basic', 'imgsq_19082016202027.png', 'imgpt_19082016195111.jpg'),
(6, 'men-print', 'imgsq_19082016195131.jpg', 'imgpt_19082016195132.jpg'),
(7, 'men-beach-wear', 'imgsq_19082016195145.png', 'imgpt_19082016195146.jpg'),
(8, 'men-casual-chic', 'imgsq_19082016195201.jpg', 'imgpt_19082016195202.png');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `contact_id` int(11) NOT NULL,
  `contact_name` varchar(25) NOT NULL,
  `contact_email` varchar(50) NOT NULL,
  `contact_subject` varchar(50) NOT NULL,
  `contact_message` text NOT NULL,
  `contact_status` enum('0','1') NOT NULL DEFAULT '0',
  `contact_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`contact_id`, `contact_name`, `contact_email`, `contact_subject`, `contact_message`, `contact_status`, `contact_date`) VALUES
(1, 'Cerise', 'it@yogyabisnis.com', 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.', '1', '2016-08-18 21:06:16'),
(2, 'Cerise', 'it@yogyabisnis.com', 'testing', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ', '1', '2016-08-18 21:11:06');

-- --------------------------------------------------------

--
-- Table structure for table `crafts`
--

CREATE TABLE `crafts` (
  `crafts_id` int(11) NOT NULL,
  `crafts_name` varchar(25) NOT NULL,
  `crafts_spec_fr` text NOT NULL,
  `crafts_spec_en` text NOT NULL,
  `crafts_image` varchar(100) NOT NULL,
  `crafts_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `crafts`
--

INSERT INTO `crafts` (`crafts_id`, `crafts_name`, `crafts_spec_fr`, `crafts_spec_en`, `crafts_image`, `crafts_date`) VALUES
(1, 'Engraved Ring', '<ul><li>Size range from S to XXL :-D</li><li>Tencel and cotton mix&nbsp;:-D</li><li>Loose fit&nbsp;:-D</li><li>Come with matching belt&nbsp;:-D</li><li>Lightwear&nbsp;:-D</li><li>FRANCE</li></ul>', '<div><ul><li>Size range from S to XXL :-D</li><li>Tencel and cotton mix&nbsp;:-D</li><li>Loose fit&nbsp;:-D</li><li>Come with matching belt&nbsp;:-D</li><li>Lightwear&nbsp;:-D</li><li>ENGLISH</li></ul></div>', 'craft_18082016223541.jpg', '2016-08-24 11:38:36'),
(2, 'Gold Rings', '<ul><li>Size range from S to XXL</li><li>Tencel and cotton mix</li><li>Loose fit</li><li>Come with matching belt</li><li>Lightwear</li><li>FRANCE</li></ul>', '<div><ul><li>Size range from S to XXL</li><li>Tencel and cotton mix</li><li>Loose fit</li><li>Come with matching belt</li><li>Lightwear</li><li>ENGLISH</li></ul></div>', 'craft_18082016224957.jpg', '2016-08-24 11:38:57');

-- --------------------------------------------------------

--
-- Table structure for table `general`
--

CREATE TABLE `general` (
  `general_id` int(11) NOT NULL,
  `general_title_fr` varchar(100) NOT NULL,
  `general_title_en` varchar(100) NOT NULL,
  `general_content_fr` text NOT NULL,
  `general_content_en` text,
  `general_url` varchar(150) DEFAULT NULL,
  `general_page` varchar(100) NOT NULL,
  `general_section` varchar(100) NOT NULL,
  `general_date` datetime NOT NULL,
  `temp_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general`
--

INSERT INTO `general` (`general_id`, `general_title_fr`, `general_title_en`, `general_content_fr`, `general_content_en`, `general_url`, `general_page`, `general_section`, `general_date`, `temp_id`) VALUES
(1, 'Antoine de St-Exupery -FR', 'Antoine de St-Exupery - EN', '“Le soleil à tant fait l’amour à la mer,  qu’ils ont fini par enfanter la Corse” -FR', '“Le soleil à tant fait l’amour à la mer,  qu’ils ont fini par enfanter la Corse” -EN', NULL, 'home', 'quotes1', '2016-08-24 09:09:18', NULL),
(2, 'Antoine de St-Exupery -FR', 'Antoine de St-Exupery -EN', '“Fais de ta vie un rêve,  et d’un rêve une réalité” -FR', '“Fais de ta vie un rêve,  et d’un rêve une réalité” -EN', NULL, 'home', 'quotes2', '2016-08-24 09:09:18', NULL),
(3, '[FR] Lorem ipsum Elit cillum', '[EN] Lorem ipsum Elit cillum', '[FR] Lorem ipsum Excepteur commodo laborum nulla deserunt Excepteur sed in ut ullamco mollit adipisicing reprehenderit voluptate veniam ea eu reprehenderit voluptate laborum fugiat aute enim.Lorem ipsum Excepteur commodo laborum nulla deserunt Excepteur sed in ut ullamco mollit adipisicing reprehenderit voluptate veniam ea eu reprehenderit voluptate.&nbsp;', '[EN] Lorem ipsum Excepteur commodo laborum nulla deserunt Excepteur sed in ut ullamco mollit adipisicing reprehenderit voluptate veniam ea eu reprehenderit voluptate laborum fugiat aute enim.Lorem ipsum Excepteur commodo laborum nulla deserunt Excepteur sed in ut ullamco mollit adipisicing reprehenderit voluptate veniam ea eu reprehenderit voluptate.\r\n\r\n<br>', NULL, 'home', 'content1', '2016-08-24 09:29:06', NULL),
(4, '[FR] Lorem Ipsum', '[EN] Lorem Ipsum', '[FR] Lorem ipsum Labore consectetur exercitation Ut et ut et eiusmod tempor commodo mollit elit cillum ullamco Duis non non laboris sit labore incididunt amet Excepteur mollit in sit proident ex voluptate cillum laborum eu pariatur irure laboris proident id reprehenderit esse dolore aute nulla dolore anim exercitation id do ad aliqua elit in cupidatat reprehenderit reprehenderit proident nostrud officia mollit ex in minim Duis eu ex deserunt sit sunt irure aute quis exercitation proident voluptate.', '[EN]&nbsp;Lorem ipsum Labore consectetur exercitation Ut et ut et eiusmod tempor commodo mollit elit cillum ullamco Duis non non laboris sit labore incididunt amet Excepteur mollit in sit proident ex voluptate cillum laborum eu pariatur irure laboris proident id reprehenderit esse dolore aute nulla dolore anim exercitation id do ad aliqua elit in cupidatat reprehenderit reprehenderit proident nostrud officia mollit ex in minim Duis eu ex deserunt sit sunt irure aute quis exercitation proident voluptate.', NULL, 'home', 'content2', '2016-08-24 09:36:52', NULL),
(5, '', 'Lorem ispum sin dolor', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem', NULL, 'home', 'content3', '2016-08-12 00:00:00', NULL),
(6, 'Lorem ipsum sin [FR]', 'Lorem ipsum sin', '“Lorem ipsum Id ut laboris enim velit laboris do sit enim elit eiusmod dolor.” [FR]', '“Lorem ipsum Id ut laboris enim velit laboris do sit enim elit eiusmod dolor.”', NULL, 'about', 'quotes', '2016-08-24 11:57:30', NULL),
(7, '', '', '[FR] THE BEACH SHOP Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', 'THE BEACH SHOP Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.', '', 'about', 'content3', '2016-08-24 11:57:52', NULL),
(8, '', '', '[FR] Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla.', NULL, 'contact', 'content', '2016-08-24 12:00:33', NULL),
(9, '', '', '-FR- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor', '-EN- Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor', NULL, 'all', 'notif', '2016-08-24 08:54:35', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `media_id` int(11) NOT NULL,
  `media_title_fr` varchar(100) NOT NULL,
  `media_title_en` varchar(100) NOT NULL,
  `media_content_fr` text NOT NULL,
  `media_content_en` text NOT NULL,
  `media_url` varchar(100) NOT NULL,
  `media_page` varchar(100) NOT NULL,
  `media_section` varchar(100) NOT NULL,
  `media_date` datetime NOT NULL,
  `temp_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`media_id`, `media_title_fr`, `media_title_en`, `media_content_fr`, `media_content_en`, `media_url`, `media_page`, `media_section`, `media_date`, `temp_id`) VALUES
(1, '', '', '', '', 'carousel1_12082016102154.jpg', 'home', 'carousel1', '2016-08-12 15:21:54', NULL),
(2, '', '', '', '', 'carousel2_12082016102155.jpg', 'home', 'carousel2', '2016-08-12 15:21:55', NULL),
(3, '', '', '', '', 'carousel3_12082016102155.jpg', 'home', 'carousel3', '2016-08-12 15:21:55', NULL),
(4, '', '', '', '', 'content_12082016114348.png', 'home', 'content1', '2016-08-12 00:00:00', NULL),
(5, '', '', '', '', 'banner_15082016223320.png', 'about', 'banner', '2016-08-15 13:33:21', NULL),
(6, '', '', '[FR] Lorem ipsum Et sunt minim sit Duis est nulla labore irure eiusmod et nulla in aute consectetur sit minim incididunt amet magna et deserunt aliqua velit ad irure ex ut dolor Duis enim incididunt voluptate qui sit.Lorem ipsum Et sunt minim sit Duis est nulla labore irure eiusmod et nulla in aute consectetur sit minim incididunt amet magna et deserunt aliqua velit ad irure ex ut dolor Duis enim incididunt voluptate qui sit.', 'Lorem ipsum Et sunt minim sit Duis est nulla labore irure eiusmod et nulla in aute consectetur sit minim incididunt amet magna et deserunt aliqua velit ad irure ex ut dolor Duis enim incididunt voluptate qui sit.Lorem ipsum Et sunt minim sit Duis est nulla labore irure eiusmod et nulla in aute consectetur sit minim incididunt amet magna et deserunt aliqua velit ad irure ex ut dolor Duis enim incididunt voluptate qui sit.<br>', 'content1_15082016225316.jpg', 'about', 'content1', '2016-08-24 11:56:55', NULL),
(7, '', '', '[FR] OUR VALUES - Content2Lorem ipsum Et sunt minim sit Duis est nulla labore irure eiusmod et nulla in aute consectetur sit minim incididunt amet magna et deserunt aliqua velit ad irure ex ut dolor Duis enim incididunt voluptate qui sit.Lorem ipsum Et sunt minim sit Duis est nulla labore irure eiusmod et nulla in aute consectetur sit minim incididunt amet magna et deserunt aliqua velit ad irure ex ut dolor Duis enim incididunt voluptate qui sit.&nbsp;', '&nbsp;- OUR VALUES - \r\n\r\nContent2Lorem ipsum Et sunt minim sit Duis est nulla labore irure eiusmod et nulla in aute consectetur sit minim incididunt amet magna et deserunt aliqua velit ad irure ex ut dolor Duis enim incididunt voluptate qui sit.Lorem ipsum Et sunt minim sit Duis est nulla labore irure eiusmod et nulla in aute consectetur sit minim incididunt amet magna et deserunt aliqua velit ad irure ex ut dolor Duis enim incididunt voluptate qui sit.\r\n\r\n<br>', 'content2_16082016002240.png', 'about', 'content2', '2016-08-24 11:57:10', NULL),
(8, '', '', '', '', 'c3img1_16082016000403.jpg', 'about', 'content3_img1', '2016-08-16 00:04:04', NULL),
(9, '', '', '', '', 'c3img2_16082016000404.jpg', 'about', 'content3_img2', '2016-08-16 00:04:05', NULL),
(10, '', '', '', '', 'c3img3_16082016000623.jpg', 'about', 'content3_img3', '2016-08-16 00:06:24', NULL),
(11, '', '', '[FR] Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse', 'content4_16082016002341.jpg', 'about', 'content4', '2016-08-24 11:58:05', NULL),
(12, '', '', '', '', 'banner_18082016182042.png', 'contact', 'banner', '2016-08-18 09:20:44', NULL),
(13, '', '', '', '', 'banner_18082016204407.png', 'crafts', 'banner', '2016-08-18 11:44:08', NULL),
(14, 'HANDMADE CLOTHES [FR]', 'HANDMADE CLOTHES', 'Lorem ipsum Sit occaecat in adipisicing laboris aliquip sed id eiusmod in Ut sed aliqua ut incididunt aliquip laboris dolor.[FR]', 'Lorem ipsum Sit occaecat in adipisicing laboris aliquip sed id eiusmod in Ut sed aliqua ut incididunt aliquip laboris dolor.', 'adv_19082016012638.png', 'about', 'adv', '2016-08-24 11:56:33', NULL),
(15, '', 'PIGMENT DYEING', '', 'Lorem ipsum Sit occaecat in adipisicing laboris aliquip sed id eiusmod in Ut sed aliqua ut incididunt aliquip laboris dolor.', 'adv_19082016012729.png', 'about', 'adv', '2016-08-19 01:27:29', NULL),
(16, '', 'NATURAL MATERIALS', '', 'Lorem ipsum Sit occaecat in adipisicing laboris aliquip sed id eiusmod in Ut sed aliqua ut incididunt aliquip laboris dolor.', 'adv_19082016012750.png', 'about', 'adv', '2016-08-19 01:27:50', NULL),
(17, '', 'PERFECT CUT', '', 'Lorem ipsum Sit occaecat in adipisicing laboris aliquip sed id eiusmod in Ut sed aliqua ut incididunt aliquip laboris dolor.', 'adv_19082016012819.png', 'about', 'adv', '2016-08-19 01:28:19', NULL),
(18, '', '', '', '', 'banner_19082016193240.png', 'women', 'banner', '2016-08-19 10:32:41', NULL),
(19, '', '', '', '', 'banner_19082016193145.png', 'men', 'banner', '2016-08-19 10:31:46', NULL),
(20, '', '', '', '', 'http://www.facebook.com/', 'all', 'link_facebook', '2016-08-22 00:00:00', NULL),
(21, '', '', '', '', 'http://www.twitter.com/', 'all', 'link_twitter', '2016-08-22 00:00:00', NULL),
(22, '', '', '', '', 'http://www.instagram.com/', 'all', 'link_instagram', '2016-08-22 00:00:00', NULL),
(23, '', '', '', '', 'http://www.pinterest.com/', 'all', 'link_pinterest', '2016-08-22 00:00:00', NULL),
(24, '', '', '', '', 'john.doe@example.com', 'all', 'link_email', '2016-08-22 00:00:00', NULL),
(25, '', '', '', '', 'carousel_24082016081412.jpg', 'home', 'carousel', '2016-08-24 08:14:12', NULL),
(28, '', '', '', '', 'carousel_24082016082240.jpg', 'home', 'carousel', '2016-08-24 08:22:40', NULL),
(30, '', '', '', '', 'carousel_24082016083212.jpg', 'home', 'carousel', '2016-08-24 08:32:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `product_image` varchar(100) DEFAULT NULL,
  `product_content_fr` text NOT NULL,
  `product_content_en` text NOT NULL,
  `product_spec_fr` text NOT NULL,
  `product_spec_en` text NOT NULL,
  `product_category` varchar(20) NOT NULL,
  `product_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_name`, `product_image`, `product_content_fr`, `product_content_en`, `product_spec_fr`, `product_spec_en`, `product_category`, `product_date`) VALUES
(1, 'Women Basic #1', 'product_19082016231357.jpg', '', '', '', '', 'women-basic', '2016-08-22 07:02:41'),
(2, 'BeachWear #1', 'product_19082016231818.jpg', '', '', '', '', 'women-beach-wear', '2016-08-22 07:02:53'),
(3, 'Women Basic #2', 'product_19082016231836.jpg', '', '', '', '', 'women-basic', '2016-08-22 07:02:47'),
(4, 'MenPrint#1', 'product_20082016012434.jpg', '', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit.', '', '<ul><li>Size range from S to XXL</li><li>Tencel and cotton mix</li><li>Loose fit</li><li>Come with matching belt</li><li>Lightwear</li></ul>', 'men-print', '2016-08-22 07:03:53'),
(5, 'MenBasic#1', 'product_22082016060725.jpg', '[FR] Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit.', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit.', '<ul><li>Size range from S to XXL [FR]</li><li>Tencel and cotton mix [FR]</li><li>Loose fit [FR]</li><li>Come with matching belt</li><li>Lightwear</li></ul>', '<ul><li>Size range from S to XXL</li><li>Tencel and cotton mix</li><li>Loose fit</li><li>Come with matching belt</li><li>Lightwear</li></ul>', 'men-basic', '2016-08-24 11:32:04'),
(6, 'MenBeachWear#1', 'product_22082016063505.jpg', '', '<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit.</span>', '', '<ul><li>Size range from S to XXL</li><li>Tencel and cotton mix</li><li>Loose fit</li><li>Come with matching belt</li><li>Lightwear</li></ul>', 'men-beach-wear', '2016-08-22 08:11:08'),
(7, 'MenCasual#1', 'product_22082016063525.png', '', '', '', '', 'men-casual-chic', '2016-08-22 07:03:48'),
(8, 'WomenBeachWear#1', 'product_22082016063908.jpg', '', '', '', '', 'women-beach-wear', '2016-08-22 07:03:01'),
(9, 'WomenCasual#1', 'product_22082016063624.png', '', '', '', '', 'women-casual-chic', '2016-08-22 07:03:08');

-- --------------------------------------------------------

--
-- Table structure for table `product_detail`
--

CREATE TABLE `product_detail` (
  `detail_id` int(11) NOT NULL,
  `detail_image` varchar(100) NOT NULL,
  `detail_color` varchar(10) NOT NULL,
  `temp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_detail`
--

INSERT INTO `product_detail` (`detail_id`, `detail_image`, `detail_color`, `temp_id`) VALUES
(1, 'detail_2_22082016102919.jpg', '#6dc1e6', 2),
(2, 'detail_5_22082016080446.jpg', '#68b1e6', 5),
(3, 'detail_5_22082016080504.jpg', '#c97878', 5),
(4, 'detail_6_22082016081229.jpg', '#f52b37', 6);

-- --------------------------------------------------------

--
-- Table structure for table `subscribe`
--

CREATE TABLE `subscribe` (
  `subs_id` int(11) NOT NULL,
  `subs_email` varchar(50) NOT NULL,
  `subs_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscribe`
--

INSERT INTO `subscribe` (`subs_id`, `subs_email`, `subs_date`) VALUES
(4, 'zakky.rfikry@gmail.com', '2016-08-22 10:17:49'),
(5, 'zrfikry@gmail.com', '2016-08-24 06:32:30');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `user_pass` varchar(40) NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `user_full_name` varchar(50) NOT NULL,
  `user_level` enum('1','2') NOT NULL DEFAULT '2'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_name`, `user_pass`, `user_email`, `user_full_name`, `user_level`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@rootscabana.com', 'Administrator Rootscabana', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexes for table `crafts`
--
ALTER TABLE `crafts`
  ADD PRIMARY KEY (`crafts_id`);

--
-- Indexes for table `general`
--
ALTER TABLE `general`
  ADD PRIMARY KEY (`general_id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`media_id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `product_detail`
--
ALTER TABLE `product_detail`
  ADD PRIMARY KEY (`detail_id`);

--
-- Indexes for table `subscribe`
--
ALTER TABLE `subscribe`
  ADD PRIMARY KEY (`subs_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `crafts`
--
ALTER TABLE `crafts`
  MODIFY `crafts_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `general`
--
ALTER TABLE `general`
  MODIFY `general_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `media_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `product_detail`
--
ALTER TABLE `product_detail`
  MODIFY `detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `subscribe`
--
ALTER TABLE `subscribe`
  MODIFY `subs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
