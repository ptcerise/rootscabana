+function(a) {
    "use strict";
    a("#scrollDown").click(function() {
        a("html, body").animate({
            scrollTop: a("#mainContent").offset().top
        }, 1e3);
    });
}(jQuery);

var sourceSwap = function() {
    var a = $(this), b = a.data("alt-src");
    a.data("alt-src", a.attr("src")), a.attr("src", b);
};

+function(a) {
    a("img[data-alt-src]").each(function() {
        new Image().src = a(this).data("alt-src");
    }).hover(sourceSwap, sourceSwap);
}(jQuery), +function(a) {
    "use strict";
    a("#topBanner a").click(function(b) {
        var c = a(b.target).parent().parent();
        a(c).animate({
            "margin-top": "-" + c.outerHeight() + "px"
        }, 1e3);
    });
}(jQuery), +function(a) {
    "use strict";
    var b = !1;
    a("#shareButton").on("click", function() {
        var c = a("#shareLinks a").first();
        0 == b ? (a(c).animate({
            "margin-left": 0
        }, 100), a("#shareLinks").delay(100).queue(function(b) {
            a(this).css("overflow", "visible"), b();
        }), b = !0) : (a(c).animate({
            "margin-left": "-150px"
        }, 100), a("#shareLinks").css("overflow", "hidden"), b = !1);
    });
}(jQuery), +function(a) {
    "use strict";
    a(".crafts article aside").addClass("hide"), a(".about article aside").addClass("hide"), 
    a(".crafts article a").click(function() {
        a(".article-content", this).toggleClass("invisible"), a(".article-specs", this).toggleClass("hide");
    }), a(".about article a").click(function() {
        a(".article-content", this).toggleClass("invisible"), a(".article-specs", this).toggleClass("hide");
    });
}(jQuery), +function(a) {
    "use strict";
    a(".form-group input").focus(function() {
        var b = a("label[for='" + a(this).attr("id") + "']");
        b.addClass("active"), b.addClass("active-color");
    }), a(".form-group input").blur(function() {
        var b = a("label[for='" + a(this).attr("id") + "']");
        b.removeClass("active-color"), a(this).val() || b.removeClass("active");
    }), a(".form-group textarea").focus(function() {
        var b = a("label[for='" + a(this).attr("id") + "']");
        b.addClass("active"), b.addClass("active-color");
    }), a(".form-group textarea").blur(function() {
        var b = a("label[for='" + a(this).attr("id") + "']");
        b.removeClass("active-color"), a(this).val() || b.removeClass("active");
    });
}(jQuery), +function(a) {
    function b(b) {
        a(b).parent().find("span").text(b.value);
    }
    function c(c) {
        h[c] || (h[c] = !0, a(c).delegate(".expandingText textarea", "input onpropertychange", function() {
            b(this);
        }));
    }
    var d = [ "lineHeight", "textDecoration", "letterSpacing", "fontSize", "fontFamily", "fontStyle", "fontWeight", "textTransform", "textAlign", "direction", "wordSpacing", "fontSizeAdjust", "whiteSpace", "wordWrap" ], e = {
        overflow: "hidden",
        position: "absolute",
        top: "0",
        left: "0",
        height: "100%",
        resize: "none"
    }, f = {
        display: "block",
        visibility: "hidden"
    }, g = {
        position: "relative"
    }, h = {};
    a.fn.expandingTextarea = function() {
        return this.filter("textarea").each(function() {
            c(this.ownerDocument || document);
            var h = a(this);
            h.wrap("<div class='expandingText'></div>"), h.after("<pre class='textareaClone'><span></span><br /></pre>");
            var i = h.parent().css(g), j = i.find("pre").css(f);
            h.css(e), a.each(d, function(a, b) {
                j.css(b, h.css(b));
            }), b(this);
        });
    }, a.fn.expandingTextarea.initialSelector = "textarea.expanding", a(function() {
        a(a.fn.expandingTextarea.initialSelector).expandingTextarea();
    });
}(jQuery);