/*!
 * Sewwwa v1.0.0 (http://sewwwa.com)
 * Copyright 2016 PT Cerise.
 * All right reserved
 */

// Home page scroll down

+ function($) {
    'use strict';
    $("#scrollDown > .btn").click(function() {
        $('html, body').animate({
            scrollTop: $("#mainContent").offset().top
        }, 1000);
    });
}(jQuery);

// Image hover switch
+ function($) {
var sourceSwap = function() {
    var $this = $(this);
    var newSource = $this.data('alt-src');
    $this.data('alt-src', $this.attr('src'));
    $this.attr('src', newSource);
};
}(jQuery);

+ function($) {
    $('img[data-alt-src]').each(function() {
        new Image().src = $(this).data('alt-src');
    }).hover(sourceSwap, sourceSwap);
}(jQuery);

// top banner close button

+ function($) {
    'use strict';
    $("#topBanner a").click(function(event) {
        var parent = $(event.target).parent().parent();
        $(parent).animate({ "margin-top": "-" + parent.outerHeight() + "px" }, 1000);
    });
}(jQuery);

// Article page share button

+ function($) {
    'use strict';
    var toggled = false;
    $("#shareButton").on('click', function() {
        var children = $("#shareLinks a").first();
        if (toggled == false) {
            $(children).animate({ "margin-left": 0 }, 100);
            $('#shareLinks').delay(100).queue(function(next) {
                $(this).css("overflow", "visible");
                next();
            });
            toggled = true;
        } else {
            $(children).animate({ "margin-left": "-150px" }, 100);
            $('#shareLinks').css("overflow", "hidden");
            toggled = false;
        }
    });
}(jQuery);

// Craft and about toggle card details

// + function($) {
//     'use strict';

//     $(".crafts article aside").addClass('hide');
//     $(".about article aside").addClass('hide');
//     $(".crafts article a").click(function() {
//         $(".article-content", this).toggleClass('invisible');
//         $(".article-specs", this).toggleClass('hide');
//     });
//     $(".about article a").click(function() {
//         $(".article-content", this).toggleClass('invisible');
//         $(".article-specs", this).toggleClass('hide');
//     });
// }(jQuery);

// $(".crafts article a").click(function() {
//     $(this).find(".article-content").toggleClass('invisible');
//     $(this).find(".article-specs").toggleClass('hide');
// });
// $(".about article a").click(function() {
//     $(this).find(".article-content").toggleClass('invisible');
//     $(this).find(".article-specs").toggleClass('hide');
// });

// Froms label translation

+ function($) {
    'use strict';
    $(".form-group input").focus(function() {
        var label = $("label[for='" + $(this).attr('id') + "']");
        label.addClass('active');
        label.addClass('active-color');
    });
    $(".form-group input").blur(function() {
        var label = $("label[for='" + $(this).attr('id') + "']");
        label.removeClass('active-color');
        if (!$(this).val()) {
            label.removeClass('active');
        }
    });

    $(".form-group textarea").focus(function() {
        var label = $("label[for='" + $(this).attr('id') + "']");
        label.addClass('active');
        label.addClass('active-color');
    });
    $(".form-group textarea").blur(function() {
        var label = $("label[for='" + $(this).attr('id') + "']");
        label.removeClass('active-color');
        if (!$(this).val()) {
            label.removeClass('active');
        }
    });

}(jQuery);

+function ($) {

    var cloneCSSProperties = [
        'lineHeight', 'textDecoration', 'letterSpacing',
        'fontSize', 'fontFamily', 'fontStyle',
        'fontWeight', 'textTransform', 'textAlign',
        'direction', 'wordSpacing', 'fontSizeAdjust',
        'whiteSpace', 'wordWrap'
    ];

    var textareaCSS = {
        overflow: "hidden",
        position: "absolute",
        top: "0",
        left: "0",
        height: "100%",
        resize: "none"
    };

    var preCSS = {
        display: "block",
        visibility: "hidden"
    };

    var containerCSS = {
        position: "relative"
    };

    var initializedDocuments = { };

    function resize(textarea) {
        $(textarea).parent().find("span").text(textarea.value);
    }

    function initialize(document) {
        // Only need to initialize events once per document
        if (!initializedDocuments[document]) {
            initializedDocuments[document] = true;

            $(document).delegate(
                ".expandingText textarea",
                "input onpropertychange",
                function () {
                    resize(this);
                }
            );
        }
    }

    $.fn.expandingTextarea = function () {

        return this.filter("textarea").each(function () {

            initialize(this.ownerDocument || document);

            var textarea = $(this);

            textarea.wrap("<div class='expandingText'></div>");
            textarea.after("<pre class='textareaClone'><span></span><br /></pre>");

            var container = textarea.parent().css(containerCSS);
            var pre = container.find("pre").css(preCSS);

            textarea.css(textareaCSS);

            $.each(cloneCSSProperties, function (i, p) {
                pre.css(p, textarea.css(p));
            });

            resize(this);
        });
    };

    $.fn.expandingTextarea.initialSelector = "textarea.expanding";

    $(function () {
        $($.fn.expandingTextarea.initialSelector).expandingTextarea();
    });

}(jQuery);

+function ($) {

$(document).ready(function(){
    var crafts = $("div.crafts-content > div.crafts-item");
    for(var i = 0; i < crafts.length; i+=4) {
      crafts.slice(i, i+4).wrapAll("<div class='row column-item'></div>");
    }

    var total_item = $("div.crafts-item").size();

    if(total_item <= 12){
        $("#showmore_craft").hide();
    }

    $("div.crafts-item:gt(11)").hide();

    $("#showmore_craft").click(function(e){
        e.preventDefault();
        var vsbl = $("div.crafts-item:visible").size();
        var more = vsbl+4;
        $("div.crafts-item:lt("+ more +")").show();

        var vsbl = $("div.crafts-item:visible").size();
        if(vsbl < total_item){
            $(this).show();
        }else{
            $(this).hide();
        }
    });
});

$(document).ready(function(){
    var items = $("div.product-list > div.product-item");
    for(var i = 0; i < items.length; i+=3) {
      items.slice(i, i+3).wrapAll("<div class='row column-item'></div>");
    }

    var total_product = $("div.product-item").size();

    if(total_product < 9){
        $("#showmore_product").hide();
    }

    $("div.product-item:gt(8)").hide();

    $("#showmore_product").click(function(e){
        e.preventDefault();
        var vsbl = $("div.product-item:visible").size();
        var more = vsbl+3;
        $("div.product-item:lt("+ more +")").show();

        var vsbl = $("div.product-item:visible").size();
        if(vsbl < total_product){
            $(this).show();
        }else{
            $(this).hide();
        }
    });
});

}(jQuery);

function play_video(){
  /* Move the video from bg to popup */
  var vid = document.getElementById('video-banner');
  vid.play();
  if (vid.requestFullscreen) {
      vid.requestFullscreen();
      vid.setAttributeNode(document.createAttribute('controls'));
    } else if (vid.mozRequestFullScreen) {
      vid.mozRequestFullScreen();
      vid.setAttributeNode(document.createAttribute('controls'));
    } else if (vid.webkitRequestFullscreen) {
      vid.webkitRequestFullscreen();
      vid.setAttributeNode(document.createAttribute('controls'));
    }
}

+function($){

    function check_video(){

        var foo = $("#video-banner").get(0);

        if (typeof foo !== 'undefined') {
            if(!($("#video-banner").get(0).paused)){
                $("#playButton").hide();
            }else{
                $("#playButton").show();
            }
        }

        setTimeout(function(){
            check_video();
        }, 100);
    }
    check_video();

}(jQuery);

+function($){
  $(document).ready(function(){
    var $h = $(window).height();
    var $w = $(window).width();
    if($w <= 768){
      $("#hero-carousel").css("height",($h*0.7)+"px");
      $(".padding-video").css("height",($h*0.65)+"px");
    }else{
      $("#hero-carousel").css("height",($h*0.95)+"px");
      $(".padding-video").css("height",($h*0.9)+"px");
    }

  });
}(jQuery);


+function($){

function check_item(){
    var no_item = $('#itemSingleCarousel').find('.item.active').data('item-no');
    $('#itemCarouselIndicator').find('[data-slide-to!="'+no_item+'"]').find('img').css('opacity','0.56');
    $('#itemCarouselIndicator').find('[data-slide-to="'+no_item+'"]').find('img').removeAttr('style');

    setTimeout(function(){
        check_item();
    }, 10);
}

check_item();

$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});

}(jQuery);

+function($){
    $(".dropdown").mouseover(function(){
        $(this).addClass("open");
        $(this).find("a.dropdown-toggle").attr("aria-expanded","true");
    });
    $(".dropdown").mouseout(function(){
        $(this).removeClass("open");
        $(this).find("a.dropdown-toggle").attr("aria-expanded","false");
    });
}(jQuery);


+function($){
    $(".navbar-nav a").mouseover(function(){
        var target = $(this).data("target");

        if(target){
          $("[data-collapse-group='nav-group']:not([data-target='" + target + "'])").each(function () {
              $($(this).data("target")).animate({height: "0px"},0);
              // $(target).parent('div').animate({height: "0px"},50);
          });
          // $(".dropdown-container").animate({height: "285px"},250);
          // alert(par);
          $(target).animate({height: "285px"},200);

        }else{
          $("[data-collapse-group='nav-group']").each(function () {
              // $($(this).data("target")).removeClass("in").addClass("collapse");
              $($(this).data("target")).animate({height: "0px"},150);
              $($(this).data("target")).parent("div").animate({height: "0px"},150);
          });

        }
    });

    $(".navbar-nav a").click(function(){
        var target = $(this).data("target");

        if(target){
          $("[data-collapse-group='nav-group']:not([data-target='" + target + "'])").each(function () {
              $($(this).data("target")).animate({height: "0px"},0);
              // $(target).parent('div').animate({height: "0px"},50);
          });
          // $(".dropdown-container").animate({height: "285px"},250);
          // alert(par);
          $(target).animate({height: "285px"},200);

        }else{
          $("[data-collapse-group='nav-group']").each(function () {
              // $($(this).data("target")).removeClass("in").addClass("collapse");
              $($(this).data("target")).animate({height: "0px"},150);
              $($(this).data("target")).parent("div").animate({height: "0px"},150);
          });

        }
    });

    $("header").mouseover(function() {
        $("[data-collapse-group='nav-group']").each(function () {
            $($(this).data("target")).animate({height: "0px"},150);
            $("[data-collapse-group='nav-group']").parent(".hover-line").find(".static-hoverline").animate({display: "none",width: "0",left: "50%"},400);
        });
    });
    $("footer").mouseover(function() {
        $("[data-collapse-group='nav-group']").each(function () {
            $($(this).data("target")).animate({height: "0px"},150);
            $("[data-collapse-group='nav-group']").parent(".hover-line").find(".static-hoverline").animate({display: "none",width: "0",left: "50%"},400);
        });
    });
    $("main").mouseover(function() {
        $("[data-collapse-group='nav-group']").each(function () {
          $($(this).data("target")).animate({height: "0px"},150);
          $("[data-collapse-group='nav-group']").parent(".hover-line").find(".static-hoverline").animate({display: "none",width: "0",left: "50%"},400);
        });
    });
}(jQuery);

+function($){
    $('.bxslider').bxSlider({
        auto: true,
        speed: 1250,
        minSlides: 3,
        maxSlides: 4,
        slideWidth: 175,
        slideMargin: 10,
        pager: false,
    });
}(jQuery);

+(function($) {
    $(document).ready(function(){

        $(window).scroll(function(){
            if ($(this).scrollTop() > 200) {
                $('.navbar-fixed-scroll').addClass('navbar-show');
            } else {
                $('.navbar-fixed-scroll').removeClass('navbar-show');
            }
        });
    });
})(jQuery);

+function($){
    var $animation_elements = $('[data-animate="true"]');
    var $window = $(window);

    if($window.width() > 768){
      $window.on('scroll', check_if_in_view);
      $(document).ready(check_if_in_view);
    }

    function check_if_in_view() {

      var window_height = $window.height();
      var window_top_position = $window.scrollTop();
      var window_bottom_position = (window_top_position + window_height);

      $.each($animation_elements, function() {
        var $element = $(this);
        var $effect = $(this).data('effect');
        var element_height = $element.outerHeight();
        var element_top_position = $element.offset().top;
        var element_bottom_position = (element_top_position + element_height);

        //check to see if this current container is within viewport
        if ((element_bottom_position >= window_top_position) &&
            (element_top_position <= window_bottom_position)) {
          $element.addClass('animated '+$effect);
        }
      });
    }
}(jQuery);

+function($){
	'use strict';
	var $wdwWidth = $(window).width();
	if($wdwWidth > 768){
		$(document).ready(function(){
			$(".separator-about").each(function(){
        var $h = $(this).height();
        var $htext = $(this).find('.about-content').height();
        var $himg = $(this).find('.about-image').height();
        if($htext > $himg){
          $(this).find('.about-image').css('min-height',$h+'px');
        }else{
          $(this).find('.about-content').css('min-height',$h+'px');
        }

        $(this).find('.contact-map').css('min-height',$h+'px');
			});
		});
	}
}(jQuery);
