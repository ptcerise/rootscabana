$("#btn-crsl").click(function(){
	$("form#carousel_img").submit();
});
$("#btn-quotes").click(function(){
	$("form#form_quotes").submit();
});
$("#btn-content1").click(function(){
	$("form#form_content1").submit();
});
$("#btn-content2").click(function(){
	$("form#form_content2").submit();
});
$("#btn-content3").click(function(){
	$("form#form_content3").submit();
});
$("#btn_product").click(function(){
  $("form#form_product").submit();
});
$("#upd_product").click(function(){
  $("form#form_detail_product").submit();
});
$("#btn_variant").click(function(){
  $("form#form_variant").submit();
});

$("#cpicker").colorpicker();
$(".cpicker").colorpicker();

$("input[type='file']").change(function(){
	var parent = $(this).parents("div.image-with-preview");
    readURLTrack(this);

    function readURLTrack(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            parent.find('img.prev-img').attr('src', e.target.result).css("border-color","#3c763d");
        }

        reader.readAsDataURL(input.files[0]);
    }
}
});

tinymce.init({
    selector:'textarea',
    plugins: "link",
    // menubar: "none",
    default_link_target: "_blank"
});

//$("#textcraft").wysihtml5({
//    "stylesheets": ["../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysiwyg5-color.css"],
//    "font-styles": false,
//    "html": false,
//    "link": true,
//    "image": false,
//});
//
//$('textarea:not("#textcraft")').wysihtml5({
//    "stylesheets": ["../assets/plugins/bootstrap-wysihtml5/bootstrap3-wysiwyg5-color.css"],
//    "font-styles": false,
//    "html": false,
//    "link": true,
//    "image": false,
//});

$('textarea').css("height","auto");

$('table:not("#table_subs")').DataTable();

$("#table_subs").DataTable( {
    dom: 'Bfrtip',
    buttons: [
        'excel',
    ]
} );

$("a.buttons-excel").addClass("btn").addClass("btn-success").text("Export to Excel");

var table = $('table').DataTable();

$("#table_message").find("tr[data-status=0]").addClass("warning").css("font-weight","bold");

//$(".btn-read").click(function(){
//	var id = $(this).data("id");
//	var name = $(this).parents("tr").find("td.msg_name").text();
//	var email = $(this).parents("tr").find("td.msg_email").text();
//	var subject = $(this).parents("tr").find("td.msg_subject").text();
//	var date = $(this).parents("tr").find("td.msg_date").text();
//	var msg = $(this).data("message");
//
//	$("#modal_message").find(".contact_name").html(name);
//	$("#modal_message").find(".contact_email").html(email);
//	$("#modal_message").find(".contact_subject").html(subject);
//	$("#modal_message").find(".contact_date").html(date);
//	$("#modal_message").find("input[name='contact_id']").val(id);
//	$("#modal_message").find(".contact_message").html(msg);
//	$("#modal_message").find("a.reply").attr("href","mailto:"+email);
//});

$("#btn_craft").click(function(){
	$("#form_craft").submit();
});

$("#btn_press").click(function(){
	$("#form_press").submit();
});

//$(".edit-craft").click(function(){
//	var id = $(this).data("id");
//    alert(table.row(this).data());
//	var img = $(this).parents("tr.craft-item").find("img.thumb-craft").attr("src");
//	var name = $(this).parents("tr.craft-item").find("td.craft-name").text();
//	var spec_en = $(this).parents("tr.craft-item").find("td.craft-spec-en").html();
//	var spec_fr = $(this).parents("tr.craft-item").find("td.craft-spec-fr").html();
//
//	$("#modal_crafts").find("input[name='crafts_id']").val(id);
//	$("#modal_crafts").find("img.prev-img").attr("src",img);
//	$("#modal_crafts").find("input[name='crafts_name']").val(name);
//
//	var editorObj = $("#textcraft").data('wysihtml5');
//	var editor = editorObj.editor;
//	editor.setValue(spec_fr);
//	var editorObj = $("#textcraft2").data('wysihtml5');
//	var editor = editorObj.editor;
//	editor.setValue(spec_en);
//});
//
//$(".add-craft").click(function(){
//	$("#modal_crafts").find("input").val("");
//	$("#modal_crafts").find("img.prev-img").attr("src",js_base_url('assets/img/blank.png'));
//	var editorObj = $("#textcraft").data('wysihtml5');
//	var editor = editorObj.editor;
//	editor.setValue("");
//	var editorObj = $("#textcraft2").data('wysihtml5');
//	var editor = editorObj.editor;
//	editor.setValue("");
//});

//$(".edit-variant").click(function(){
//	var id = $(this).data("id");
//	var color = $(this).data("color");
//	var img = $(this).parents("tr.variant-item").find("img.thumb-product").attr("src");
//	$("#cpicker").find("i").css("background-color",color);
//
//	$("#modal_variant").find("input[name='detail_id']").val(id);
//	$("#modal_variant").find("img.prev-img").attr("src",img);
//	$("#modal_variant").find("input[name='detail_color']").val(color);
//});

$(".add-variant").click(function(){
	$("#cpicker").find("i").css("background-color","#fff");
	$("#modal_variant").find("input[name='detail_id']").val("");
	$("#modal_variant").find("img.prev-img").attr("src",js_base_url('assets/img/blank.png'));
	$("#modal_variant").find("input[name='detail_color']").val("");
});

$(".show_pass").click(function(){
	if($(".show_pass").is(":checked")){
		$(this).parents("form").find("input[name='user_pass']").attr("type","text");
	}else{
		$(this).parents("form").find("input[name='user_pass']").attr("type","password");
	}
});

$("form.form_user").on("submit",function(e){
	var pass1 = $(".modal_user:visible").find("input[name='user_pass']").val();
	var pass2 = $(".modal_user:visible").find("input[name='user_pass2']").val();

	if(pass1 == pass2){
		$("form.form_user").submit();
	}else{
		e.preventDefault();
		$(".modal_user:visible").find("input[name='user_pass2']").css("border","solid 1px #ff0000");
		alert("Password confirmation is incorrect!");
		$(".modal_user:visible").find("input[name='user_pass2']").focus();
	}
});

$(".add-user").click(function(){
	$("#modal_user").find("input[type='password']").attr("required","");
	$("#modal_user").find("input").val("");
	$("#modal_user").find("select[name='user_level'] option:first-child").attr("selected","");
});

//$(".edit-user").click(function(){
//	$("#modal_user").find("input[type='password']").removeAttr("required","");
//	var user_name = $(this).parents("tr.tbl-row").find("td.user-name").text();
//	var user_fname = $(this).parents("tr.tbl-row").find("td.user-fname").text();
//	var user_email = $(this).parents("tr.tbl-row").find("td.email").text();
//	var user_level = $(this).data("level");
//	var user_id = $(this).data("id");
//
//	$("form#form_user").find("input[name='user_name']").val(user_name);
//	$("form#form_user").find("input[name='user_full_name']").val(user_fname);
//	$("form#form_user").find("input[name='user_email']").val(user_email);
//	$("form#form_user").find("select[name='user_level'] option[value='"+user_level+"']").attr("selected","");
//	$("form#form_user").find("input[name='user_id']").val(user_id);
//});

//$("#export").click(function(){
//  $("#table_subs").table2excel({
//    // exclude CSS class
//    exclude: ".noExl",
//	name: "Subscribers",
//	filename: "subscriber-list",
//	fileext: ".xls",
//	exclude_img: true,
//	exclude_links: true,
//	exclude_inputs: true
//  });
//});
